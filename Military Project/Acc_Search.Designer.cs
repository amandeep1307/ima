namespace Military_Project
{
    partial class Acc_Search
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.Label acc_IdLabel;
            this.dataSet1 = new Military_Project.DataSet1();
            this.account_MasterBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.account_MasterTableAdapter = new Military_Project.DataSet1TableAdapters.Account_MasterTableAdapter();
            this.account_MasterDataGridView = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.acc_IdComboBox = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            acc_IdLabel = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.account_MasterBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.account_MasterDataGridView)).BeginInit();
            this.SuspendLayout();
            // 
            // acc_IdLabel
            // 
            acc_IdLabel.AutoSize = true;
            acc_IdLabel.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            acc_IdLabel.Location = new System.Drawing.Point(203, 108);
            acc_IdLabel.Name = "acc_IdLabel";
            acc_IdLabel.Size = new System.Drawing.Size(77, 17);
            acc_IdLabel.TabIndex = 2;
            acc_IdLabel.Text = "Account Id:";
            // 
            // dataSet1
            // 
            this.dataSet1.DataSetName = "DataSet1";
            this.dataSet1.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // account_MasterBindingSource
            // 
            this.account_MasterBindingSource.DataMember = "Account_Master";
            this.account_MasterBindingSource.DataSource = this.dataSet1;
            // 
            // account_MasterTableAdapter
            // 
            this.account_MasterTableAdapter.ClearBeforeFill = true;
            // 
            // account_MasterDataGridView
            // 
            this.account_MasterDataGridView.AutoGenerateColumns = false;
            this.account_MasterDataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn1,
            this.dataGridViewTextBoxColumn2,
            this.dataGridViewTextBoxColumn3,
            this.dataGridViewTextBoxColumn4});
            this.account_MasterDataGridView.DataSource = this.account_MasterBindingSource;
            this.account_MasterDataGridView.Location = new System.Drawing.Point(122, 161);
            this.account_MasterDataGridView.Name = "account_MasterDataGridView";
            this.account_MasterDataGridView.Size = new System.Drawing.Size(443, 220);
            this.account_MasterDataGridView.TabIndex = 1;
            this.account_MasterDataGridView.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.account_MasterDataGridView_CellContentClick);
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.DataPropertyName = "Acc_Id";
            this.dataGridViewTextBoxColumn1.HeaderText = "Acc_Id";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.DataPropertyName = "Acc_Head";
            this.dataGridViewTextBoxColumn2.HeaderText = "Acc_Head";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.DataPropertyName = "Limit_Bal";
            this.dataGridViewTextBoxColumn3.HeaderText = "Limit_Bal";
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            // 
            // dataGridViewTextBoxColumn4
            // 
            this.dataGridViewTextBoxColumn4.DataPropertyName = "Comments";
            this.dataGridViewTextBoxColumn4.HeaderText = "Comments";
            this.dataGridViewTextBoxColumn4.Name = "dataGridViewTextBoxColumn4";
            // 
            // acc_IdComboBox
            // 
            this.acc_IdComboBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.account_MasterBindingSource, "Acc_Id", true));
            this.acc_IdComboBox.DataSource = this.account_MasterBindingSource;
            this.acc_IdComboBox.DisplayMember = "Acc_Id";
            this.acc_IdComboBox.FormattingEnabled = true;
            this.acc_IdComboBox.Location = new System.Drawing.Point(301, 107);
            this.acc_IdComboBox.Name = "acc_IdComboBox";
            this.acc_IdComboBox.Size = new System.Drawing.Size(170, 21);
            this.acc_IdComboBox.TabIndex = 3;
            this.acc_IdComboBox.SelectedIndexChanged += new System.EventHandler(this.acc_IdComboBox_SelectedIndexChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Times New Roman", 15.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(261, 47);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(152, 24);
            this.label1.TabIndex = 4;
            this.label1.Text = "Account Search";
            // 
            // Acc_Search
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(684, 487);
            this.Controls.Add(this.label1);
            this.Controls.Add(acc_IdLabel);
            this.Controls.Add(this.acc_IdComboBox);
            this.Controls.Add(this.account_MasterDataGridView);
            this.Name = "Acc_Search";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Account_Search";
            this.Load += new System.EventHandler(this.Acc_Search_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataSet1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.account_MasterBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.account_MasterDataGridView)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DataSet1 dataSet1;
        private System.Windows.Forms.BindingSource account_MasterBindingSource;
        private Military_Project.DataSet1TableAdapters.Account_MasterTableAdapter account_MasterTableAdapter;
        private System.Windows.Forms.DataGridView account_MasterDataGridView;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
        private System.Windows.Forms.ComboBox acc_IdComboBox;
        private System.Windows.Forms.Label label1;
    }
}