using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Military_Project
{
    public partial class Acc_Search : Form
    {
        public Acc_Search()
        {
            InitializeComponent();
        }
        DataSet1TableAdapters.Account_MasterTableAdapter AccS = new Military_Project.DataSet1TableAdapters.Account_MasterTableAdapter();
        private void account_MasterBindingNavigatorSaveItem_Click(object sender, EventArgs e)
        {
            this.Validate();
            this.account_MasterBindingSource.EndEdit();
            this.account_MasterTableAdapter.Update(this.dataSet1.Account_Master);

        }

        private void Acc_Search_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'dataSet1.Account_Master' table. You can move, or remove it, as needed.
            this.account_MasterTableAdapter.Fill(this.dataSet1.Account_Master);

        }

        private void acc_IdComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            DataTable dt = new DataTable();
            dt = AccS.search_account(acc_IdComboBox.Text);
            if (dt.Rows.Count > 0)
            {
                account_MasterDataGridView.DataSource = dt;
            }
        }

        private void account_MasterDataGridView_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }
    }
}