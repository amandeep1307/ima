namespace Military_Project
{
    partial class Account_Master
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.Label acc_IdLabel;
            System.Windows.Forms.Label acc_HeadLabel;
            System.Windows.Forms.Label limit_BalLabel;
            System.Windows.Forms.Label commentsLabel;
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Account_Master));
            this.dataSet1 = new Military_Project.DataSet1();
            this.account_MasterBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.account_MasterTableAdapter = new Military_Project.DataSet1TableAdapters.Account_MasterTableAdapter();
            this.account_MasterBindingNavigator = new System.Windows.Forms.BindingNavigator(this.components);
            this.bindingNavigatorAddNewItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorCountItem = new System.Windows.Forms.ToolStripLabel();
            this.bindingNavigatorMoveFirstItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorMovePreviousItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorSeparator = new System.Windows.Forms.ToolStripSeparator();
            this.bindingNavigatorPositionItem = new System.Windows.Forms.ToolStripTextBox();
            this.bindingNavigatorSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.bindingNavigatorMoveNextItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorMoveLastItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripButton1 = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton2 = new System.Windows.Forms.ToolStripButton();
            this.account_MasterBindingNavigatorSaveItem = new System.Windows.Forms.ToolStripButton();
            this.acc_IdTextBox = new System.Windows.Forms.TextBox();
            this.acc_HeadTextBox = new System.Windows.Forms.TextBox();
            this.limit_BalTextBox = new System.Windows.Forms.TextBox();
            this.commentsTextBox = new System.Windows.Forms.TextBox();
            this.button1 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            acc_IdLabel = new System.Windows.Forms.Label();
            acc_HeadLabel = new System.Windows.Forms.Label();
            limit_BalLabel = new System.Windows.Forms.Label();
            commentsLabel = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.account_MasterBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.account_MasterBindingNavigator)).BeginInit();
            this.account_MasterBindingNavigator.SuspendLayout();
            this.SuspendLayout();
            // 
            // acc_IdLabel
            // 
            acc_IdLabel.AutoSize = true;
            acc_IdLabel.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            acc_IdLabel.Location = new System.Drawing.Point(153, 155);
            acc_IdLabel.Name = "acc_IdLabel";
            acc_IdLabel.Size = new System.Drawing.Size(77, 17);
            acc_IdLabel.TabIndex = 1;
            acc_IdLabel.Text = "Account Id:";
            // 
            // acc_HeadLabel
            // 
            acc_HeadLabel.AutoSize = true;
            acc_HeadLabel.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            acc_HeadLabel.Location = new System.Drawing.Point(133, 203);
            acc_HeadLabel.Name = "acc_HeadLabel";
            acc_HeadLabel.Size = new System.Drawing.Size(97, 17);
            acc_HeadLabel.TabIndex = 3;
            acc_HeadLabel.Text = "Account Head:";
            // 
            // limit_BalLabel
            // 
            limit_BalLabel.AutoSize = true;
            limit_BalLabel.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            limit_BalLabel.Location = new System.Drawing.Point(165, 248);
            limit_BalLabel.Name = "limit_BalLabel";
            limit_BalLabel.Size = new System.Drawing.Size(65, 17);
            limit_BalLabel.TabIndex = 5;
            limit_BalLabel.Text = "Limit Bal:";
            // 
            // commentsLabel
            // 
            commentsLabel.AutoSize = true;
            commentsLabel.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            commentsLabel.Location = new System.Drawing.Point(156, 293);
            commentsLabel.Name = "commentsLabel";
            commentsLabel.Size = new System.Drawing.Size(74, 17);
            commentsLabel.TabIndex = 7;
            commentsLabel.Text = "Comments:";
            // 
            // dataSet1
            // 
            this.dataSet1.DataSetName = "DataSet1";
            this.dataSet1.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // account_MasterBindingSource
            // 
            this.account_MasterBindingSource.DataMember = "Account_Master";
            this.account_MasterBindingSource.DataSource = this.dataSet1;
            // 
            // account_MasterTableAdapter
            // 
            this.account_MasterTableAdapter.ClearBeforeFill = true;
            // 
            // account_MasterBindingNavigator
            // 
            this.account_MasterBindingNavigator.AddNewItem = this.bindingNavigatorAddNewItem;
            this.account_MasterBindingNavigator.BindingSource = this.account_MasterBindingSource;
            this.account_MasterBindingNavigator.CountItem = this.bindingNavigatorCountItem;
            this.account_MasterBindingNavigator.DeleteItem = null;
            this.account_MasterBindingNavigator.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.bindingNavigatorMoveFirstItem,
            this.bindingNavigatorMovePreviousItem,
            this.bindingNavigatorSeparator,
            this.bindingNavigatorPositionItem,
            this.bindingNavigatorCountItem,
            this.bindingNavigatorSeparator1,
            this.bindingNavigatorMoveNextItem,
            this.bindingNavigatorMoveLastItem,
            this.bindingNavigatorSeparator2,
            this.bindingNavigatorAddNewItem,
            this.toolStripButton1,
            this.toolStripButton2,
            this.account_MasterBindingNavigatorSaveItem});
            this.account_MasterBindingNavigator.Location = new System.Drawing.Point(0, 0);
            this.account_MasterBindingNavigator.MoveFirstItem = this.bindingNavigatorMoveFirstItem;
            this.account_MasterBindingNavigator.MoveLastItem = this.bindingNavigatorMoveLastItem;
            this.account_MasterBindingNavigator.MoveNextItem = this.bindingNavigatorMoveNextItem;
            this.account_MasterBindingNavigator.MovePreviousItem = this.bindingNavigatorMovePreviousItem;
            this.account_MasterBindingNavigator.Name = "account_MasterBindingNavigator";
            this.account_MasterBindingNavigator.PositionItem = this.bindingNavigatorPositionItem;
            this.account_MasterBindingNavigator.Size = new System.Drawing.Size(684, 25);
            this.account_MasterBindingNavigator.TabIndex = 0;
            this.account_MasterBindingNavigator.Text = "bindingNavigator1";
            // 
            // bindingNavigatorAddNewItem
            // 
            this.bindingNavigatorAddNewItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorAddNewItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorAddNewItem.Image")));
            this.bindingNavigatorAddNewItem.Name = "bindingNavigatorAddNewItem";
            this.bindingNavigatorAddNewItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorAddNewItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorAddNewItem.Text = "Add new data";
            // 
            // bindingNavigatorCountItem
            // 
            this.bindingNavigatorCountItem.Name = "bindingNavigatorCountItem";
            this.bindingNavigatorCountItem.Size = new System.Drawing.Size(35, 22);
            this.bindingNavigatorCountItem.Text = "of {0}";
            this.bindingNavigatorCountItem.ToolTipText = "Total number of items";
            // 
            // bindingNavigatorMoveFirstItem
            // 
            this.bindingNavigatorMoveFirstItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMoveFirstItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveFirstItem.Image")));
            this.bindingNavigatorMoveFirstItem.Name = "bindingNavigatorMoveFirstItem";
            this.bindingNavigatorMoveFirstItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMoveFirstItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorMoveFirstItem.Text = "Move first";
            // 
            // bindingNavigatorMovePreviousItem
            // 
            this.bindingNavigatorMovePreviousItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMovePreviousItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMovePreviousItem.Image")));
            this.bindingNavigatorMovePreviousItem.Name = "bindingNavigatorMovePreviousItem";
            this.bindingNavigatorMovePreviousItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMovePreviousItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorMovePreviousItem.Text = "Move previous";
            // 
            // bindingNavigatorSeparator
            // 
            this.bindingNavigatorSeparator.Name = "bindingNavigatorSeparator";
            this.bindingNavigatorSeparator.Size = new System.Drawing.Size(6, 25);
            // 
            // bindingNavigatorPositionItem
            // 
            this.bindingNavigatorPositionItem.AccessibleName = "Position";
            this.bindingNavigatorPositionItem.AutoSize = false;
            this.bindingNavigatorPositionItem.Name = "bindingNavigatorPositionItem";
            this.bindingNavigatorPositionItem.Size = new System.Drawing.Size(50, 23);
            this.bindingNavigatorPositionItem.Text = "0";
            this.bindingNavigatorPositionItem.ToolTipText = "Current position";
            // 
            // bindingNavigatorSeparator1
            // 
            this.bindingNavigatorSeparator1.Name = "bindingNavigatorSeparator1";
            this.bindingNavigatorSeparator1.Size = new System.Drawing.Size(6, 25);
            // 
            // bindingNavigatorMoveNextItem
            // 
            this.bindingNavigatorMoveNextItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMoveNextItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveNextItem.Image")));
            this.bindingNavigatorMoveNextItem.Name = "bindingNavigatorMoveNextItem";
            this.bindingNavigatorMoveNextItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMoveNextItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorMoveNextItem.Text = "Move next";
            // 
            // bindingNavigatorMoveLastItem
            // 
            this.bindingNavigatorMoveLastItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMoveLastItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveLastItem.Image")));
            this.bindingNavigatorMoveLastItem.Name = "bindingNavigatorMoveLastItem";
            this.bindingNavigatorMoveLastItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMoveLastItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorMoveLastItem.Text = "Move last";
            // 
            // bindingNavigatorSeparator2
            // 
            this.bindingNavigatorSeparator2.Name = "bindingNavigatorSeparator2";
            this.bindingNavigatorSeparator2.Size = new System.Drawing.Size(6, 25);
            // 
            // toolStripButton1
            // 
            this.toolStripButton1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton1.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton1.Image")));
            this.toolStripButton1.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton1.Name = "toolStripButton1";
            this.toolStripButton1.Size = new System.Drawing.Size(23, 22);
            this.toolStripButton1.Text = "Delete";
            this.toolStripButton1.Click += new System.EventHandler(this.toolStripButton1_Click);
            // 
            // toolStripButton2
            // 
            this.toolStripButton2.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton2.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton2.Image")));
            this.toolStripButton2.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton2.Name = "toolStripButton2";
            this.toolStripButton2.Size = new System.Drawing.Size(23, 22);
            this.toolStripButton2.Text = "Update";
            this.toolStripButton2.Click += new System.EventHandler(this.toolStripButton2_Click);
            // 
            // account_MasterBindingNavigatorSaveItem
            // 
            this.account_MasterBindingNavigatorSaveItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.account_MasterBindingNavigatorSaveItem.Image = ((System.Drawing.Image)(resources.GetObject("account_MasterBindingNavigatorSaveItem.Image")));
            this.account_MasterBindingNavigatorSaveItem.Name = "account_MasterBindingNavigatorSaveItem";
            this.account_MasterBindingNavigatorSaveItem.Size = new System.Drawing.Size(23, 22);
            this.account_MasterBindingNavigatorSaveItem.Text = "Save";
            this.account_MasterBindingNavigatorSaveItem.Click += new System.EventHandler(this.account_MasterBindingNavigatorSaveItem_Click);
            // 
            // acc_IdTextBox
            // 
            this.acc_IdTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.account_MasterBindingSource, "Acc_Id", true));
            this.acc_IdTextBox.Location = new System.Drawing.Point(252, 154);
            this.acc_IdTextBox.Name = "acc_IdTextBox";
            this.acc_IdTextBox.Size = new System.Drawing.Size(189, 20);
            this.acc_IdTextBox.TabIndex = 2;
            // 
            // acc_HeadTextBox
            // 
            this.acc_HeadTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.account_MasterBindingSource, "Acc_Head", true));
            this.acc_HeadTextBox.Location = new System.Drawing.Point(252, 200);
            this.acc_HeadTextBox.Name = "acc_HeadTextBox";
            this.acc_HeadTextBox.Size = new System.Drawing.Size(189, 20);
            this.acc_HeadTextBox.TabIndex = 4;
            // 
            // limit_BalTextBox
            // 
            this.limit_BalTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.account_MasterBindingSource, "Limit_Bal", true));
            this.limit_BalTextBox.Location = new System.Drawing.Point(252, 245);
            this.limit_BalTextBox.Name = "limit_BalTextBox";
            this.limit_BalTextBox.Size = new System.Drawing.Size(189, 20);
            this.limit_BalTextBox.TabIndex = 6;
            // 
            // commentsTextBox
            // 
            this.commentsTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.account_MasterBindingSource, "Comments", true));
            this.commentsTextBox.Location = new System.Drawing.Point(250, 289);
            this.commentsTextBox.Multiline = true;
            this.commentsTextBox.Name = "commentsTextBox";
            this.commentsTextBox.Size = new System.Drawing.Size(192, 58);
            this.commentsTextBox.TabIndex = 8;
            // 
            // button1
            // 
            this.button1.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.Location = new System.Drawing.Point(447, 152);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(131, 23);
            this.button1.TabIndex = 15;
            this.button1.Text = "Check Availability?";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button3
            // 
            this.button3.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button3.Location = new System.Drawing.Point(467, 324);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(75, 23);
            this.button3.TabIndex = 16;
            this.button3.Text = "Search";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Times New Roman", 18F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(245, 69);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(177, 26);
            this.label1.TabIndex = 17;
            this.label1.Text = "Account Master";
            // 
            // Account_Master
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(684, 487);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.button1);
            this.Controls.Add(acc_IdLabel);
            this.Controls.Add(this.acc_IdTextBox);
            this.Controls.Add(acc_HeadLabel);
            this.Controls.Add(this.acc_HeadTextBox);
            this.Controls.Add(limit_BalLabel);
            this.Controls.Add(this.limit_BalTextBox);
            this.Controls.Add(commentsLabel);
            this.Controls.Add(this.commentsTextBox);
            this.Controls.Add(this.account_MasterBindingNavigator);
            this.Name = "Account_Master";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Account_Master";
            this.Load += new System.EventHandler(this.Account_Master_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataSet1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.account_MasterBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.account_MasterBindingNavigator)).EndInit();
            this.account_MasterBindingNavigator.ResumeLayout(false);
            this.account_MasterBindingNavigator.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DataSet1 dataSet1;
        private System.Windows.Forms.BindingSource account_MasterBindingSource;
        private Military_Project.DataSet1TableAdapters.Account_MasterTableAdapter account_MasterTableAdapter;
        private System.Windows.Forms.BindingNavigator account_MasterBindingNavigator;
        private System.Windows.Forms.ToolStripButton bindingNavigatorAddNewItem;
        private System.Windows.Forms.ToolStripLabel bindingNavigatorCountItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMoveFirstItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMovePreviousItem;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator;
        private System.Windows.Forms.ToolStripTextBox bindingNavigatorPositionItem;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator1;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMoveNextItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMoveLastItem;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator2;
        private System.Windows.Forms.ToolStripButton account_MasterBindingNavigatorSaveItem;
        private System.Windows.Forms.TextBox acc_IdTextBox;
        private System.Windows.Forms.TextBox acc_HeadTextBox;
        private System.Windows.Forms.TextBox limit_BalTextBox;
        private System.Windows.Forms.TextBox commentsTextBox;
        private System.Windows.Forms.ToolStripButton toolStripButton1;
        private System.Windows.Forms.ToolStripButton toolStripButton2;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Label label1;
    }
}