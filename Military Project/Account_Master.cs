using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Military_Project
{
    public partial class Account_Master : Form
    {
        public Account_Master()
        {
            InitializeComponent();
        }
        DataSet1TableAdapters.Account_MasterTableAdapter a1 = new Military_Project.DataSet1TableAdapters.Account_MasterTableAdapter();

        private void account_MasterBindingNavigatorSaveItem_Click(object sender, EventArgs e)
        {
            if (acc_IdTextBox.Text.Length != 0 && acc_HeadTextBox.Text.Length != 0 && limit_BalTextBox.Text.Length != 0 && commentsTextBox.Text.Length != 0)
            {
                a1.insert_account(acc_IdTextBox.Text, acc_HeadTextBox.Text, limit_BalTextBox.Text, commentsTextBox.Text);
                MessageBox.Show("Record Inserted Successfully !");

            }
            else
            {
                MessageBox.Show("Please enter all values", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }
        private void Account_Master_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'dataSet1.Account_Master' table. You can move, or remove it, as needed.
            this.account_MasterTableAdapter.Fill(this.dataSet1.Account_Master);
            //acc_HeadTextBox.Clear();
            //acc_IdTextBox.Clear();
            //limit_BalTextBox.Clear();
            //commentsTextBox.Clear();
        }

        private void toolStripButton1_Click(object sender, EventArgs e)
        {
            a1.delete_account(acc_IdTextBox.Text);
            MessageBox.Show("Record Deleted Successfully !");
        }

        private void toolStripButton2_Click(object sender, EventArgs e)
        {
            a1.update_account(acc_IdTextBox.Text, acc_HeadTextBox.Text, limit_BalTextBox.Text, commentsTextBox.Text);
            MessageBox.Show("Record Updated Successfully !");
        }

        private void button3_Click(object sender, EventArgs e)
        {
            Acc_Search AC1 = new Acc_Search();
            AC1.ShowDialog();
           
        }

        private void button1_Click(object sender, EventArgs e)
        {
            DataTable dt = new DataTable();
            dt = a1.search_account(acc_IdTextBox.Text);
            if (dt.Rows.Count > 0)
            {
                MessageBox.Show("Already Exists !");
                acc_IdTextBox.Text = "";
                acc_IdTextBox.Focus();
            }
            else
            {
                MessageBox.Show("Account_Id Available !");
            }
        }
    }
}