using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Military_Project
{
    public partial class Asearch : Form
    {
        public Asearch()
        {
            InitializeComponent();
        }
        DataSet1TableAdapters.Soldier_MasterTableAdapter AS = new Military_Project.DataSet1TableAdapters.Soldier_MasterTableAdapter();
        private void soldier_MasterBindingNavigatorSaveItem_Click(object sender, EventArgs e)
        {
            this.Validate();
            this.soldier_MasterBindingSource.EndEdit();
            this.soldier_MasterTableAdapter.Update(this.dataSet1.Soldier_Master);

        }

        private void Asearch_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'dataSet1.Soldier_Master' table. You can move, or remove it, as needed.
            this.soldier_MasterTableAdapter.Fill(this.dataSet1.Soldier_Master);
            comboBox1.Text = "";
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            DataTable dt = new DataTable();
            dt = AS.search_soldier(comboBox1.Text);
            if (dt.Rows.Count > 0)
            {
                soldier_MasterDataGridView.DataSource = dt;
            }
        }
    }
}