namespace Military_Project
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.Label battalion_IdLabel;
            System.Windows.Forms.Label battalion_NameLabel;
            System.Windows.Forms.Label battalion_DescLabel;
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.label1 = new System.Windows.Forms.Label();
            this.battalion_IdTextBox = new System.Windows.Forms.TextBox();
            this.battalion_MasterBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataSet1 = new Military_Project.DataSet1();
            this.battalion_NameTextBox = new System.Windows.Forms.TextBox();
            this.battalion_DescTextBox = new System.Windows.Forms.TextBox();
            this.button3 = new System.Windows.Forms.Button();
            this.battalion_MasterBindingNavigator = new System.Windows.Forms.BindingNavigator(this.components);
            this.bindingNavigatorAddNewItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorCountItem = new System.Windows.Forms.ToolStripLabel();
            this.bindingNavigatorMoveFirstItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorMovePreviousItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorSeparator = new System.Windows.Forms.ToolStripSeparator();
            this.bindingNavigatorPositionItem = new System.Windows.Forms.ToolStripTextBox();
            this.bindingNavigatorSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.bindingNavigatorMoveNextItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorMoveLastItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripButton1 = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton2 = new System.Windows.Forms.ToolStripButton();
            this.battalion_MasterBindingNavigatorSaveItem = new System.Windows.Forms.ToolStripButton();
            this.button1 = new System.Windows.Forms.Button();
            this.battalion_MasterTableAdapter = new Military_Project.DataSet1TableAdapters.Battalion_MasterTableAdapter();
            battalion_IdLabel = new System.Windows.Forms.Label();
            battalion_NameLabel = new System.Windows.Forms.Label();
            battalion_DescLabel = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.battalion_MasterBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.battalion_MasterBindingNavigator)).BeginInit();
            this.battalion_MasterBindingNavigator.SuspendLayout();
            this.SuspendLayout();
            // 
            // battalion_IdLabel
            // 
            battalion_IdLabel.AutoSize = true;
            battalion_IdLabel.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            battalion_IdLabel.Location = new System.Drawing.Point(158, 165);
            battalion_IdLabel.Name = "battalion_IdLabel";
            battalion_IdLabel.Size = new System.Drawing.Size(79, 17);
            battalion_IdLabel.TabIndex = 2;
            battalion_IdLabel.Text = "Battalion Id:";
            // 
            // battalion_NameLabel
            // 
            battalion_NameLabel.AutoSize = true;
            battalion_NameLabel.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            battalion_NameLabel.Location = new System.Drawing.Point(158, 210);
            battalion_NameLabel.Name = "battalion_NameLabel";
            battalion_NameLabel.Size = new System.Drawing.Size(103, 17);
            battalion_NameLabel.TabIndex = 4;
            battalion_NameLabel.Text = "Battalion Name:";
            // 
            // battalion_DescLabel
            // 
            battalion_DescLabel.AutoSize = true;
            battalion_DescLabel.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            battalion_DescLabel.Location = new System.Drawing.Point(158, 254);
            battalion_DescLabel.Name = "battalion_DescLabel";
            battalion_DescLabel.Size = new System.Drawing.Size(98, 17);
            battalion_DescLabel.TabIndex = 6;
            battalion_DescLabel.Text = "Battalion Desc:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Times New Roman", 15.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(241, 92);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(161, 24);
            this.label1.TabIndex = 0;
            this.label1.Text = "Battalion Master";
            // 
            // battalion_IdTextBox
            // 
            this.battalion_IdTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.battalion_MasterBindingSource, "Battalion_Id", true));
            this.battalion_IdTextBox.Location = new System.Drawing.Point(278, 162);
            this.battalion_IdTextBox.Name = "battalion_IdTextBox";
            this.battalion_IdTextBox.Size = new System.Drawing.Size(163, 20);
            this.battalion_IdTextBox.TabIndex = 3;
            // 
            // battalion_MasterBindingSource
            // 
            this.battalion_MasterBindingSource.DataMember = "Battalion_Master";
            this.battalion_MasterBindingSource.DataSource = this.dataSet1;
            // 
            // dataSet1
            // 
            this.dataSet1.DataSetName = "DataSet1";
            this.dataSet1.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // battalion_NameTextBox
            // 
            this.battalion_NameTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.battalion_MasterBindingSource, "Battalion_Name", true));
            this.battalion_NameTextBox.Location = new System.Drawing.Point(278, 207);
            this.battalion_NameTextBox.Name = "battalion_NameTextBox";
            this.battalion_NameTextBox.Size = new System.Drawing.Size(163, 20);
            this.battalion_NameTextBox.TabIndex = 5;
            // 
            // battalion_DescTextBox
            // 
            this.battalion_DescTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.battalion_MasterBindingSource, "Battalion_Desc", true));
            this.battalion_DescTextBox.Location = new System.Drawing.Point(278, 251);
            this.battalion_DescTextBox.Multiline = true;
            this.battalion_DescTextBox.Name = "battalion_DescTextBox";
            this.battalion_DescTextBox.Size = new System.Drawing.Size(211, 65);
            this.battalion_DescTextBox.TabIndex = 7;
            // 
            // button3
            // 
            this.button3.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button3.Location = new System.Drawing.Point(533, 293);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(75, 23);
            this.button3.TabIndex = 10;
            this.button3.Text = "Search";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // battalion_MasterBindingNavigator
            // 
            this.battalion_MasterBindingNavigator.AddNewItem = this.bindingNavigatorAddNewItem;
            this.battalion_MasterBindingNavigator.BindingSource = this.battalion_MasterBindingSource;
            this.battalion_MasterBindingNavigator.CountItem = this.bindingNavigatorCountItem;
            this.battalion_MasterBindingNavigator.DeleteItem = null;
            this.battalion_MasterBindingNavigator.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.bindingNavigatorMoveFirstItem,
            this.bindingNavigatorMovePreviousItem,
            this.bindingNavigatorSeparator,
            this.bindingNavigatorPositionItem,
            this.bindingNavigatorCountItem,
            this.bindingNavigatorSeparator1,
            this.bindingNavigatorMoveNextItem,
            this.bindingNavigatorMoveLastItem,
            this.bindingNavigatorSeparator2,
            this.bindingNavigatorAddNewItem,
            this.toolStripButton1,
            this.toolStripButton2,
            this.battalion_MasterBindingNavigatorSaveItem});
            this.battalion_MasterBindingNavigator.Location = new System.Drawing.Point(0, 0);
            this.battalion_MasterBindingNavigator.MoveFirstItem = this.bindingNavigatorMoveFirstItem;
            this.battalion_MasterBindingNavigator.MoveLastItem = this.bindingNavigatorMoveLastItem;
            this.battalion_MasterBindingNavigator.MoveNextItem = this.bindingNavigatorMoveNextItem;
            this.battalion_MasterBindingNavigator.MovePreviousItem = this.bindingNavigatorMovePreviousItem;
            this.battalion_MasterBindingNavigator.Name = "battalion_MasterBindingNavigator";
            this.battalion_MasterBindingNavigator.PositionItem = this.bindingNavigatorPositionItem;
            this.battalion_MasterBindingNavigator.Size = new System.Drawing.Size(684, 25);
            this.battalion_MasterBindingNavigator.TabIndex = 13;
            this.battalion_MasterBindingNavigator.Text = "bindingNavigator1";
            // 
            // bindingNavigatorAddNewItem
            // 
            this.bindingNavigatorAddNewItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorAddNewItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorAddNewItem.Image")));
            this.bindingNavigatorAddNewItem.Name = "bindingNavigatorAddNewItem";
            this.bindingNavigatorAddNewItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorAddNewItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorAddNewItem.Text = "Add new data";
            this.bindingNavigatorAddNewItem.Click += new System.EventHandler(this.bindingNavigatorAddNewItem_Click);
            // 
            // bindingNavigatorCountItem
            // 
            this.bindingNavigatorCountItem.Name = "bindingNavigatorCountItem";
            this.bindingNavigatorCountItem.Size = new System.Drawing.Size(35, 22);
            this.bindingNavigatorCountItem.Text = "of {0}";
            this.bindingNavigatorCountItem.ToolTipText = "Total number of items";
            // 
            // bindingNavigatorMoveFirstItem
            // 
            this.bindingNavigatorMoveFirstItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMoveFirstItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveFirstItem.Image")));
            this.bindingNavigatorMoveFirstItem.Name = "bindingNavigatorMoveFirstItem";
            this.bindingNavigatorMoveFirstItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMoveFirstItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorMoveFirstItem.Text = "Move first";
            // 
            // bindingNavigatorMovePreviousItem
            // 
            this.bindingNavigatorMovePreviousItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMovePreviousItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMovePreviousItem.Image")));
            this.bindingNavigatorMovePreviousItem.Name = "bindingNavigatorMovePreviousItem";
            this.bindingNavigatorMovePreviousItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMovePreviousItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorMovePreviousItem.Text = "Move previous";
            // 
            // bindingNavigatorSeparator
            // 
            this.bindingNavigatorSeparator.Name = "bindingNavigatorSeparator";
            this.bindingNavigatorSeparator.Size = new System.Drawing.Size(6, 25);
            // 
            // bindingNavigatorPositionItem
            // 
            this.bindingNavigatorPositionItem.AccessibleName = "Position";
            this.bindingNavigatorPositionItem.AutoSize = false;
            this.bindingNavigatorPositionItem.Name = "bindingNavigatorPositionItem";
            this.bindingNavigatorPositionItem.Size = new System.Drawing.Size(50, 23);
            this.bindingNavigatorPositionItem.Text = "0";
            this.bindingNavigatorPositionItem.ToolTipText = "Current position";
            // 
            // bindingNavigatorSeparator1
            // 
            this.bindingNavigatorSeparator1.Name = "bindingNavigatorSeparator1";
            this.bindingNavigatorSeparator1.Size = new System.Drawing.Size(6, 25);
            // 
            // bindingNavigatorMoveNextItem
            // 
            this.bindingNavigatorMoveNextItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMoveNextItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveNextItem.Image")));
            this.bindingNavigatorMoveNextItem.Name = "bindingNavigatorMoveNextItem";
            this.bindingNavigatorMoveNextItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMoveNextItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorMoveNextItem.Text = "Move next";
            // 
            // bindingNavigatorMoveLastItem
            // 
            this.bindingNavigatorMoveLastItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMoveLastItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveLastItem.Image")));
            this.bindingNavigatorMoveLastItem.Name = "bindingNavigatorMoveLastItem";
            this.bindingNavigatorMoveLastItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMoveLastItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorMoveLastItem.Text = "Move last";
            // 
            // bindingNavigatorSeparator2
            // 
            this.bindingNavigatorSeparator2.Name = "bindingNavigatorSeparator2";
            this.bindingNavigatorSeparator2.Size = new System.Drawing.Size(6, 25);
            // 
            // toolStripButton1
            // 
            this.toolStripButton1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton1.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton1.Image")));
            this.toolStripButton1.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton1.Name = "toolStripButton1";
            this.toolStripButton1.Size = new System.Drawing.Size(23, 22);
            this.toolStripButton1.Text = "Delete";
            this.toolStripButton1.Click += new System.EventHandler(this.toolStripButton1_Click);
            // 
            // toolStripButton2
            // 
            this.toolStripButton2.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton2.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton2.Image")));
            this.toolStripButton2.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton2.Name = "toolStripButton2";
            this.toolStripButton2.Size = new System.Drawing.Size(23, 22);
            this.toolStripButton2.Text = "Update";
            this.toolStripButton2.Click += new System.EventHandler(this.toolStripButton2_Click);
            // 
            // battalion_MasterBindingNavigatorSaveItem
            // 
            this.battalion_MasterBindingNavigatorSaveItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.battalion_MasterBindingNavigatorSaveItem.Image = ((System.Drawing.Image)(resources.GetObject("battalion_MasterBindingNavigatorSaveItem.Image")));
            this.battalion_MasterBindingNavigatorSaveItem.Name = "battalion_MasterBindingNavigatorSaveItem";
            this.battalion_MasterBindingNavigatorSaveItem.Size = new System.Drawing.Size(23, 22);
            this.battalion_MasterBindingNavigatorSaveItem.Text = "Save";
            this.battalion_MasterBindingNavigatorSaveItem.Click += new System.EventHandler(this.battalion_MasterBindingNavigatorSaveItem_Click_1);
            // 
            // button1
            // 
            this.button1.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.Location = new System.Drawing.Point(447, 159);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(129, 23);
            this.button1.TabIndex = 14;
            this.button1.Text = "Check Availability?";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click_1);
            // 
            // battalion_MasterTableAdapter
            // 
            this.battalion_MasterTableAdapter.ClearBeforeFill = true;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.ClientSize = new System.Drawing.Size(684, 487);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.battalion_MasterBindingNavigator);
            this.Controls.Add(this.button3);
            this.Controls.Add(battalion_IdLabel);
            this.Controls.Add(this.battalion_IdTextBox);
            this.Controls.Add(battalion_NameLabel);
            this.Controls.Add(this.battalion_NameTextBox);
            this.Controls.Add(battalion_DescLabel);
            this.Controls.Add(this.battalion_DescTextBox);
            this.Controls.Add(this.label1);
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Battalion_Master";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.battalion_MasterBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.battalion_MasterBindingNavigator)).EndInit();
            this.battalion_MasterBindingNavigator.ResumeLayout(false);
            this.battalion_MasterBindingNavigator.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private DataSet1 dataSet1;
        private System.Windows.Forms.BindingSource battalion_MasterBindingSource;
        private Military_Project.DataSet1TableAdapters.Battalion_MasterTableAdapter battalion_MasterTableAdapter;
        private System.Windows.Forms.TextBox battalion_IdTextBox;
        private System.Windows.Forms.TextBox battalion_NameTextBox;
        private System.Windows.Forms.TextBox battalion_DescTextBox;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.BindingNavigator battalion_MasterBindingNavigator;
        private System.Windows.Forms.ToolStripButton bindingNavigatorAddNewItem;
        private System.Windows.Forms.ToolStripLabel bindingNavigatorCountItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMoveFirstItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMovePreviousItem;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator;
        private System.Windows.Forms.ToolStripTextBox bindingNavigatorPositionItem;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator1;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMoveNextItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMoveLastItem;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator2;
        private System.Windows.Forms.ToolStripButton battalion_MasterBindingNavigatorSaveItem;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.ToolStripButton toolStripButton1;
        private System.Windows.Forms.ToolStripButton toolStripButton2;
    }
}

