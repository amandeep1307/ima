using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Military_Project
{
    public partial class Form1: Form
    {
        public Form1()
        {
            InitializeComponent();
        }
        DataSet1TableAdapters.Battalion_MasterTableAdapter a1 = new Military_Project.DataSet1TableAdapters.Battalion_MasterTableAdapter();
        
        private void battalion_MasterBindingNavigatorSaveItem_Click(object sender, EventArgs e)
        {
            this.Validate();
            this.battalion_MasterBindingSource.EndEdit();
            this.battalion_MasterTableAdapter.Update(this.dataSet1.Battalion_Master);

        }

        private void Form1_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'dataSet1.Battalion_Master' table. You can move, or remove it, as needed.
            this.battalion_MasterTableAdapter.Fill(this.dataSet1.Battalion_Master);        
            //battalion_IdTextBox.Clear();
            //battalion_DescTextBox.Clear();
            //battalion_NameTextBox.Clear();
        }
       
        private void button3_Click(object sender, EventArgs e)
        {
            Bsearch B1 = new Bsearch();
            B1.ShowDialog();
        }

        private void battalion_MasterBindingNavigatorSaveItem_Click_1(object sender, EventArgs e)
        {
            if(battalion_IdTextBox.Text.Length!=0 && battalion_NameTextBox.Text.Length!=0 && battalion_DescTextBox.Text.Length!=0)
            {
                a1.insert_battalion(battalion_IdTextBox.Text, battalion_NameTextBox.Text, battalion_DescTextBox.Text);
                MessageBox.Show("Record Inserted Successfully !");    
            }
            else
            {
                MessageBox.Show("Please enter all values", "Warning",MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        private void toolStripButton2_Click(object sender, EventArgs e)
        {
            a1.update_battalion(battalion_IdTextBox.Text, battalion_NameTextBox.Text, battalion_DescTextBox.Text);
            MessageBox.Show("Record Updated Successfully !");
        }

        private void toolStripButton1_Click(object sender, EventArgs e)
        {
            a1.delete_battalion(battalion_IdTextBox.Text);
            MessageBox.Show("Record Deleted Successfully !");
        }
        private void bindingNavigatorAddNewItem_Click(object sender, EventArgs e)
        {

        }
        private void button1_Click_1(object sender, EventArgs e)
        {
            DataTable dt = new DataTable();
            dt = a1.search_battalion(battalion_IdTextBox.Text);
            if (dt.Rows.Count > 0)
            {
                MessageBox.Show("Already Exists !");
                battalion_IdTextBox.Text = "";
                battalion_IdTextBox.Focus();
            }
            else
            {
                MessageBox.Show("Battalion_Id Available !");
            }
        }

        
    }
}