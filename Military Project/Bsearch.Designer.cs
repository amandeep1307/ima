namespace Military_Project
{
    partial class Bsearch
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.Label battalion_IdLabel;
            this.dataSet1 = new Military_Project.DataSet1();
            this.battalion_MasterBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.battalion_MasterTableAdapter = new Military_Project.DataSet1TableAdapters.Battalion_MasterTableAdapter();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.battalionMasterBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.battalion_MasterDataGridView = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.label1 = new System.Windows.Forms.Label();
            battalion_IdLabel = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.battalion_MasterBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.battalionMasterBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.battalion_MasterDataGridView)).BeginInit();
            this.SuspendLayout();
            // 
            // battalion_IdLabel
            // 
            battalion_IdLabel.AutoSize = true;
            battalion_IdLabel.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            battalion_IdLabel.Location = new System.Drawing.Point(207, 145);
            battalion_IdLabel.Name = "battalion_IdLabel";
            battalion_IdLabel.Size = new System.Drawing.Size(79, 17);
            battalion_IdLabel.TabIndex = 1;
            battalion_IdLabel.Text = "Battalion Id:";
            // 
            // dataSet1
            // 
            this.dataSet1.DataSetName = "DataSet1";
            this.dataSet1.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // battalion_MasterBindingSource
            // 
            this.battalion_MasterBindingSource.DataMember = "Battalion_Master";
            this.battalion_MasterBindingSource.DataSource = this.dataSet1;
            // 
            // battalion_MasterTableAdapter
            // 
            this.battalion_MasterTableAdapter.ClearBeforeFill = true;
            // 
            // comboBox1
            // 
            this.comboBox1.DataSource = this.battalionMasterBindingSource;
            this.comboBox1.DisplayMember = "Battalion_Id";
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Location = new System.Drawing.Point(307, 144);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(154, 21);
            this.comboBox1.TabIndex = 7;
            this.comboBox1.SelectedIndexChanged += new System.EventHandler(this.comboBox1_SelectedIndexChanged);
            // 
            // battalionMasterBindingSource
            // 
            this.battalionMasterBindingSource.DataMember = "Battalion_Master";
            this.battalionMasterBindingSource.DataSource = this.dataSet1;
            // 
            // battalion_MasterDataGridView
            // 
            this.battalion_MasterDataGridView.AutoGenerateColumns = false;
            this.battalion_MasterDataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn1,
            this.dataGridViewTextBoxColumn2,
            this.dataGridViewTextBoxColumn3});
            this.battalion_MasterDataGridView.DataSource = this.battalion_MasterBindingSource;
            this.battalion_MasterDataGridView.Location = new System.Drawing.Point(167, 198);
            this.battalion_MasterDataGridView.Name = "battalion_MasterDataGridView";
            this.battalion_MasterDataGridView.Size = new System.Drawing.Size(344, 220);
            this.battalion_MasterDataGridView.TabIndex = 7;
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.DataPropertyName = "Battalion_Id";
            this.dataGridViewTextBoxColumn1.HeaderText = "Battalion_Id";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.DataPropertyName = "Battalion_Name";
            this.dataGridViewTextBoxColumn2.HeaderText = "Battalion_Name";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.DataPropertyName = "Battalion_Desc";
            this.dataGridViewTextBoxColumn3.HeaderText = "Battalion_Desc";
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Times New Roman", 15.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(247, 56);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(160, 24);
            this.label1.TabIndex = 8;
            this.label1.Text = "Battalion Search";
            // 
            // Bsearch
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(684, 487);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.battalion_MasterDataGridView);
            this.Controls.Add(this.comboBox1);
            this.Controls.Add(battalion_IdLabel);
            this.Name = "Bsearch";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Battalion_Search";
            this.Load += new System.EventHandler(this.Bsearch_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataSet1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.battalion_MasterBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.battalionMasterBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.battalion_MasterDataGridView)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DataSet1 dataSet1;
        private System.Windows.Forms.BindingSource battalion_MasterBindingSource;
        private Military_Project.DataSet1TableAdapters.Battalion_MasterTableAdapter battalion_MasterTableAdapter;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.DataGridView battalion_MasterDataGridView;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private System.Windows.Forms.BindingSource battalionMasterBindingSource;
        private System.Windows.Forms.Label label1;
    }
}