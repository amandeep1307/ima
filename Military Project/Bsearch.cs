using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Military_Project
{
    public partial class Bsearch : Form
    {
        public Bsearch()
        {
            InitializeComponent();
        }
        DataSet1TableAdapters.Battalion_MasterTableAdapter BS = new Military_Project.DataSet1TableAdapters.Battalion_MasterTableAdapter();

        private void Bsearch_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'dataSet1.Battalion_Master' table. You can move, or remove it, as needed.
            this.battalion_MasterTableAdapter.Fill(this.dataSet1.Battalion_Master);
            comboBox1.Text="";
        }

        private void battalion_MasterBindingNavigatorSaveItem_Click(object sender, EventArgs e)
        {
            this.Validate();
            this.battalion_MasterBindingSource.EndEdit();
            this.battalion_MasterTableAdapter.Update(this.dataSet1.Battalion_Master);

        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            DataTable dt = new DataTable();
            dt = BS.search_battalion(comboBox1.Text);
            if (dt.Rows.Count>0)
            {
                battalion_MasterDataGridView.DataSource = dt;
            }
        }
    }
}