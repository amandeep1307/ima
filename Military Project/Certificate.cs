using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Military_Project
{
    public partial class Certificate : Form
    {
        public Certificate()
        {
            InitializeComponent();
        }

        private void Certificate_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'dataSet1.Service_Record' table. You can move, or remove it, as needed.
            this.service_RecordTableAdapter.Fill(this.dataSet1.Service_Record);
           
       }

        private void crystalReportViewer1_Load(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'dataSet1.Soldier_Master' table. You can move, or remove it, as needed.
            this.soldier_MasterTableAdapter.Fill(this.dataSet1.Soldier_Master);
            CrystalReport1 cr1 = new CrystalReport1();
            cr1.Load("D:/c # .net complete/Military Project/Military Project/Military Project/CrystalReport1.rpt");
            // DataSet1TableAdapters.Company_MasterTableAdapter c1 = new Military_Project.DataSet1TableAdapters.Company_MasterTableAdapter();
            DataSet1TableAdapters.Service_RecordTableAdapter  c2 = new Military_Project.DataSet1TableAdapters.Service_RecordTableAdapter();
            DataSet1TableAdapters.Embodiment_PeriodTableAdapter k1 = new Military_Project.DataSet1TableAdapters.Embodiment_PeriodTableAdapter();  
            // DataTable dt = new DataTable();
            // dt = c1.GetData();
            DataTable dt1 = new DataTable();
            dt1.Rows.Clear(); 
           // Retirement_Certificate rc = new Retirement_Certificate();
            dt1 = c2.sum_date(comboBox1.Text);   
            // cr1.SetDataSource(dt);
           int days,years;
           days = Convert.ToInt32(dt1.Rows[0][8]);
           years = Convert.ToInt32(dt1.Rows[0][7]);
           if (days > 365)
           {

               years = years + 1;
               days = days%365;
           }
          


             k1.InsertQuery(Convert.ToInt64(dt1.Rows[0][0]),Convert.ToInt64(years),Convert.ToInt64(days),(dt1.Rows[0][9].ToString()),(dt1.Rows[0][10].ToString()),(dt1.Rows[0][11].ToString())); 
            
            cr1.SetDataSource(dt1);
            cr1.Refresh();
            crystalReportViewer1.ReportSource = cr1;






        }

        private void crystalReportViewer1_Load_1(object sender, EventArgs e)
        {

        }
    }
}