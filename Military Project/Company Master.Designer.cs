namespace Military_Project
{
    partial class Form2
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.Label company_IdLabel;
            System.Windows.Forms.Label company_NameLabel;
            System.Windows.Forms.Label company_DescLabel;
            System.Windows.Forms.Label battalion_IdLabel1;
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form2));
            this.label1 = new System.Windows.Forms.Label();
            this.company_MasterBindingNavigator = new System.Windows.Forms.BindingNavigator(this.components);
            this.bindingNavigatorAddNewItem = new System.Windows.Forms.ToolStripButton();
            this.company_MasterBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataSet1 = new Military_Project.DataSet1();
            this.bindingNavigatorCountItem = new System.Windows.Forms.ToolStripLabel();
            this.bindingNavigatorMoveFirstItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorMovePreviousItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorSeparator = new System.Windows.Forms.ToolStripSeparator();
            this.bindingNavigatorPositionItem = new System.Windows.Forms.ToolStripTextBox();
            this.bindingNavigatorSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.bindingNavigatorMoveNextItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorMoveLastItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripButton1 = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton2 = new System.Windows.Forms.ToolStripButton();
            this.company_MasterBindingNavigatorSaveItem = new System.Windows.Forms.ToolStripButton();
            this.company_IdTextBox = new System.Windows.Forms.TextBox();
            this.company_NameTextBox = new System.Windows.Forms.TextBox();
            this.company_DescTextBox = new System.Windows.Forms.TextBox();
            this.button3 = new System.Windows.Forms.Button();
            this.company_MasterTableAdapter = new Military_Project.DataSet1TableAdapters.Company_MasterTableAdapter();
            this.button1 = new System.Windows.Forms.Button();
            this.battalion_IdComboBox = new System.Windows.Forms.ComboBox();
            this.battalionMasterBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.battalion_MasterTableAdapter = new Military_Project.DataSet1TableAdapters.Battalion_MasterTableAdapter();
            company_IdLabel = new System.Windows.Forms.Label();
            company_NameLabel = new System.Windows.Forms.Label();
            company_DescLabel = new System.Windows.Forms.Label();
            battalion_IdLabel1 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.company_MasterBindingNavigator)).BeginInit();
            this.company_MasterBindingNavigator.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.company_MasterBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.battalionMasterBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // company_IdLabel
            // 
            company_IdLabel.AutoSize = true;
            company_IdLabel.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            company_IdLabel.Location = new System.Drawing.Point(127, 203);
            company_IdLabel.Name = "company_IdLabel";
            company_IdLabel.Size = new System.Drawing.Size(83, 17);
            company_IdLabel.TabIndex = 4;
            company_IdLabel.Text = "Company Id:";
            // 
            // company_NameLabel
            // 
            company_NameLabel.AutoSize = true;
            company_NameLabel.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            company_NameLabel.Location = new System.Drawing.Point(127, 243);
            company_NameLabel.Name = "company_NameLabel";
            company_NameLabel.Size = new System.Drawing.Size(107, 17);
            company_NameLabel.TabIndex = 6;
            company_NameLabel.Text = "Company Name:";
            // 
            // company_DescLabel
            // 
            company_DescLabel.AutoSize = true;
            company_DescLabel.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            company_DescLabel.Location = new System.Drawing.Point(127, 288);
            company_DescLabel.Name = "company_DescLabel";
            company_DescLabel.Size = new System.Drawing.Size(138, 17);
            company_DescLabel.TabIndex = 8;
            company_DescLabel.Text = "Company Description:";
            // 
            // battalion_IdLabel1
            // 
            battalion_IdLabel1.AutoSize = true;
            battalion_IdLabel1.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            battalion_IdLabel1.Location = new System.Drawing.Point(127, 161);
            battalion_IdLabel1.Name = "battalion_IdLabel1";
            battalion_IdLabel1.Size = new System.Drawing.Size(79, 17);
            battalion_IdLabel1.TabIndex = 15;
            battalion_IdLabel1.Text = "Battalion Id:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Times New Roman", 15.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(271, 63);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(164, 24);
            this.label1.TabIndex = 0;
            this.label1.Text = "Company Master";
            // 
            // company_MasterBindingNavigator
            // 
            this.company_MasterBindingNavigator.AddNewItem = this.bindingNavigatorAddNewItem;
            this.company_MasterBindingNavigator.BindingSource = this.company_MasterBindingSource;
            this.company_MasterBindingNavigator.CountItem = this.bindingNavigatorCountItem;
            this.company_MasterBindingNavigator.DeleteItem = null;
            this.company_MasterBindingNavigator.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.bindingNavigatorMoveFirstItem,
            this.bindingNavigatorMovePreviousItem,
            this.bindingNavigatorSeparator,
            this.bindingNavigatorPositionItem,
            this.bindingNavigatorCountItem,
            this.bindingNavigatorSeparator1,
            this.bindingNavigatorMoveNextItem,
            this.bindingNavigatorMoveLastItem,
            this.bindingNavigatorSeparator2,
            this.bindingNavigatorAddNewItem,
            this.toolStripButton1,
            this.toolStripButton2,
            this.company_MasterBindingNavigatorSaveItem});
            this.company_MasterBindingNavigator.Location = new System.Drawing.Point(0, 0);
            this.company_MasterBindingNavigator.MoveFirstItem = this.bindingNavigatorMoveFirstItem;
            this.company_MasterBindingNavigator.MoveLastItem = this.bindingNavigatorMoveLastItem;
            this.company_MasterBindingNavigator.MoveNextItem = this.bindingNavigatorMoveNextItem;
            this.company_MasterBindingNavigator.MovePreviousItem = this.bindingNavigatorMovePreviousItem;
            this.company_MasterBindingNavigator.Name = "company_MasterBindingNavigator";
            this.company_MasterBindingNavigator.PositionItem = this.bindingNavigatorPositionItem;
            this.company_MasterBindingNavigator.Size = new System.Drawing.Size(684, 25);
            this.company_MasterBindingNavigator.TabIndex = 1;
            this.company_MasterBindingNavigator.Text = "bindingNavigator1";
            // 
            // bindingNavigatorAddNewItem
            // 
            this.bindingNavigatorAddNewItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorAddNewItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorAddNewItem.Image")));
            this.bindingNavigatorAddNewItem.Name = "bindingNavigatorAddNewItem";
            this.bindingNavigatorAddNewItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorAddNewItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorAddNewItem.Text = "Add new data";
            this.bindingNavigatorAddNewItem.Click += new System.EventHandler(this.bindingNavigatorAddNewItem_Click);
            // 
            // company_MasterBindingSource
            // 
            this.company_MasterBindingSource.DataMember = "Company_Master";
            this.company_MasterBindingSource.DataSource = this.dataSet1;
            // 
            // dataSet1
            // 
            this.dataSet1.DataSetName = "DataSet1";
            this.dataSet1.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // bindingNavigatorCountItem
            // 
            this.bindingNavigatorCountItem.Name = "bindingNavigatorCountItem";
            this.bindingNavigatorCountItem.Size = new System.Drawing.Size(35, 22);
            this.bindingNavigatorCountItem.Text = "of {0}";
            this.bindingNavigatorCountItem.ToolTipText = "Total number of items";
            // 
            // bindingNavigatorMoveFirstItem
            // 
            this.bindingNavigatorMoveFirstItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMoveFirstItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveFirstItem.Image")));
            this.bindingNavigatorMoveFirstItem.Name = "bindingNavigatorMoveFirstItem";
            this.bindingNavigatorMoveFirstItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMoveFirstItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorMoveFirstItem.Text = "Move first";
            // 
            // bindingNavigatorMovePreviousItem
            // 
            this.bindingNavigatorMovePreviousItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMovePreviousItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMovePreviousItem.Image")));
            this.bindingNavigatorMovePreviousItem.Name = "bindingNavigatorMovePreviousItem";
            this.bindingNavigatorMovePreviousItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMovePreviousItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorMovePreviousItem.Text = "Move previous";
            // 
            // bindingNavigatorSeparator
            // 
            this.bindingNavigatorSeparator.Name = "bindingNavigatorSeparator";
            this.bindingNavigatorSeparator.Size = new System.Drawing.Size(6, 25);
            // 
            // bindingNavigatorPositionItem
            // 
            this.bindingNavigatorPositionItem.AccessibleName = "Position";
            this.bindingNavigatorPositionItem.AutoSize = false;
            this.bindingNavigatorPositionItem.Name = "bindingNavigatorPositionItem";
            this.bindingNavigatorPositionItem.Size = new System.Drawing.Size(50, 23);
            this.bindingNavigatorPositionItem.Text = "0";
            this.bindingNavigatorPositionItem.ToolTipText = "Current position";
            // 
            // bindingNavigatorSeparator1
            // 
            this.bindingNavigatorSeparator1.Name = "bindingNavigatorSeparator1";
            this.bindingNavigatorSeparator1.Size = new System.Drawing.Size(6, 25);
            // 
            // bindingNavigatorMoveNextItem
            // 
            this.bindingNavigatorMoveNextItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMoveNextItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveNextItem.Image")));
            this.bindingNavigatorMoveNextItem.Name = "bindingNavigatorMoveNextItem";
            this.bindingNavigatorMoveNextItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMoveNextItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorMoveNextItem.Text = "Move next";
            // 
            // bindingNavigatorMoveLastItem
            // 
            this.bindingNavigatorMoveLastItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMoveLastItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveLastItem.Image")));
            this.bindingNavigatorMoveLastItem.Name = "bindingNavigatorMoveLastItem";
            this.bindingNavigatorMoveLastItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMoveLastItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorMoveLastItem.Text = "Move last";
            // 
            // bindingNavigatorSeparator2
            // 
            this.bindingNavigatorSeparator2.Name = "bindingNavigatorSeparator2";
            this.bindingNavigatorSeparator2.Size = new System.Drawing.Size(6, 25);
            // 
            // toolStripButton1
            // 
            this.toolStripButton1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton1.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton1.Image")));
            this.toolStripButton1.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton1.Name = "toolStripButton1";
            this.toolStripButton1.Size = new System.Drawing.Size(23, 22);
            this.toolStripButton1.Text = "Delete";
            this.toolStripButton1.Click += new System.EventHandler(this.toolStripButton1_Click);
            // 
            // toolStripButton2
            // 
            this.toolStripButton2.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton2.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton2.Image")));
            this.toolStripButton2.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton2.Name = "toolStripButton2";
            this.toolStripButton2.Size = new System.Drawing.Size(23, 22);
            this.toolStripButton2.Text = "Update";
            this.toolStripButton2.Click += new System.EventHandler(this.toolStripButton2_Click);
            // 
            // company_MasterBindingNavigatorSaveItem
            // 
            this.company_MasterBindingNavigatorSaveItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.company_MasterBindingNavigatorSaveItem.Image = ((System.Drawing.Image)(resources.GetObject("company_MasterBindingNavigatorSaveItem.Image")));
            this.company_MasterBindingNavigatorSaveItem.Name = "company_MasterBindingNavigatorSaveItem";
            this.company_MasterBindingNavigatorSaveItem.Size = new System.Drawing.Size(23, 22);
            this.company_MasterBindingNavigatorSaveItem.Text = "Save";
            this.company_MasterBindingNavigatorSaveItem.Click += new System.EventHandler(this.company_MasterBindingNavigatorSaveItem_Click);
            // 
            // company_IdTextBox
            // 
            this.company_IdTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.company_MasterBindingSource, "Company_Id", true));
            this.company_IdTextBox.Location = new System.Drawing.Point(291, 202);
            this.company_IdTextBox.Name = "company_IdTextBox";
            this.company_IdTextBox.Size = new System.Drawing.Size(170, 20);
            this.company_IdTextBox.TabIndex = 5;
            // 
            // company_NameTextBox
            // 
            this.company_NameTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.company_MasterBindingSource, "Company_Name", true));
            this.company_NameTextBox.Location = new System.Drawing.Point(291, 242);
            this.company_NameTextBox.Name = "company_NameTextBox";
            this.company_NameTextBox.Size = new System.Drawing.Size(170, 20);
            this.company_NameTextBox.TabIndex = 7;
            // 
            // company_DescTextBox
            // 
            this.company_DescTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.company_MasterBindingSource, "Company_Desc", true));
            this.company_DescTextBox.Location = new System.Drawing.Point(291, 284);
            this.company_DescTextBox.Multiline = true;
            this.company_DescTextBox.Name = "company_DescTextBox";
            this.company_DescTextBox.Size = new System.Drawing.Size(197, 61);
            this.company_DescTextBox.TabIndex = 9;
            // 
            // button3
            // 
            this.button3.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button3.Location = new System.Drawing.Point(520, 322);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(75, 23);
            this.button3.TabIndex = 14;
            this.button3.Text = "Search";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // company_MasterTableAdapter
            // 
            this.company_MasterTableAdapter.ClearBeforeFill = true;
            // 
            // button1
            // 
            this.button1.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.Location = new System.Drawing.Point(467, 199);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(128, 23);
            this.button1.TabIndex = 15;
            this.button1.Text = "Check Availability?";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // battalion_IdComboBox
            // 
            this.battalion_IdComboBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.company_MasterBindingSource, "Battalion_Id", true));
            this.battalion_IdComboBox.DataSource = this.battalionMasterBindingSource;
            this.battalion_IdComboBox.DisplayMember = "Battalion_Id";
            this.battalion_IdComboBox.FormattingEnabled = true;
            this.battalion_IdComboBox.Location = new System.Drawing.Point(291, 160);
            this.battalion_IdComboBox.Name = "battalion_IdComboBox";
            this.battalion_IdComboBox.Size = new System.Drawing.Size(170, 21);
            this.battalion_IdComboBox.TabIndex = 16;
            // 
            // battalionMasterBindingSource
            // 
            this.battalionMasterBindingSource.DataMember = "Battalion_Master";
            this.battalionMasterBindingSource.DataSource = this.dataSet1;
            // 
            // battalion_MasterTableAdapter
            // 
            this.battalion_MasterTableAdapter.ClearBeforeFill = true;
            // 
            // Form2
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(684, 487);
            this.Controls.Add(battalion_IdLabel1);
            this.Controls.Add(this.battalion_IdComboBox);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.button3);
            this.Controls.Add(company_IdLabel);
            this.Controls.Add(this.company_IdTextBox);
            this.Controls.Add(company_NameLabel);
            this.Controls.Add(this.company_NameTextBox);
            this.Controls.Add(company_DescLabel);
            this.Controls.Add(this.company_DescTextBox);
            this.Controls.Add(this.company_MasterBindingNavigator);
            this.Controls.Add(this.label1);
            this.Name = "Form2";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Company_Master";
            this.Load += new System.EventHandler(this.Form2_Load);
            ((System.ComponentModel.ISupportInitialize)(this.company_MasterBindingNavigator)).EndInit();
            this.company_MasterBindingNavigator.ResumeLayout(false);
            this.company_MasterBindingNavigator.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.company_MasterBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.battalionMasterBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private DataSet1 dataSet1;
        private System.Windows.Forms.BindingSource company_MasterBindingSource;
        private Military_Project.DataSet1TableAdapters.Company_MasterTableAdapter company_MasterTableAdapter;
        private System.Windows.Forms.BindingNavigator company_MasterBindingNavigator;
        private System.Windows.Forms.ToolStripButton bindingNavigatorAddNewItem;
        private System.Windows.Forms.ToolStripLabel bindingNavigatorCountItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMoveFirstItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMovePreviousItem;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator;
        private System.Windows.Forms.ToolStripTextBox bindingNavigatorPositionItem;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator1;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMoveNextItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMoveLastItem;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator2;
        private System.Windows.Forms.ToolStripButton company_MasterBindingNavigatorSaveItem;
        private System.Windows.Forms.TextBox company_IdTextBox;
        private System.Windows.Forms.TextBox company_NameTextBox;
        private System.Windows.Forms.TextBox company_DescTextBox;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.ToolStripButton toolStripButton1;
        private System.Windows.Forms.ToolStripButton toolStripButton2;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.ComboBox battalion_IdComboBox;
        private System.Windows.Forms.BindingSource battalionMasterBindingSource;
        private Military_Project.DataSet1TableAdapters.Battalion_MasterTableAdapter battalion_MasterTableAdapter;
    }
}