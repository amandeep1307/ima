using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Military_Project
{
    public partial class Form2: Form
    {
        public Form2()
        {
            InitializeComponent();
        }
        DataSet1TableAdapters.Company_MasterTableAdapter a1 = new Military_Project.DataSet1TableAdapters.Company_MasterTableAdapter();
        private void clearfn()
        {
            company_IdTextBox.Clear();
            company_DescTextBox.Clear();
            company_NameTextBox.Clear();
            battalion_IdComboBox.Text = "";
        }
        private void company_MasterBindingNavigatorSaveItem_Click(object sender, EventArgs e)
        {
            if (battalion_IdComboBox.Text.Length != 0 && company_IdTextBox.Text.Length != 0 && company_NameTextBox.Text.Length != 0 && company_DescTextBox.Text.Length != 0)
            {
                a1.insert_company(battalion_IdComboBox.Text, company_IdTextBox.Text, company_NameTextBox.Text, company_DescTextBox.Text);
                MessageBox.Show("Record Inserted Successfully !");
            }
            else
            {
                MessageBox.Show("Please enter all values !", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }
        private void Form2_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'dataSet1.Battalion_Master' table. You can move, or remove it, as needed.
            this.battalion_MasterTableAdapter.Fill(this.dataSet1.Battalion_Master);
            // TODO: This line of code loads data into the 'dataSet1.Company_Master' table. You can move, or remove it, as needed.
            this.company_MasterTableAdapter.Fill(this.dataSet1.Company_Master);
            //clearfn();
        }
        private void button3_Click(object sender, EventArgs e)
        {
            Csearch C1 = new Csearch();
            C1.ShowDialog();
            
        }

        private void toolStripButton2_Click(object sender, EventArgs e)
        {
            a1.update_company(battalion_IdComboBox.Text, company_IdTextBox.Text, company_NameTextBox.Text, company_DescTextBox.Text);
            MessageBox.Show("Record Updated Successfully !");
        }

        private void toolStripButton1_Click(object sender, EventArgs e)
        {
            a1.delete_company(company_IdTextBox.Text);
            MessageBox.Show("Record Deleted Successfully !");
        }

        private void button1_Click(object sender, EventArgs e)
        {
            DataTable dt = new DataTable();
            dt = a1.search_company(company_IdTextBox.Text);
            if (dt.Rows.Count > 0)
            {
                MessageBox.Show("Already Exists !");
                company_IdTextBox.Text = "";
                company_IdTextBox.Focus();
            }
            else
            {
                MessageBox.Show("Company_Id Available !");
            }
        }

        private void bindingNavigatorAddNewItem_Click(object sender, EventArgs e)
        {

        }
    }
}