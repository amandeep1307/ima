namespace Military_Project
{
    partial class Csearch
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.Label company_IdLabel;
            this.dataSet1 = new Military_Project.DataSet1();
            this.company_MasterBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.company_MasterTableAdapter = new Military_Project.DataSet1TableAdapters.Company_MasterTableAdapter();
            this.company_MasterDataGridView = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            company_IdLabel = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.company_MasterBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.company_MasterDataGridView)).BeginInit();
            this.SuspendLayout();
            // 
            // company_IdLabel
            // 
            company_IdLabel.AutoSize = true;
            company_IdLabel.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            company_IdLabel.Location = new System.Drawing.Point(211, 147);
            company_IdLabel.Name = "company_IdLabel";
            company_IdLabel.Size = new System.Drawing.Size(83, 17);
            company_IdLabel.TabIndex = 2;
            company_IdLabel.Text = "Company Id:";
            // 
            // dataSet1
            // 
            this.dataSet1.DataSetName = "DataSet1";
            this.dataSet1.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // company_MasterBindingSource
            // 
            this.company_MasterBindingSource.DataMember = "Company_Master";
            this.company_MasterBindingSource.DataSource = this.dataSet1;
            // 
            // company_MasterTableAdapter
            // 
            this.company_MasterTableAdapter.ClearBeforeFill = true;
            // 
            // company_MasterDataGridView
            // 
            this.company_MasterDataGridView.AutoGenerateColumns = false;
            this.company_MasterDataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn1,
            this.dataGridViewTextBoxColumn2,
            this.dataGridViewTextBoxColumn3,
            this.dataGridViewTextBoxColumn4});
            this.company_MasterDataGridView.DataSource = this.company_MasterBindingSource;
            this.company_MasterDataGridView.Location = new System.Drawing.Point(119, 208);
            this.company_MasterDataGridView.Name = "company_MasterDataGridView";
            this.company_MasterDataGridView.Size = new System.Drawing.Size(443, 188);
            this.company_MasterDataGridView.TabIndex = 1;
            this.company_MasterDataGridView.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.company_MasterDataGridView_CellContentClick);
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.DataPropertyName = "Battalion_Id";
            this.dataGridViewTextBoxColumn1.HeaderText = "Battalion_Id";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.DataPropertyName = "Company_Id";
            this.dataGridViewTextBoxColumn2.HeaderText = "Company_Id";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.DataPropertyName = "Company_Name";
            this.dataGridViewTextBoxColumn3.HeaderText = "Company_Name";
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            // 
            // dataGridViewTextBoxColumn4
            // 
            this.dataGridViewTextBoxColumn4.DataPropertyName = "Company_Desc";
            this.dataGridViewTextBoxColumn4.HeaderText = "Company_Desc";
            this.dataGridViewTextBoxColumn4.Name = "dataGridViewTextBoxColumn4";
            // 
            // comboBox1
            // 
            this.comboBox1.DataSource = this.company_MasterBindingSource;
            this.comboBox1.DisplayMember = "Company_Id";
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Location = new System.Drawing.Point(313, 146);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(145, 21);
            this.comboBox1.TabIndex = 3;
            this.comboBox1.SelectedIndexChanged += new System.EventHandler(this.comboBox1_SelectedIndexChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Times New Roman", 15.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(259, 56);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(163, 24);
            this.label1.TabIndex = 4;
            this.label1.Text = "Company Search";
            // 
            // Csearch
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(684, 487);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.comboBox1);
            this.Controls.Add(company_IdLabel);
            this.Controls.Add(this.company_MasterDataGridView);
            this.Name = "Csearch";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Company_Search";
            this.Load += new System.EventHandler(this.Csearch_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataSet1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.company_MasterBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.company_MasterDataGridView)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DataSet1 dataSet1;
        private System.Windows.Forms.BindingSource company_MasterBindingSource;
        private Military_Project.DataSet1TableAdapters.Company_MasterTableAdapter company_MasterTableAdapter;
        private System.Windows.Forms.DataGridView company_MasterDataGridView;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.Label label1;
    }
}