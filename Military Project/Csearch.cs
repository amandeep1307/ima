using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Military_Project
{
    public partial class Csearch : Form
    {
        public Csearch()
        {
            InitializeComponent();
        }
        DataSet1TableAdapters.Company_MasterTableAdapter CS = new Military_Project.DataSet1TableAdapters.Company_MasterTableAdapter();
        private void company_MasterBindingNavigatorSaveItem_Click(object sender, EventArgs e)
        {
            this.Validate();
            this.company_MasterBindingSource.EndEdit();
            this.company_MasterTableAdapter.Update(this.dataSet1.Company_Master);

        }

        private void Csearch_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'dataSet1.Company_Master' table. You can move, or remove it, as needed.
            this.company_MasterTableAdapter.Fill(this.dataSet1.Company_Master);

        }

        private void bindingNavigatorAddNewItem_Click(object sender, EventArgs e)
        {

        }

        private void company_IdComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            
        }

        private void company_MasterDataGridView_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            DataTable dt = new DataTable();
            dt = CS.search_company(comboBox1.Text);
            if (dt.Rows.Count > 0)
            {
                company_MasterDataGridView.DataSource = dt;
            }
        }

 
    }
}