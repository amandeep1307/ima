namespace Military_Project
{
    partial class ESearch
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.Label emp_IdLabel;
            this.dataSet1 = new Military_Project.DataSet1();
            this.employee_MasterBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.employee_MasterTableAdapter = new Military_Project.DataSet1TableAdapters.Employee_MasterTableAdapter();
            this.employee_MasterDataGridView = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.emp_IdComboBox = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            emp_IdLabel = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.employee_MasterBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.employee_MasterDataGridView)).BeginInit();
            this.SuspendLayout();
            // 
            // emp_IdLabel
            // 
            emp_IdLabel.AutoSize = true;
            emp_IdLabel.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            emp_IdLabel.Location = new System.Drawing.Point(232, 151);
            emp_IdLabel.Name = "emp_IdLabel";
            emp_IdLabel.Size = new System.Drawing.Size(54, 17);
            emp_IdLabel.TabIndex = 2;
            emp_IdLabel.Text = "Emp Id:";
            // 
            // dataSet1
            // 
            this.dataSet1.DataSetName = "DataSet1";
            this.dataSet1.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // employee_MasterBindingSource
            // 
            this.employee_MasterBindingSource.DataMember = "Employee_Master";
            this.employee_MasterBindingSource.DataSource = this.dataSet1;
            // 
            // employee_MasterTableAdapter
            // 
            this.employee_MasterTableAdapter.ClearBeforeFill = true;
            // 
            // employee_MasterDataGridView
            // 
            this.employee_MasterDataGridView.AutoGenerateColumns = false;
            this.employee_MasterDataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn1,
            this.dataGridViewTextBoxColumn2,
            this.dataGridViewTextBoxColumn3,
            this.dataGridViewTextBoxColumn4});
            this.employee_MasterDataGridView.DataSource = this.employee_MasterBindingSource;
            this.employee_MasterDataGridView.Location = new System.Drawing.Point(130, 193);
            this.employee_MasterDataGridView.Name = "employee_MasterDataGridView";
            this.employee_MasterDataGridView.Size = new System.Drawing.Size(443, 220);
            this.employee_MasterDataGridView.TabIndex = 1;
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.DataPropertyName = "Emp_Id";
            this.dataGridViewTextBoxColumn1.HeaderText = "Emp_Id";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.DataPropertyName = "Emp_Name";
            this.dataGridViewTextBoxColumn2.HeaderText = "Emp_Name";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.DataPropertyName = "Salary";
            this.dataGridViewTextBoxColumn3.HeaderText = "Salary";
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            // 
            // dataGridViewTextBoxColumn4
            // 
            this.dataGridViewTextBoxColumn4.DataPropertyName = "Designation";
            this.dataGridViewTextBoxColumn4.HeaderText = "Designation";
            this.dataGridViewTextBoxColumn4.Name = "dataGridViewTextBoxColumn4";
            // 
            // emp_IdComboBox
            // 
            this.emp_IdComboBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.employee_MasterBindingSource, "Emp_Id", true));
            this.emp_IdComboBox.DataSource = this.employee_MasterBindingSource;
            this.emp_IdComboBox.DisplayMember = "Emp_Id";
            this.emp_IdComboBox.FormattingEnabled = true;
            this.emp_IdComboBox.Location = new System.Drawing.Point(327, 150);
            this.emp_IdComboBox.Name = "emp_IdComboBox";
            this.emp_IdComboBox.Size = new System.Drawing.Size(143, 21);
            this.emp_IdComboBox.TabIndex = 3;
            this.emp_IdComboBox.SelectedIndexChanged += new System.EventHandler(this.emp_IdComboBox_SelectedIndexChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Times New Roman", 15.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(267, 50);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(165, 24);
            this.label1.TabIndex = 4;
            this.label1.Text = "Employee Search";
            // 
            // ESearch
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(684, 487);
            this.Controls.Add(this.label1);
            this.Controls.Add(emp_IdLabel);
            this.Controls.Add(this.emp_IdComboBox);
            this.Controls.Add(this.employee_MasterDataGridView);
            this.Name = "ESearch";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Employee_Search";
            this.Load += new System.EventHandler(this.ESearch_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataSet1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.employee_MasterBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.employee_MasterDataGridView)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DataSet1 dataSet1;
        private System.Windows.Forms.BindingSource employee_MasterBindingSource;
        private Military_Project.DataSet1TableAdapters.Employee_MasterTableAdapter employee_MasterTableAdapter;
        private System.Windows.Forms.DataGridView employee_MasterDataGridView;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
        private System.Windows.Forms.ComboBox emp_IdComboBox;
        private System.Windows.Forms.Label label1;
    }
}