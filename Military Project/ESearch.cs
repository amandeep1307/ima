using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Military_Project
{
    public partial class ESearch : Form
    {
        public ESearch()
        {
            InitializeComponent();
        }
        DataSet1TableAdapters.Employee_MasterTableAdapter ES = new Military_Project.DataSet1TableAdapters.Employee_MasterTableAdapter();


        private void employee_MasterBindingNavigatorSaveItem_Click(object sender, EventArgs e)
        {
            this.Validate();
            this.employee_MasterBindingSource.EndEdit();
            this.employee_MasterTableAdapter.Update(this.dataSet1.Employee_Master);

        }

        private void ESearch_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'dataSet1.Employee_Master' table. You can move, or remove it, as needed.
            this.employee_MasterTableAdapter.Fill(this.dataSet1.Employee_Master);
            emp_IdComboBox.Text = "";
        }

        private void emp_IdComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            DataTable dt = new DataTable();
            dt = ES.search_employee(emp_IdComboBox.Text);
            if (dt.Rows.Count > 0)
            {
                employee_MasterDataGridView.DataSource = dt;
            }
        }
    }
}