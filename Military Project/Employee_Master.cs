using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Military_Project
{
    public partial class Employee_Master : Form
    {
        public Employee_Master()
        {
            InitializeComponent();
        }
        DataSet1TableAdapters.Employee_MasterTableAdapter a1 = new Military_Project.DataSet1TableAdapters.Employee_MasterTableAdapter();

        private void clearfn()
        {
            emp_IdTextBox.Clear();
            emp_NameTextBox.Clear();
            salaryTextBox.Clear();
            designationTextBox.Clear();
        }
        private void employee_MasterBindingNavigatorSaveItem_Click(object sender, EventArgs e)
        {
            if (emp_IdTextBox.Text.Length != 0 && emp_NameTextBox.Text.Length != 0 && salaryTextBox.Text.Length != 0 && designationTextBox.Text.Length != 0)
            {
                a1.insert_employee(emp_IdTextBox.Text, emp_NameTextBox.Text, salaryTextBox.Text, designationTextBox.Text);
                MessageBox.Show("Record Inserted Successfully !");
            }
            else
            {
                MessageBox.Show("Please enter all values !", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        private void Employee_Master_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'dataSet1.Employee_Master' table. You can move, or remove it, as needed.
            this.employee_MasterTableAdapter.Fill(this.dataSet1.Employee_Master);
            //clearfn();
        }

        private void toolStripButton1_Click(object sender, EventArgs e)
        {
            a1.delete_employee(emp_IdTextBox.Text);
            MessageBox.Show("Record Deleted Successfully !");
        }

        private void toolStripButton2_Click(object sender, EventArgs e)
        {
            a1.update_employee(emp_IdTextBox.Text, emp_NameTextBox.Text, salaryTextBox.Text, designationTextBox.Text);
            MessageBox.Show("Record Updated Successfully !");
        }

        private void button3_Click(object sender, EventArgs e)
        {
            ESearch E1 = new ESearch();
            E1.ShowDialog();
           
        }

        private void button1_Click(object sender, EventArgs e)
        {
            DataTable dt = new DataTable();
            dt = a1.search_employee(emp_IdTextBox.Text);
            if (dt.Rows.Count > 0)
            {
                MessageBox.Show("Already Exists !");
                emp_IdTextBox.Text = "";
                emp_IdTextBox.Focus();
            }
            else
            {
                MessageBox.Show("Emp_Id Available !");
            }
        }
    }
}