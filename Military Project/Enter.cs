using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Military_Project
{
    public partial class Enter : Form
    {
        public Enter()
        {
            InitializeComponent();

        }

        private void button1_Click(object sender, EventArgs e)
        {
            Sign_in si = new Sign_in();
            si.Show();
            this.Hide();
        }

        private void Enter_Load(object sender, EventArgs e)
        {
            this.progressBar1.Value = 0;
            this.timer1.Interval = 3;
            this.timer1.Enabled = true;
            timer1_Tick(sender, e);

        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            if (this.progressBar1.Value < 100)
            {
                this.progressBar1.Value++;
                if (this.progressBar1.Value == 100)
                {
                   // this.Hide();
                    Sign_in sp = new Sign_in();
                    sp.ShowDialog();
                    this.Hide();
                }
            }

        }
    }
}