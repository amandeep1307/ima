namespace Military_Project
{
    partial class Navigation_Master
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Navigation_Master));
            this.panel1 = new System.Windows.Forms.Panel();
            this.button2 = new System.Windows.Forms.Button();
            this.panel11 = new System.Windows.Forms.Panel();
            this.panel10 = new System.Windows.Forms.Panel();
            this.panel16 = new System.Windows.Forms.Panel();
            this.panel9 = new System.Windows.Forms.Panel();
            this.panel15 = new System.Windows.Forms.Panel();
            this.menuStrip8 = new System.Windows.Forms.MenuStrip();
            this.toolStripMenuItem16 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem17 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem18 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem19 = new System.Windows.Forms.ToolStripMenuItem();
            this.panel2 = new System.Windows.Forms.Panel();
            this.menuStrip2 = new System.Windows.Forms.MenuStrip();
            this.generatePdfsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.retirementCertificateToolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.uESApplicationToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.paymentReceiptToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.panel14 = new System.Windows.Forms.Panel();
            this.panel8 = new System.Windows.Forms.Panel();
            this.panel7 = new System.Windows.Forms.Panel();
            this.panel6 = new System.Windows.Forms.Panel();
            this.menuStrip6 = new System.Windows.Forms.MenuStrip();
            this.createNewUserToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.panel13 = new System.Windows.Forms.Panel();
            this.menuStrip7 = new System.Windows.Forms.MenuStrip();
            this.toolStripMenuItem5 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem6 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem7 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem8 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem9 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem10 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem11 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem12 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem13 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem14 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem15 = new System.Windows.Forms.ToolStripMenuItem();
            this.panel5 = new System.Windows.Forms.Panel();
            this.menuStrip5 = new System.Windows.Forms.MenuStrip();
            this.employeesDatabaseToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.battalionMasterToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.companyMasterToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.platoonMasterToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.sectionMasterToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.serviceRecordToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.soldierMasterToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.employeeMasterToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.sportsMasterToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.punishmentMasterToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.accountMasterToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.panel12 = new System.Windows.Forms.Panel();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.panel4 = new System.Windows.Forms.Panel();
            this.menuStrip4 = new System.Windows.Forms.MenuStrip();
            this.paymentsDatabaseToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.panel3 = new System.Windows.Forms.Panel();
            this.menuStrip3 = new System.Windows.Forms.MenuStrip();
            this.uESApplicantsDatabaseToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem3 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem4 = new System.Windows.Forms.ToolStripMenuItem();
            this.panel17 = new System.Windows.Forms.Panel();
            this.panel1.SuspendLayout();
            this.panel15.SuspendLayout();
            this.menuStrip8.SuspendLayout();
            this.panel2.SuspendLayout();
            this.menuStrip2.SuspendLayout();
            this.panel6.SuspendLayout();
            this.menuStrip6.SuspendLayout();
            this.panel13.SuspendLayout();
            this.menuStrip7.SuspendLayout();
            this.panel5.SuspendLayout();
            this.menuStrip5.SuspendLayout();
            this.panel12.SuspendLayout();
            this.menuStrip1.SuspendLayout();
            this.panel4.SuspendLayout();
            this.menuStrip4.SuspendLayout();
            this.panel3.SuspendLayout();
            this.menuStrip3.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.panel1.Controls.Add(this.button2);
            this.panel1.Controls.Add(this.panel11);
            this.panel1.Controls.Add(this.panel10);
            this.panel1.Controls.Add(this.panel16);
            this.panel1.Controls.Add(this.panel9);
            this.panel1.Controls.Add(this.panel15);
            this.panel1.Controls.Add(this.panel2);
            this.panel1.Controls.Add(this.panel14);
            this.panel1.Controls.Add(this.panel8);
            this.panel1.Controls.Add(this.panel7);
            this.panel1.Controls.Add(this.panel6);
            this.panel1.Controls.Add(this.panel13);
            this.panel1.Controls.Add(this.panel5);
            this.panel1.Controls.Add(this.panel12);
            this.panel1.Controls.Add(this.panel4);
            this.panel1.Controls.Add(this.panel3);
            this.panel1.Location = new System.Drawing.Point(24, 112);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(637, 420);
            this.panel1.TabIndex = 8;
            this.panel1.Paint += new System.Windows.Forms.PaintEventHandler(this.panel1_Paint);
            // 
            // button2
            // 
            this.button2.BackColor = System.Drawing.Color.DodgerBlue;
            this.button2.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.button2.Location = new System.Drawing.Point(226, 397);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(179, 23);
            this.button2.TabIndex = 24;
            this.button2.Text = "Logout";
            this.button2.UseVisualStyleBackColor = false;
            this.button2.Click += new System.EventHandler(this.button2_Click_1);
            // 
            // panel11
            // 
            this.panel11.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("panel11.BackgroundImage")));
            this.panel11.Location = new System.Drawing.Point(250, 12);
            this.panel11.Name = "panel11";
            this.panel11.Size = new System.Drawing.Size(128, 129);
            this.panel11.TabIndex = 32;
            // 
            // panel10
            // 
            this.panel10.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("panel10.BackgroundImage")));
            this.panel10.Location = new System.Drawing.Point(490, 254);
            this.panel10.Name = "panel10";
            this.panel10.Size = new System.Drawing.Size(100, 100);
            this.panel10.TabIndex = 31;
            // 
            // panel16
            // 
            this.panel16.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("panel16.BackgroundImage")));
            this.panel16.Location = new System.Drawing.Point(490, 27);
            this.panel16.Name = "panel16";
            this.panel16.Size = new System.Drawing.Size(110, 128);
            this.panel16.TabIndex = 30;
            this.panel16.Paint += new System.Windows.Forms.PaintEventHandler(this.panel16_Paint);
            // 
            // panel9
            // 
            this.panel9.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("panel9.BackgroundImage")));
            this.panel9.Location = new System.Drawing.Point(490, 27);
            this.panel9.Name = "panel9";
            this.panel9.Size = new System.Drawing.Size(110, 128);
            this.panel9.TabIndex = 30;
            this.panel9.Paint += new System.Windows.Forms.PaintEventHandler(this.panel9_Paint);
            // 
            // panel15
            // 
            this.panel15.Controls.Add(this.menuStrip8);
            this.panel15.Location = new System.Drawing.Point(24, 161);
            this.panel15.Name = "panel15";
            this.panel15.Size = new System.Drawing.Size(115, 22);
            this.panel15.TabIndex = 23;
            this.panel15.Paint += new System.Windows.Forms.PaintEventHandler(this.panel15_Paint);
            // 
            // menuStrip8
            // 
            this.menuStrip8.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem16});
            this.menuStrip8.Location = new System.Drawing.Point(0, 0);
            this.menuStrip8.Name = "menuStrip8";
            this.menuStrip8.Size = new System.Drawing.Size(115, 24);
            this.menuStrip8.TabIndex = 0;
            this.menuStrip8.Text = "menuStrip2";
            // 
            // toolStripMenuItem16
            // 
            this.toolStripMenuItem16.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem17,
            this.toolStripMenuItem18,
            this.toolStripMenuItem19});
            this.toolStripMenuItem16.Name = "toolStripMenuItem16";
            this.toolStripMenuItem16.Size = new System.Drawing.Size(95, 20);
            this.toolStripMenuItem16.Text = "Generate Pdf\'s";
            // 
            // toolStripMenuItem17
            // 
            this.toolStripMenuItem17.Name = "toolStripMenuItem17";
            this.toolStripMenuItem17.Size = new System.Drawing.Size(189, 22);
            this.toolStripMenuItem17.Text = "Retirement Certificate";
            this.toolStripMenuItem17.Click += new System.EventHandler(this.retirementCertificateToolStripMenuItem2_Click);
            // 
            // toolStripMenuItem18
            // 
            this.toolStripMenuItem18.Name = "toolStripMenuItem18";
            this.toolStripMenuItem18.Size = new System.Drawing.Size(189, 22);
            this.toolStripMenuItem18.Text = "UES Application";
            this.toolStripMenuItem18.Click += new System.EventHandler(this.uESApplicationToolStripMenuItem1_Click);
            // 
            // toolStripMenuItem19
            // 
            this.toolStripMenuItem19.Name = "toolStripMenuItem19";
            this.toolStripMenuItem19.Size = new System.Drawing.Size(189, 22);
            this.toolStripMenuItem19.Text = "Payment Receipt";
            this.toolStripMenuItem19.Click += new System.EventHandler(this.paymentReceiptToolStripMenuItem1_Click);
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.menuStrip2);
            this.panel2.Location = new System.Drawing.Point(24, 161);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(115, 22);
            this.panel2.TabIndex = 23;
            this.panel2.Paint += new System.Windows.Forms.PaintEventHandler(this.panel2_Paint);
            // 
            // menuStrip2
            // 
            this.menuStrip2.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.generatePdfsToolStripMenuItem});
            this.menuStrip2.Location = new System.Drawing.Point(0, 0);
            this.menuStrip2.Name = "menuStrip2";
            this.menuStrip2.Size = new System.Drawing.Size(115, 24);
            this.menuStrip2.TabIndex = 0;
            this.menuStrip2.Text = "menuStrip2";
            // 
            // generatePdfsToolStripMenuItem
            // 
            this.generatePdfsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.retirementCertificateToolStripMenuItem2,
            this.uESApplicationToolStripMenuItem1,
            this.paymentReceiptToolStripMenuItem1});
            this.generatePdfsToolStripMenuItem.Name = "generatePdfsToolStripMenuItem";
            this.generatePdfsToolStripMenuItem.Size = new System.Drawing.Size(95, 20);
            this.generatePdfsToolStripMenuItem.Text = "Generate Pdf\'s";
            // 
            // retirementCertificateToolStripMenuItem2
            // 
            this.retirementCertificateToolStripMenuItem2.Name = "retirementCertificateToolStripMenuItem2";
            this.retirementCertificateToolStripMenuItem2.Size = new System.Drawing.Size(189, 22);
            this.retirementCertificateToolStripMenuItem2.Text = "Retirement Certificate";
            this.retirementCertificateToolStripMenuItem2.Click += new System.EventHandler(this.retirementCertificateToolStripMenuItem2_Click);
            // 
            // uESApplicationToolStripMenuItem1
            // 
            this.uESApplicationToolStripMenuItem1.Name = "uESApplicationToolStripMenuItem1";
            this.uESApplicationToolStripMenuItem1.Size = new System.Drawing.Size(189, 22);
            this.uESApplicationToolStripMenuItem1.Text = "UES Application";
            this.uESApplicationToolStripMenuItem1.Click += new System.EventHandler(this.uESApplicationToolStripMenuItem1_Click);
            // 
            // paymentReceiptToolStripMenuItem1
            // 
            this.paymentReceiptToolStripMenuItem1.Name = "paymentReceiptToolStripMenuItem1";
            this.paymentReceiptToolStripMenuItem1.Size = new System.Drawing.Size(189, 22);
            this.paymentReceiptToolStripMenuItem1.Text = "Payment Receipt";
            this.paymentReceiptToolStripMenuItem1.Click += new System.EventHandler(this.paymentReceiptToolStripMenuItem1_Click);
            // 
            // panel14
            // 
            this.panel14.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("panel14.BackgroundImage")));
            this.panel14.Location = new System.Drawing.Point(24, 27);
            this.panel14.Name = "panel14";
            this.panel14.Size = new System.Drawing.Size(128, 128);
            this.panel14.TabIndex = 29;
            this.panel14.Paint += new System.Windows.Forms.PaintEventHandler(this.panel14_Paint);
            // 
            // panel8
            // 
            this.panel8.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("panel8.BackgroundImage")));
            this.panel8.Location = new System.Drawing.Point(24, 27);
            this.panel8.Name = "panel8";
            this.panel8.Size = new System.Drawing.Size(128, 128);
            this.panel8.TabIndex = 29;
            this.panel8.Paint += new System.Windows.Forms.PaintEventHandler(this.panel8_Paint);
            // 
            // panel7
            // 
            this.panel7.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("panel7.BackgroundImage")));
            this.panel7.Location = new System.Drawing.Point(24, 254);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(128, 100);
            this.panel7.TabIndex = 28;
            // 
            // panel6
            // 
            this.panel6.Controls.Add(this.menuStrip6);
            this.panel6.Location = new System.Drawing.Point(24, 359);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(115, 24);
            this.panel6.TabIndex = 27;
            // 
            // menuStrip6
            // 
            this.menuStrip6.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.createNewUserToolStripMenuItem1});
            this.menuStrip6.Location = new System.Drawing.Point(0, 0);
            this.menuStrip6.Name = "menuStrip6";
            this.menuStrip6.Size = new System.Drawing.Size(115, 24);
            this.menuStrip6.TabIndex = 0;
            this.menuStrip6.Text = "menuStrip6";
            // 
            // createNewUserToolStripMenuItem1
            // 
            this.createNewUserToolStripMenuItem1.Name = "createNewUserToolStripMenuItem1";
            this.createNewUserToolStripMenuItem1.Size = new System.Drawing.Size(106, 20);
            this.createNewUserToolStripMenuItem1.Text = "Create New User";
            this.createNewUserToolStripMenuItem1.Click += new System.EventHandler(this.createNewUserToolStripMenuItem1_Click);
            // 
            // panel13
            // 
            this.panel13.Controls.Add(this.menuStrip7);
            this.panel13.Location = new System.Drawing.Point(250, 146);
            this.panel13.Name = "panel13";
            this.panel13.Size = new System.Drawing.Size(137, 24);
            this.panel13.TabIndex = 26;
            // 
            // menuStrip7
            // 
            this.menuStrip7.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem5});
            this.menuStrip7.Location = new System.Drawing.Point(0, 0);
            this.menuStrip7.Name = "menuStrip7";
            this.menuStrip7.Size = new System.Drawing.Size(137, 24);
            this.menuStrip7.TabIndex = 0;
            this.menuStrip7.Text = "menuStrip5";
            // 
            // toolStripMenuItem5
            // 
            this.toolStripMenuItem5.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem6,
            this.toolStripMenuItem7,
            this.toolStripMenuItem8,
            this.toolStripMenuItem9,
            this.toolStripMenuItem10,
            this.toolStripMenuItem11,
            this.toolStripMenuItem12,
            this.toolStripMenuItem13,
            this.toolStripMenuItem14,
            this.toolStripMenuItem15});
            this.toolStripMenuItem5.Name = "toolStripMenuItem5";
            this.toolStripMenuItem5.Size = new System.Drawing.Size(130, 20);
            this.toolStripMenuItem5.Text = "Employee\'s Database";
            // 
            // toolStripMenuItem6
            // 
            this.toolStripMenuItem6.Name = "toolStripMenuItem6";
            this.toolStripMenuItem6.Size = new System.Drawing.Size(177, 22);
            this.toolStripMenuItem6.Text = "Battalion Master";
            this.toolStripMenuItem6.Click += new System.EventHandler(this.battalionMasterToolStripMenuItem1_Click);
            // 
            // toolStripMenuItem7
            // 
            this.toolStripMenuItem7.Name = "toolStripMenuItem7";
            this.toolStripMenuItem7.Size = new System.Drawing.Size(177, 22);
            this.toolStripMenuItem7.Text = "Company Master";
            this.toolStripMenuItem7.Click += new System.EventHandler(this.companyMasterToolStripMenuItem1_Click);
            // 
            // toolStripMenuItem8
            // 
            this.toolStripMenuItem8.Name = "toolStripMenuItem8";
            this.toolStripMenuItem8.Size = new System.Drawing.Size(177, 22);
            this.toolStripMenuItem8.Text = "Platoon Master";
            this.toolStripMenuItem8.Click += new System.EventHandler(this.platoonMasterToolStripMenuItem1_Click);
            // 
            // toolStripMenuItem9
            // 
            this.toolStripMenuItem9.Name = "toolStripMenuItem9";
            this.toolStripMenuItem9.Size = new System.Drawing.Size(177, 22);
            this.toolStripMenuItem9.Text = "Section Master";
            this.toolStripMenuItem9.Click += new System.EventHandler(this.sectionMasterToolStripMenuItem1_Click);
            // 
            // toolStripMenuItem10
            // 
            this.toolStripMenuItem10.Name = "toolStripMenuItem10";
            this.toolStripMenuItem10.Size = new System.Drawing.Size(177, 22);
            this.toolStripMenuItem10.Text = "Service Record";
            this.toolStripMenuItem10.Click += new System.EventHandler(this.serviceRecordToolStripMenuItem1_Click);
            // 
            // toolStripMenuItem11
            // 
            this.toolStripMenuItem11.Name = "toolStripMenuItem11";
            this.toolStripMenuItem11.Size = new System.Drawing.Size(177, 22);
            this.toolStripMenuItem11.Text = "Soldier Master";
            this.toolStripMenuItem11.Click += new System.EventHandler(this.soldierMasterToolStripMenuItem1_Click);
            // 
            // toolStripMenuItem12
            // 
            this.toolStripMenuItem12.Name = "toolStripMenuItem12";
            this.toolStripMenuItem12.Size = new System.Drawing.Size(177, 22);
            this.toolStripMenuItem12.Text = "Employee Master";
            this.toolStripMenuItem12.Click += new System.EventHandler(this.employeeMasterToolStripMenuItem1_Click);
            // 
            // toolStripMenuItem13
            // 
            this.toolStripMenuItem13.Name = "toolStripMenuItem13";
            this.toolStripMenuItem13.Size = new System.Drawing.Size(177, 22);
            this.toolStripMenuItem13.Text = "Sports Master";
            this.toolStripMenuItem13.Click += new System.EventHandler(this.sportsMasterToolStripMenuItem1_Click);
            // 
            // toolStripMenuItem14
            // 
            this.toolStripMenuItem14.Name = "toolStripMenuItem14";
            this.toolStripMenuItem14.Size = new System.Drawing.Size(177, 22);
            this.toolStripMenuItem14.Text = "Punishment Master";
            this.toolStripMenuItem14.Click += new System.EventHandler(this.punishmentMasterToolStripMenuItem1_Click);
            // 
            // toolStripMenuItem15
            // 
            this.toolStripMenuItem15.Name = "toolStripMenuItem15";
            this.toolStripMenuItem15.Size = new System.Drawing.Size(177, 22);
            this.toolStripMenuItem15.Text = "Account Master";
            this.toolStripMenuItem15.Click += new System.EventHandler(this.accountMasterToolStripMenuItem1_Click);
            // 
            // panel5
            // 
            this.panel5.Controls.Add(this.menuStrip5);
            this.panel5.Location = new System.Drawing.Point(250, 146);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(137, 24);
            this.panel5.TabIndex = 26;
            // 
            // menuStrip5
            // 
            this.menuStrip5.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.employeesDatabaseToolStripMenuItem});
            this.menuStrip5.Location = new System.Drawing.Point(0, 0);
            this.menuStrip5.Name = "menuStrip5";
            this.menuStrip5.Size = new System.Drawing.Size(137, 24);
            this.menuStrip5.TabIndex = 0;
            this.menuStrip5.Text = "menuStrip5";
            // 
            // employeesDatabaseToolStripMenuItem
            // 
            this.employeesDatabaseToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.battalionMasterToolStripMenuItem1,
            this.companyMasterToolStripMenuItem1,
            this.platoonMasterToolStripMenuItem1,
            this.sectionMasterToolStripMenuItem1,
            this.serviceRecordToolStripMenuItem1,
            this.soldierMasterToolStripMenuItem1,
            this.employeeMasterToolStripMenuItem1,
            this.sportsMasterToolStripMenuItem1,
            this.punishmentMasterToolStripMenuItem1,
            this.accountMasterToolStripMenuItem1});
            this.employeesDatabaseToolStripMenuItem.Name = "employeesDatabaseToolStripMenuItem";
            this.employeesDatabaseToolStripMenuItem.Size = new System.Drawing.Size(130, 20);
            this.employeesDatabaseToolStripMenuItem.Text = "Employee\'s Database";
            // 
            // battalionMasterToolStripMenuItem1
            // 
            this.battalionMasterToolStripMenuItem1.Name = "battalionMasterToolStripMenuItem1";
            this.battalionMasterToolStripMenuItem1.Size = new System.Drawing.Size(177, 22);
            this.battalionMasterToolStripMenuItem1.Text = "Battalion Master";
            this.battalionMasterToolStripMenuItem1.Click += new System.EventHandler(this.battalionMasterToolStripMenuItem1_Click);
            // 
            // companyMasterToolStripMenuItem1
            // 
            this.companyMasterToolStripMenuItem1.Name = "companyMasterToolStripMenuItem1";
            this.companyMasterToolStripMenuItem1.Size = new System.Drawing.Size(177, 22);
            this.companyMasterToolStripMenuItem1.Text = "Company Master";
            this.companyMasterToolStripMenuItem1.Click += new System.EventHandler(this.companyMasterToolStripMenuItem1_Click);
            // 
            // platoonMasterToolStripMenuItem1
            // 
            this.platoonMasterToolStripMenuItem1.Name = "platoonMasterToolStripMenuItem1";
            this.platoonMasterToolStripMenuItem1.Size = new System.Drawing.Size(177, 22);
            this.platoonMasterToolStripMenuItem1.Text = "Platoon Master";
            this.platoonMasterToolStripMenuItem1.Click += new System.EventHandler(this.platoonMasterToolStripMenuItem1_Click);
            // 
            // sectionMasterToolStripMenuItem1
            // 
            this.sectionMasterToolStripMenuItem1.Name = "sectionMasterToolStripMenuItem1";
            this.sectionMasterToolStripMenuItem1.Size = new System.Drawing.Size(177, 22);
            this.sectionMasterToolStripMenuItem1.Text = "Section Master";
            this.sectionMasterToolStripMenuItem1.Click += new System.EventHandler(this.sectionMasterToolStripMenuItem1_Click);
            // 
            // serviceRecordToolStripMenuItem1
            // 
            this.serviceRecordToolStripMenuItem1.Name = "serviceRecordToolStripMenuItem1";
            this.serviceRecordToolStripMenuItem1.Size = new System.Drawing.Size(177, 22);
            this.serviceRecordToolStripMenuItem1.Text = "Service Record";
            this.serviceRecordToolStripMenuItem1.Click += new System.EventHandler(this.serviceRecordToolStripMenuItem1_Click);
            // 
            // soldierMasterToolStripMenuItem1
            // 
            this.soldierMasterToolStripMenuItem1.Name = "soldierMasterToolStripMenuItem1";
            this.soldierMasterToolStripMenuItem1.Size = new System.Drawing.Size(177, 22);
            this.soldierMasterToolStripMenuItem1.Text = "Soldier Master";
            this.soldierMasterToolStripMenuItem1.Click += new System.EventHandler(this.soldierMasterToolStripMenuItem1_Click);
            // 
            // employeeMasterToolStripMenuItem1
            // 
            this.employeeMasterToolStripMenuItem1.Name = "employeeMasterToolStripMenuItem1";
            this.employeeMasterToolStripMenuItem1.Size = new System.Drawing.Size(177, 22);
            this.employeeMasterToolStripMenuItem1.Text = "Employee Master";
            this.employeeMasterToolStripMenuItem1.Click += new System.EventHandler(this.employeeMasterToolStripMenuItem1_Click);
            // 
            // sportsMasterToolStripMenuItem1
            // 
            this.sportsMasterToolStripMenuItem1.Name = "sportsMasterToolStripMenuItem1";
            this.sportsMasterToolStripMenuItem1.Size = new System.Drawing.Size(177, 22);
            this.sportsMasterToolStripMenuItem1.Text = "Sports Master";
            this.sportsMasterToolStripMenuItem1.Click += new System.EventHandler(this.sportsMasterToolStripMenuItem1_Click);
            // 
            // punishmentMasterToolStripMenuItem1
            // 
            this.punishmentMasterToolStripMenuItem1.Name = "punishmentMasterToolStripMenuItem1";
            this.punishmentMasterToolStripMenuItem1.Size = new System.Drawing.Size(177, 22);
            this.punishmentMasterToolStripMenuItem1.Text = "Punishment Master";
            this.punishmentMasterToolStripMenuItem1.Click += new System.EventHandler(this.punishmentMasterToolStripMenuItem1_Click);
            // 
            // accountMasterToolStripMenuItem1
            // 
            this.accountMasterToolStripMenuItem1.Name = "accountMasterToolStripMenuItem1";
            this.accountMasterToolStripMenuItem1.Size = new System.Drawing.Size(177, 22);
            this.accountMasterToolStripMenuItem1.Text = "Account Master";
            this.accountMasterToolStripMenuItem1.Click += new System.EventHandler(this.accountMasterToolStripMenuItem1_Click);
            // 
            // panel12
            // 
            this.panel12.Controls.Add(this.menuStrip1);
            this.panel12.Location = new System.Drawing.Point(469, 161);
            this.panel12.Name = "panel12";
            this.panel12.Size = new System.Drawing.Size(158, 22);
            this.panel12.TabIndex = 24;
            this.panel12.Paint += new System.Windows.Forms.PaintEventHandler(this.panel3_Paint);
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem1});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(158, 24);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip3";
            this.menuStrip1.ItemClicked += new System.Windows.Forms.ToolStripItemClickedEventHandler(this.menuStrip3_ItemClicked);
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(149, 20);
            this.toolStripMenuItem1.Text = "UES Applicants Database";
            this.toolStripMenuItem1.Click += new System.EventHandler(this.uESApplicantsDatabaseToolStripMenuItem1_Click);
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.menuStrip4);
            this.panel4.Location = new System.Drawing.Point(476, 360);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(158, 23);
            this.panel4.TabIndex = 25;
            // 
            // menuStrip4
            // 
            this.menuStrip4.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.paymentsDatabaseToolStripMenuItem1});
            this.menuStrip4.Location = new System.Drawing.Point(0, 0);
            this.menuStrip4.Name = "menuStrip4";
            this.menuStrip4.Size = new System.Drawing.Size(158, 24);
            this.menuStrip4.TabIndex = 0;
            this.menuStrip4.Text = "menuStrip4";
            // 
            // paymentsDatabaseToolStripMenuItem1
            // 
            this.paymentsDatabaseToolStripMenuItem1.Name = "paymentsDatabaseToolStripMenuItem1";
            this.paymentsDatabaseToolStripMenuItem1.Size = new System.Drawing.Size(122, 20);
            this.paymentsDatabaseToolStripMenuItem1.Text = "Payments Database";
            this.paymentsDatabaseToolStripMenuItem1.Click += new System.EventHandler(this.paymentsDatabaseToolStripMenuItem1_Click);
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.menuStrip3);
            this.panel3.Location = new System.Drawing.Point(469, 161);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(158, 22);
            this.panel3.TabIndex = 24;
            this.panel3.Paint += new System.Windows.Forms.PaintEventHandler(this.panel3_Paint);
            // 
            // menuStrip3
            // 
            this.menuStrip3.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.uESApplicantsDatabaseToolStripMenuItem1});
            this.menuStrip3.Location = new System.Drawing.Point(0, 0);
            this.menuStrip3.Name = "menuStrip3";
            this.menuStrip3.Size = new System.Drawing.Size(158, 24);
            this.menuStrip3.TabIndex = 0;
            this.menuStrip3.Text = "menuStrip3";
            this.menuStrip3.ItemClicked += new System.Windows.Forms.ToolStripItemClickedEventHandler(this.menuStrip3_ItemClicked);
            // 
            // uESApplicantsDatabaseToolStripMenuItem1
            // 
            this.uESApplicantsDatabaseToolStripMenuItem1.Name = "uESApplicantsDatabaseToolStripMenuItem1";
            this.uESApplicantsDatabaseToolStripMenuItem1.Size = new System.Drawing.Size(149, 20);
            this.uESApplicantsDatabaseToolStripMenuItem1.Text = "UES Applicants Database";
            this.uESApplicantsDatabaseToolStripMenuItem1.Click += new System.EventHandler(this.uESApplicantsDatabaseToolStripMenuItem1_Click);
            // 
            // toolStripMenuItem2
            // 
            this.toolStripMenuItem2.Name = "toolStripMenuItem2";
            this.toolStripMenuItem2.Size = new System.Drawing.Size(189, 22);
            this.toolStripMenuItem2.Text = "Retirement Certificate";
            // 
            // toolStripMenuItem3
            // 
            this.toolStripMenuItem3.Name = "toolStripMenuItem3";
            this.toolStripMenuItem3.Size = new System.Drawing.Size(189, 22);
            this.toolStripMenuItem3.Text = "UES Application";
            // 
            // toolStripMenuItem4
            // 
            this.toolStripMenuItem4.Name = "toolStripMenuItem4";
            this.toolStripMenuItem4.Size = new System.Drawing.Size(189, 22);
            this.toolStripMenuItem4.Text = "Payment Receipt";
            // 
            // panel17
            // 
            this.panel17.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("panel17.BackgroundImage")));
            this.panel17.Location = new System.Drawing.Point(24, 0);
            this.panel17.Name = "panel17";
            this.panel17.Size = new System.Drawing.Size(637, 118);
            this.panel17.TabIndex = 9;
            // 
            // Navigation_Master
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(684, 555);
            this.Controls.Add(this.panel17);
            this.Controls.Add(this.panel1);
            this.MaximizeBox = false;
            this.Name = "Navigation_Master";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Navigation_Master";
            this.Load += new System.EventHandler(this.Navigation_Master_Load);
            this.panel1.ResumeLayout(false);
            this.panel15.ResumeLayout(false);
            this.panel15.PerformLayout();
            this.menuStrip8.ResumeLayout(false);
            this.menuStrip8.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.menuStrip2.ResumeLayout(false);
            this.menuStrip2.PerformLayout();
            this.panel6.ResumeLayout(false);
            this.panel6.PerformLayout();
            this.menuStrip6.ResumeLayout(false);
            this.menuStrip6.PerformLayout();
            this.panel13.ResumeLayout(false);
            this.panel13.PerformLayout();
            this.menuStrip7.ResumeLayout(false);
            this.menuStrip7.PerformLayout();
            this.panel5.ResumeLayout(false);
            this.panel5.PerformLayout();
            this.menuStrip5.ResumeLayout(false);
            this.menuStrip5.PerformLayout();
            this.panel12.ResumeLayout(false);
            this.panel12.PerformLayout();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            this.menuStrip4.ResumeLayout(false);
            this.menuStrip4.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.menuStrip3.ResumeLayout(false);
            this.menuStrip3.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem3;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem4;
        private System.Windows.Forms.MenuStrip menuStrip2;
        private System.Windows.Forms.ToolStripMenuItem generatePdfsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem retirementCertificateToolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem uESApplicationToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem paymentReceiptToolStripMenuItem1;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.MenuStrip menuStrip3;
        private System.Windows.Forms.ToolStripMenuItem uESApplicantsDatabaseToolStripMenuItem1;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.MenuStrip menuStrip4;
        private System.Windows.Forms.ToolStripMenuItem paymentsDatabaseToolStripMenuItem1;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.MenuStrip menuStrip6;
        private System.Windows.Forms.ToolStripMenuItem createNewUserToolStripMenuItem1;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.MenuStrip menuStrip5;
        private System.Windows.Forms.ToolStripMenuItem employeesDatabaseToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem battalionMasterToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem companyMasterToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem platoonMasterToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem sectionMasterToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem serviceRecordToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem soldierMasterToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem employeeMasterToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem sportsMasterToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem punishmentMasterToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem accountMasterToolStripMenuItem1;
        private System.Windows.Forms.Panel panel7;
        private System.Windows.Forms.Panel panel8;
        private System.Windows.Forms.Panel panel9;
        private System.Windows.Forms.Panel panel10;
        private System.Windows.Forms.Panel panel11;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Panel panel16;
        private System.Windows.Forms.Panel panel15;
        private System.Windows.Forms.MenuStrip menuStrip8;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem16;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem17;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem18;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem19;
        private System.Windows.Forms.Panel panel14;
        private System.Windows.Forms.Panel panel13;
        private System.Windows.Forms.MenuStrip menuStrip7;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem5;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem6;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem7;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem8;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem9;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem10;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem11;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem12;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem13;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem14;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem15;
        private System.Windows.Forms.Panel panel12;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem1;
        private System.Windows.Forms.Panel panel17;
    }
}