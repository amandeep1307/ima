using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Military_Project
{
    public partial class Navigation_Master : Form
    {
        public Navigation_Master()
        {
            InitializeComponent();
        }

        private void Navigation_Master_Load(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            Form1 BN = new Form1();
            BN.ShowDialog();

        }

        private void button2_Click(object sender, EventArgs e)
        {
            Form2 CN = new Form2();
            CN.ShowDialog();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            Form3 PN = new Form3();
            PN.ShowDialog();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            Section_Master SN = new Section_Master();
            SN.ShowDialog();
        }

        private void button5_Click(object sender, EventArgs e)
        {
            Soldier_Master soldier1 = new Soldier_Master();
            soldier1.ShowDialog();
        }

        private void button6_Click(object sender, EventArgs e)
        {
            Sports_Master sp1 = new Sports_Master();
            sp1.ShowDialog();
        }

        private void button7_Click(object sender, EventArgs e)
        {
            Punishment_Master PN1 = new Punishment_Master();
            PN1.ShowDialog();
        }

        private void button8_Click(object sender, EventArgs e)
        {
            Service_Record SR1 = new Service_Record();
            SR1.ShowDialog();
        }

        private void button9_Click(object sender, EventArgs e)
        {
            Sign_up sp1=new Sign_up();
            sp1.ShowDialog();

        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void retirementCertificateToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            
        }

        private void uESApplicationToolStripMenuItem_Click(object sender, EventArgs e)
        {
            
        }

        private void paymentReceiptToolStripMenuItem_Click(object sender, EventArgs e)
        {
            
        }

        private void retirementCertificateToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void trainingMasterToolStripMenuItem_Click(object sender, EventArgs e)
        {
           
        }

        private void voucherMasterToolStripMenuItem_Click(object sender, EventArgs e)
        {
            

        }

        private void battalionMasterToolStripMenuItem_Click(object sender, EventArgs e)
        {
           
        }

        private void companyMasterToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form2 fm2 = new Form2();
            fm2.ShowDialog();
        }

        private void platoonMasterToolStripMenuItem_Click(object sender, EventArgs e)
        {
            
        }

        private void sectionMasterToolStripMenuItem_Click(object sender, EventArgs e)
        {
            
        }

        private void serviceRecordToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Service_Record sr = new Service_Record();
            sr.ShowDialog();
        }

        private void soldierMasterToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Soldier_Master som = new Soldier_Master();
            som.ShowDialog();
        }

        private void employeeMasterToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Employee_Master em = new Employee_Master();
            em.ShowDialog();

        }

        private void sportsMasterToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Sports_Master spm = new Sports_Master();
            spm.ShowDialog();
        }

        private void punishmentMasterToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Punishment_Master pm = new Punishment_Master();
            pm.ShowDialog();
        }

        private void accountMasterToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Account_Master am = new Account_Master();
            am.ShowDialog();
        }

        private void menuStrip1_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {

        }

        private void signUpToolStripMenuItem_Click(object sender, EventArgs e)
        {
            
        }

        private void toolStripMenuItem1_Click(object sender, EventArgs e)
        {

        }

        private void toolStripMenuItem5_Click(object sender, EventArgs e)
        {

        }

        private void retirementCertificateToolStripMenuItem2_Click(object sender, EventArgs e)
        {
            Retirement_Certificate rc = new Retirement_Certificate();
            rc.ShowDialog();
        }

        private void uESApplicationToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            UES_Application ues = new UES_Application();
            ues.ShowDialog();
        }

        private void paymentReceiptToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            Payment_Receipt pr = new Payment_Receipt();
            pr.ShowDialog();
        }

        private void trainingMasterToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            
        }

        private void panel3_Paint(object sender, PaintEventArgs e)
        {

        }

        private void menuStrip3_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {

        }

        private void voucherMasterToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            
        }

        private void createNewUserToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            Sign_up sp1 = new Sign_up();
            sp1.ShowDialog();
        }

        private void createNewUserToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void uESApplicantsDatabaseToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            Training_Master tm = new Training_Master();
            tm.ShowDialog();
        }

        private void paymentsDatabaseToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            Voucher_Master vm = new Voucher_Master();
            vm.ShowDialog();
        }

        private void battalionMasterToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            Form1 bm = new Form1();
            bm.ShowDialog();
        }

        private void companyMasterToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            Form2 fm2 = new Form2();
            fm2.ShowDialog();
        }

        private void platoonMasterToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            Form3 fm3 = new Form3();
            fm3.ShowDialog();
        }

        private void sectionMasterToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            Section_Master sm = new Section_Master();
            sm.ShowDialog();
        }

        private void serviceRecordToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            Service_Record sr = new Service_Record();
            sr.ShowDialog();
        }

        private void soldierMasterToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            Soldier_Master som = new Soldier_Master();
            som.ShowDialog();
        }

        private void employeeMasterToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            Employee_Master em = new Employee_Master();
            em.ShowDialog();
        }

        private void sportsMasterToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            Sports_Master spm = new Sports_Master();
            spm.ShowDialog();
        }

        private void punishmentMasterToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            Punishment_Master pm = new Punishment_Master();
            pm.ShowDialog();
        }

        private void accountMasterToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            Account_Master am = new Account_Master();
            am.ShowDialog();
        }

        private void panel12_Paint(object sender, PaintEventArgs e)
        {
           

        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            this.Dispose();
        }

        private void panel14_Paint(object sender, PaintEventArgs e)
        {

        }

        private void panel2_Paint(object sender, PaintEventArgs e)
        {

        }

        private void panel8_Paint(object sender, PaintEventArgs e)
        {

        }

        private void panel16_Paint(object sender, PaintEventArgs e)
        {

        }

        private void panel15_Paint(object sender, PaintEventArgs e)
        {

        }

        private void panel9_Paint(object sender, PaintEventArgs e)
        {

        }

        private void button2_Click_1(object sender, EventArgs e)
        {
            this.Dispose();
            
          
        }
    }
}