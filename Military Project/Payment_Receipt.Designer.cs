namespace Military_Project
{
    partial class Payment_Receipt
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.Label voucher_NoLabel;
            this.dataSet1 = new Military_Project.DataSet1();
            this.voucher_MasterBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.voucher_MasterTableAdapter = new Military_Project.DataSet1TableAdapters.Voucher_MasterTableAdapter();
            this.voucher_NoComboBox = new System.Windows.Forms.ComboBox();
            this.voucher_MasterDataGridView = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn8 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn9 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.button1 = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            voucher_NoLabel = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.voucher_MasterBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.voucher_MasterDataGridView)).BeginInit();
            this.SuspendLayout();
            // 
            // voucher_NoLabel
            // 
            voucher_NoLabel.AutoSize = true;
            voucher_NoLabel.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            voucher_NoLabel.Location = new System.Drawing.Point(178, 148);
            voucher_NoLabel.Name = "voucher_NoLabel";
            voucher_NoLabel.Size = new System.Drawing.Size(82, 17);
            voucher_NoLabel.TabIndex = 1;
            voucher_NoLabel.Text = "Voucher No:";
            // 
            // dataSet1
            // 
            this.dataSet1.DataSetName = "DataSet1";
            this.dataSet1.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // voucher_MasterBindingSource
            // 
            this.voucher_MasterBindingSource.DataMember = "Voucher_Master";
            this.voucher_MasterBindingSource.DataSource = this.dataSet1;
            // 
            // voucher_MasterTableAdapter
            // 
            this.voucher_MasterTableAdapter.ClearBeforeFill = true;
            // 
            // voucher_NoComboBox
            // 
            this.voucher_NoComboBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.voucher_MasterBindingSource, "Voucher_No", true));
            this.voucher_NoComboBox.DataSource = this.voucher_MasterBindingSource;
            this.voucher_NoComboBox.DisplayMember = "Voucher_No";
            this.voucher_NoComboBox.FormattingEnabled = true;
            this.voucher_NoComboBox.Location = new System.Drawing.Point(267, 145);
            this.voucher_NoComboBox.Name = "voucher_NoComboBox";
            this.voucher_NoComboBox.Size = new System.Drawing.Size(157, 21);
            this.voucher_NoComboBox.TabIndex = 2;
            this.voucher_NoComboBox.SelectedIndexChanged += new System.EventHandler(this.voucher_NoComboBox_SelectedIndexChanged);
            // 
            // voucher_MasterDataGridView
            // 
            this.voucher_MasterDataGridView.AutoGenerateColumns = false;
            this.voucher_MasterDataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn1,
            this.dataGridViewTextBoxColumn2,
            this.dataGridViewTextBoxColumn3,
            this.dataGridViewTextBoxColumn4,
            this.dataGridViewTextBoxColumn5,
            this.dataGridViewTextBoxColumn6,
            this.dataGridViewTextBoxColumn7,
            this.dataGridViewTextBoxColumn8,
            this.dataGridViewTextBoxColumn9});
            this.voucher_MasterDataGridView.DataSource = this.voucher_MasterBindingSource;
            this.voucher_MasterDataGridView.Location = new System.Drawing.Point(65, 208);
            this.voucher_MasterDataGridView.Name = "voucher_MasterDataGridView";
            this.voucher_MasterDataGridView.Size = new System.Drawing.Size(542, 220);
            this.voucher_MasterDataGridView.TabIndex = 2;
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.DataPropertyName = "Voucher_No";
            this.dataGridViewTextBoxColumn1.HeaderText = "Voucher_No";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.DataPropertyName = "VDate";
            this.dataGridViewTextBoxColumn2.HeaderText = "VDate";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.DataPropertyName = "Account_Head";
            this.dataGridViewTextBoxColumn3.HeaderText = "Account_Head";
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            // 
            // dataGridViewTextBoxColumn4
            // 
            this.dataGridViewTextBoxColumn4.DataPropertyName = "Cash_Cheque_No";
            this.dataGridViewTextBoxColumn4.HeaderText = "Cash_Cheque_No";
            this.dataGridViewTextBoxColumn4.Name = "dataGridViewTextBoxColumn4";
            // 
            // dataGridViewTextBoxColumn5
            // 
            this.dataGridViewTextBoxColumn5.DataPropertyName = "Date2";
            this.dataGridViewTextBoxColumn5.HeaderText = "Date2";
            this.dataGridViewTextBoxColumn5.Name = "dataGridViewTextBoxColumn5";
            // 
            // dataGridViewTextBoxColumn6
            // 
            this.dataGridViewTextBoxColumn6.DataPropertyName = "Emp_Name";
            this.dataGridViewTextBoxColumn6.HeaderText = "Emp_Name";
            this.dataGridViewTextBoxColumn6.Name = "dataGridViewTextBoxColumn6";
            // 
            // dataGridViewTextBoxColumn7
            // 
            this.dataGridViewTextBoxColumn7.DataPropertyName = "Recieved_From";
            this.dataGridViewTextBoxColumn7.HeaderText = "Recieved_From";
            this.dataGridViewTextBoxColumn7.Name = "dataGridViewTextBoxColumn7";
            // 
            // dataGridViewTextBoxColumn8
            // 
            this.dataGridViewTextBoxColumn8.DataPropertyName = "Sum1";
            this.dataGridViewTextBoxColumn8.HeaderText = "Sum1";
            this.dataGridViewTextBoxColumn8.Name = "dataGridViewTextBoxColumn8";
            // 
            // dataGridViewTextBoxColumn9
            // 
            this.dataGridViewTextBoxColumn9.DataPropertyName = "Witness";
            this.dataGridViewTextBoxColumn9.HeaderText = "Witness";
            this.dataGridViewTextBoxColumn9.Name = "dataGridViewTextBoxColumn9";
            // 
            // button1
            // 
            this.button1.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.Location = new System.Drawing.Point(450, 145);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(97, 23);
            this.button1.TabIndex = 3;
            this.button1.Text = "Generate Pdf";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Times New Roman", 15.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(262, 46);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(163, 24);
            this.label1.TabIndex = 4;
            this.label1.Text = "Payment Receipt";
            // 
            // Payment_Receipt
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(684, 487);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.voucher_MasterDataGridView);
            this.Controls.Add(voucher_NoLabel);
            this.Controls.Add(this.voucher_NoComboBox);
            this.Name = "Payment_Receipt";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Payment_Receipt";
            this.Load += new System.EventHandler(this.Payment_Receipt_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataSet1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.voucher_MasterBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.voucher_MasterDataGridView)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DataSet1 dataSet1;
        private System.Windows.Forms.BindingSource voucher_MasterBindingSource;
        private Military_Project.DataSet1TableAdapters.Voucher_MasterTableAdapter voucher_MasterTableAdapter;
        private System.Windows.Forms.ComboBox voucher_NoComboBox;
        private System.Windows.Forms.DataGridView voucher_MasterDataGridView;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn5;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn6;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn7;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn8;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn9;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label label1;
    }
}