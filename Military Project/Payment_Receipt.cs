using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Military_Project
{
    public partial class Payment_Receipt : Form
    {
        public Payment_Receipt()
        {
            InitializeComponent();

        }
        DataSet1TableAdapters.Voucher_MasterTableAdapter AS = new Military_Project.DataSet1TableAdapters.Voucher_MasterTableAdapter();
        private void voucher_MasterBindingNavigatorSaveItem_Click(object sender, EventArgs e)
        {
            this.Validate();
            this.voucher_MasterBindingSource.EndEdit();
            this.voucher_MasterTableAdapter.Update(this.dataSet1.Voucher_Master);

        }

        private void Payment_Receipt_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'dataSet1.Voucher_Master' table. You can move, or remove it, as needed.
            this.voucher_MasterTableAdapter.Fill(this.dataSet1.Voucher_Master);
            voucher_NoComboBox.Text = "";
        }

        private void voucher_NoComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            DataTable dt = new DataTable();
            dt = AS.search_voucher(voucher_NoComboBox.Text);
            if (dt.Rows.Count > 0)
            {
                voucher_MasterDataGridView.DataSource = dt;
            }
        }
        public static string vn;
        private void button1_Click(object sender, EventArgs e)
        {
            vn = this.voucher_NoComboBox.Text;
            
            Voucher vo = new Voucher();
            vo.Show();
            
        }
    }
}