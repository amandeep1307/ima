namespace Military_Project
{
    partial class Form3
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.Label platoon_IdLabel;
            System.Windows.Forms.Label platoon_NameLabel;
            System.Windows.Forms.Label platoon_DescLabel;
            System.Windows.Forms.Label battalion_IdLabel;
            System.Windows.Forms.Label company_IdLabel;
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form3));
            this.label1 = new System.Windows.Forms.Label();
            this.platoon_MasterBindingNavigator = new System.Windows.Forms.BindingNavigator(this.components);
            this.bindingNavigatorAddNewItem = new System.Windows.Forms.ToolStripButton();
            this.platoon_MasterBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataSet1 = new Military_Project.DataSet1();
            this.bindingNavigatorCountItem = new System.Windows.Forms.ToolStripLabel();
            this.bindingNavigatorMoveFirstItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorMovePreviousItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorSeparator = new System.Windows.Forms.ToolStripSeparator();
            this.bindingNavigatorPositionItem = new System.Windows.Forms.ToolStripTextBox();
            this.bindingNavigatorSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.bindingNavigatorMoveNextItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorMoveLastItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripButton1 = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton2 = new System.Windows.Forms.ToolStripButton();
            this.platoon_MasterBindingNavigatorSaveItem = new System.Windows.Forms.ToolStripButton();
            this.platoon_IdTextBox = new System.Windows.Forms.TextBox();
            this.platoon_NameTextBox = new System.Windows.Forms.TextBox();
            this.platoon_DescTextBox = new System.Windows.Forms.TextBox();
            this.button3 = new System.Windows.Forms.Button();
            this.platoon_MasterTableAdapter = new Military_Project.DataSet1TableAdapters.Platoon_MasterTableAdapter();
            this.button1 = new System.Windows.Forms.Button();
            this.battalion_IdComboBox = new System.Windows.Forms.ComboBox();
            this.battalionMasterBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.companyMasterBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.company_IdComboBox = new System.Windows.Forms.ComboBox();
            this.battalion_MasterTableAdapter = new Military_Project.DataSet1TableAdapters.Battalion_MasterTableAdapter();
            this.company_MasterTableAdapter = new Military_Project.DataSet1TableAdapters.Company_MasterTableAdapter();
            platoon_IdLabel = new System.Windows.Forms.Label();
            platoon_NameLabel = new System.Windows.Forms.Label();
            platoon_DescLabel = new System.Windows.Forms.Label();
            battalion_IdLabel = new System.Windows.Forms.Label();
            company_IdLabel = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.platoon_MasterBindingNavigator)).BeginInit();
            this.platoon_MasterBindingNavigator.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.platoon_MasterBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.battalionMasterBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.companyMasterBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // platoon_IdLabel
            // 
            platoon_IdLabel.AutoSize = true;
            platoon_IdLabel.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            platoon_IdLabel.Location = new System.Drawing.Point(158, 243);
            platoon_IdLabel.Name = "platoon_IdLabel";
            platoon_IdLabel.Size = new System.Drawing.Size(71, 17);
            platoon_IdLabel.TabIndex = 6;
            platoon_IdLabel.Text = "Platoon Id:";
            // 
            // platoon_NameLabel
            // 
            platoon_NameLabel.AutoSize = true;
            platoon_NameLabel.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            platoon_NameLabel.Location = new System.Drawing.Point(158, 286);
            platoon_NameLabel.Name = "platoon_NameLabel";
            platoon_NameLabel.Size = new System.Drawing.Size(95, 17);
            platoon_NameLabel.TabIndex = 8;
            platoon_NameLabel.Text = "Platoon Name:";
            // 
            // platoon_DescLabel
            // 
            platoon_DescLabel.AutoSize = true;
            platoon_DescLabel.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            platoon_DescLabel.Location = new System.Drawing.Point(158, 329);
            platoon_DescLabel.Name = "platoon_DescLabel";
            platoon_DescLabel.Size = new System.Drawing.Size(90, 17);
            platoon_DescLabel.TabIndex = 10;
            platoon_DescLabel.Text = "Platoon Desc:";
            // 
            // battalion_IdLabel
            // 
            battalion_IdLabel.AutoSize = true;
            battalion_IdLabel.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            battalion_IdLabel.Location = new System.Drawing.Point(158, 153);
            battalion_IdLabel.Name = "battalion_IdLabel";
            battalion_IdLabel.Size = new System.Drawing.Size(79, 17);
            battalion_IdLabel.TabIndex = 16;
            battalion_IdLabel.Text = "Battalion Id:";
            battalion_IdLabel.Click += new System.EventHandler(this.battalion_IdLabel_Click);
            // 
            // company_IdLabel
            // 
            company_IdLabel.AutoSize = true;
            company_IdLabel.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            company_IdLabel.Location = new System.Drawing.Point(158, 198);
            company_IdLabel.Name = "company_IdLabel";
            company_IdLabel.Size = new System.Drawing.Size(83, 17);
            company_IdLabel.TabIndex = 17;
            company_IdLabel.Text = "Company Id:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Times New Roman", 15.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(270, 85);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(148, 24);
            this.label1.TabIndex = 0;
            this.label1.Text = "Platoon Master";
            // 
            // platoon_MasterBindingNavigator
            // 
            this.platoon_MasterBindingNavigator.AddNewItem = this.bindingNavigatorAddNewItem;
            this.platoon_MasterBindingNavigator.BindingSource = this.platoon_MasterBindingSource;
            this.platoon_MasterBindingNavigator.CountItem = this.bindingNavigatorCountItem;
            this.platoon_MasterBindingNavigator.DeleteItem = null;
            this.platoon_MasterBindingNavigator.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.bindingNavigatorMoveFirstItem,
            this.bindingNavigatorMovePreviousItem,
            this.bindingNavigatorSeparator,
            this.bindingNavigatorPositionItem,
            this.bindingNavigatorCountItem,
            this.bindingNavigatorSeparator1,
            this.bindingNavigatorMoveNextItem,
            this.bindingNavigatorMoveLastItem,
            this.bindingNavigatorSeparator2,
            this.bindingNavigatorAddNewItem,
            this.toolStripButton1,
            this.toolStripButton2,
            this.platoon_MasterBindingNavigatorSaveItem});
            this.platoon_MasterBindingNavigator.Location = new System.Drawing.Point(0, 0);
            this.platoon_MasterBindingNavigator.MoveFirstItem = this.bindingNavigatorMoveFirstItem;
            this.platoon_MasterBindingNavigator.MoveLastItem = this.bindingNavigatorMoveLastItem;
            this.platoon_MasterBindingNavigator.MoveNextItem = this.bindingNavigatorMoveNextItem;
            this.platoon_MasterBindingNavigator.MovePreviousItem = this.bindingNavigatorMovePreviousItem;
            this.platoon_MasterBindingNavigator.Name = "platoon_MasterBindingNavigator";
            this.platoon_MasterBindingNavigator.PositionItem = this.bindingNavigatorPositionItem;
            this.platoon_MasterBindingNavigator.Size = new System.Drawing.Size(684, 25);
            this.platoon_MasterBindingNavigator.TabIndex = 1;
            this.platoon_MasterBindingNavigator.Text = "bindingNavigator1";
            // 
            // bindingNavigatorAddNewItem
            // 
            this.bindingNavigatorAddNewItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorAddNewItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorAddNewItem.Image")));
            this.bindingNavigatorAddNewItem.Name = "bindingNavigatorAddNewItem";
            this.bindingNavigatorAddNewItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorAddNewItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorAddNewItem.Text = "Add new data";
            this.bindingNavigatorAddNewItem.Click += new System.EventHandler(this.bindingNavigatorAddNewItem_Click);
            // 
            // platoon_MasterBindingSource
            // 
            this.platoon_MasterBindingSource.DataMember = "Platoon_Master";
            this.platoon_MasterBindingSource.DataSource = this.dataSet1;
            // 
            // dataSet1
            // 
            this.dataSet1.DataSetName = "DataSet1";
            this.dataSet1.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // bindingNavigatorCountItem
            // 
            this.bindingNavigatorCountItem.Name = "bindingNavigatorCountItem";
            this.bindingNavigatorCountItem.Size = new System.Drawing.Size(35, 22);
            this.bindingNavigatorCountItem.Text = "of {0}";
            this.bindingNavigatorCountItem.ToolTipText = "Total number of items";
            // 
            // bindingNavigatorMoveFirstItem
            // 
            this.bindingNavigatorMoveFirstItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMoveFirstItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveFirstItem.Image")));
            this.bindingNavigatorMoveFirstItem.Name = "bindingNavigatorMoveFirstItem";
            this.bindingNavigatorMoveFirstItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMoveFirstItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorMoveFirstItem.Text = "Move first";
            // 
            // bindingNavigatorMovePreviousItem
            // 
            this.bindingNavigatorMovePreviousItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMovePreviousItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMovePreviousItem.Image")));
            this.bindingNavigatorMovePreviousItem.Name = "bindingNavigatorMovePreviousItem";
            this.bindingNavigatorMovePreviousItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMovePreviousItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorMovePreviousItem.Text = "Move previous";
            // 
            // bindingNavigatorSeparator
            // 
            this.bindingNavigatorSeparator.Name = "bindingNavigatorSeparator";
            this.bindingNavigatorSeparator.Size = new System.Drawing.Size(6, 25);
            // 
            // bindingNavigatorPositionItem
            // 
            this.bindingNavigatorPositionItem.AccessibleName = "Position";
            this.bindingNavigatorPositionItem.AutoSize = false;
            this.bindingNavigatorPositionItem.Name = "bindingNavigatorPositionItem";
            this.bindingNavigatorPositionItem.Size = new System.Drawing.Size(50, 23);
            this.bindingNavigatorPositionItem.Text = "0";
            this.bindingNavigatorPositionItem.ToolTipText = "Current position";
            // 
            // bindingNavigatorSeparator1
            // 
            this.bindingNavigatorSeparator1.Name = "bindingNavigatorSeparator1";
            this.bindingNavigatorSeparator1.Size = new System.Drawing.Size(6, 25);
            // 
            // bindingNavigatorMoveNextItem
            // 
            this.bindingNavigatorMoveNextItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMoveNextItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveNextItem.Image")));
            this.bindingNavigatorMoveNextItem.Name = "bindingNavigatorMoveNextItem";
            this.bindingNavigatorMoveNextItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMoveNextItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorMoveNextItem.Text = "Move next";
            // 
            // bindingNavigatorMoveLastItem
            // 
            this.bindingNavigatorMoveLastItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMoveLastItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveLastItem.Image")));
            this.bindingNavigatorMoveLastItem.Name = "bindingNavigatorMoveLastItem";
            this.bindingNavigatorMoveLastItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMoveLastItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorMoveLastItem.Text = "Move last";
            // 
            // bindingNavigatorSeparator2
            // 
            this.bindingNavigatorSeparator2.Name = "bindingNavigatorSeparator2";
            this.bindingNavigatorSeparator2.Size = new System.Drawing.Size(6, 25);
            // 
            // toolStripButton1
            // 
            this.toolStripButton1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton1.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton1.Image")));
            this.toolStripButton1.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton1.Name = "toolStripButton1";
            this.toolStripButton1.Size = new System.Drawing.Size(23, 22);
            this.toolStripButton1.Text = "Delete";
            this.toolStripButton1.Click += new System.EventHandler(this.toolStripButton1_Click);
            // 
            // toolStripButton2
            // 
            this.toolStripButton2.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton2.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton2.Image")));
            this.toolStripButton2.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton2.Name = "toolStripButton2";
            this.toolStripButton2.Size = new System.Drawing.Size(23, 22);
            this.toolStripButton2.Text = "Update";
            this.toolStripButton2.Click += new System.EventHandler(this.toolStripButton2_Click);
            // 
            // platoon_MasterBindingNavigatorSaveItem
            // 
            this.platoon_MasterBindingNavigatorSaveItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.platoon_MasterBindingNavigatorSaveItem.Image = ((System.Drawing.Image)(resources.GetObject("platoon_MasterBindingNavigatorSaveItem.Image")));
            this.platoon_MasterBindingNavigatorSaveItem.Name = "platoon_MasterBindingNavigatorSaveItem";
            this.platoon_MasterBindingNavigatorSaveItem.Size = new System.Drawing.Size(23, 22);
            this.platoon_MasterBindingNavigatorSaveItem.Text = "Save";
            this.platoon_MasterBindingNavigatorSaveItem.Click += new System.EventHandler(this.platoon_MasterBindingNavigatorSaveItem_Click);
            // 
            // platoon_IdTextBox
            // 
            this.platoon_IdTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.platoon_MasterBindingSource, "Platoon_Id", true));
            this.platoon_IdTextBox.Location = new System.Drawing.Point(281, 242);
            this.platoon_IdTextBox.Name = "platoon_IdTextBox";
            this.platoon_IdTextBox.Size = new System.Drawing.Size(192, 20);
            this.platoon_IdTextBox.TabIndex = 7;
            this.platoon_IdTextBox.TextChanged += new System.EventHandler(this.platoon_IdTextBox_TextChanged);
            // 
            // platoon_NameTextBox
            // 
            this.platoon_NameTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.platoon_MasterBindingSource, "Platoon_Name", true));
            this.platoon_NameTextBox.Location = new System.Drawing.Point(281, 285);
            this.platoon_NameTextBox.Name = "platoon_NameTextBox";
            this.platoon_NameTextBox.Size = new System.Drawing.Size(192, 20);
            this.platoon_NameTextBox.TabIndex = 9;
            // 
            // platoon_DescTextBox
            // 
            this.platoon_DescTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.platoon_MasterBindingSource, "Platoon_Desc", true));
            this.platoon_DescTextBox.Location = new System.Drawing.Point(281, 328);
            this.platoon_DescTextBox.Name = "platoon_DescTextBox";
            this.platoon_DescTextBox.Size = new System.Drawing.Size(192, 20);
            this.platoon_DescTextBox.TabIndex = 11;
            // 
            // button3
            // 
            this.button3.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button3.Location = new System.Drawing.Point(515, 326);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(75, 23);
            this.button3.TabIndex = 14;
            this.button3.Text = "Search";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // platoon_MasterTableAdapter
            // 
            this.platoon_MasterTableAdapter.ClearBeforeFill = true;
            // 
            // button1
            // 
            this.button1.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.Location = new System.Drawing.Point(479, 240);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(131, 23);
            this.button1.TabIndex = 16;
            this.button1.Text = "Check Availability?";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // battalion_IdComboBox
            // 
            this.battalion_IdComboBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.platoon_MasterBindingSource, "Battalion_Id", true));
            this.battalion_IdComboBox.DataSource = this.battalionMasterBindingSource;
            this.battalion_IdComboBox.DisplayMember = "Battalion_Id";
            this.battalion_IdComboBox.FormattingEnabled = true;
            this.battalion_IdComboBox.Location = new System.Drawing.Point(281, 152);
            this.battalion_IdComboBox.Name = "battalion_IdComboBox";
            this.battalion_IdComboBox.Size = new System.Drawing.Size(192, 21);
            this.battalion_IdComboBox.TabIndex = 17;
            this.battalion_IdComboBox.SelectedIndexChanged += new System.EventHandler(this.battalion_IdComboBox_SelectedIndexChanged);
            // 
            // battalionMasterBindingSource
            // 
            this.battalionMasterBindingSource.DataMember = "Battalion_Master";
            this.battalionMasterBindingSource.DataSource = this.dataSet1;
            // 
            // companyMasterBindingSource
            // 
            this.companyMasterBindingSource.DataMember = "Company_Master";
            this.companyMasterBindingSource.DataSource = this.dataSet1;
            // 
            // company_IdComboBox
            // 
            this.company_IdComboBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.platoon_MasterBindingSource, "Company_Id", true));
            this.company_IdComboBox.DataSource = this.companyMasterBindingSource;
            this.company_IdComboBox.DisplayMember = "Company_Id";
            this.company_IdComboBox.FormattingEnabled = true;
            this.company_IdComboBox.Location = new System.Drawing.Point(281, 197);
            this.company_IdComboBox.Name = "company_IdComboBox";
            this.company_IdComboBox.Size = new System.Drawing.Size(192, 21);
            this.company_IdComboBox.TabIndex = 18;
            // 
            // battalion_MasterTableAdapter
            // 
            this.battalion_MasterTableAdapter.ClearBeforeFill = true;
            // 
            // company_MasterTableAdapter
            // 
            this.company_MasterTableAdapter.ClearBeforeFill = true;
            // 
            // Form3
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(684, 487);
            this.Controls.Add(company_IdLabel);
            this.Controls.Add(this.company_IdComboBox);
            this.Controls.Add(battalion_IdLabel);
            this.Controls.Add(this.battalion_IdComboBox);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.button3);
            this.Controls.Add(platoon_IdLabel);
            this.Controls.Add(this.platoon_IdTextBox);
            this.Controls.Add(platoon_NameLabel);
            this.Controls.Add(this.platoon_NameTextBox);
            this.Controls.Add(platoon_DescLabel);
            this.Controls.Add(this.platoon_DescTextBox);
            this.Controls.Add(this.platoon_MasterBindingNavigator);
            this.Controls.Add(this.label1);
            this.Name = "Form3";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Platoon_Master";
            this.Load += new System.EventHandler(this.Form3_Load);
            ((System.ComponentModel.ISupportInitialize)(this.platoon_MasterBindingNavigator)).EndInit();
            this.platoon_MasterBindingNavigator.ResumeLayout(false);
            this.platoon_MasterBindingNavigator.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.platoon_MasterBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.battalionMasterBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.companyMasterBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private DataSet1 dataSet1;
        private System.Windows.Forms.BindingSource platoon_MasterBindingSource;
        private Military_Project.DataSet1TableAdapters.Platoon_MasterTableAdapter platoon_MasterTableAdapter;
        private System.Windows.Forms.BindingNavigator platoon_MasterBindingNavigator;
        private System.Windows.Forms.ToolStripButton bindingNavigatorAddNewItem;
        private System.Windows.Forms.ToolStripLabel bindingNavigatorCountItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMoveFirstItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMovePreviousItem;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator;
        private System.Windows.Forms.ToolStripTextBox bindingNavigatorPositionItem;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator1;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMoveNextItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMoveLastItem;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator2;
        private System.Windows.Forms.ToolStripButton platoon_MasterBindingNavigatorSaveItem;
        private System.Windows.Forms.TextBox platoon_IdTextBox;
        private System.Windows.Forms.TextBox platoon_NameTextBox;
        private System.Windows.Forms.TextBox platoon_DescTextBox;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.ToolStripButton toolStripButton1;
        private System.Windows.Forms.ToolStripButton toolStripButton2;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.ComboBox battalion_IdComboBox;
        private System.Windows.Forms.ComboBox company_IdComboBox;
        private System.Windows.Forms.BindingSource battalionMasterBindingSource;
        private Military_Project.DataSet1TableAdapters.Battalion_MasterTableAdapter battalion_MasterTableAdapter;
        private System.Windows.Forms.BindingSource companyMasterBindingSource;
        private Military_Project.DataSet1TableAdapters.Company_MasterTableAdapter company_MasterTableAdapter;
    }
}