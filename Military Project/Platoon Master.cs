using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Military_Project
{
    public partial class Form3: Form
    {
        public Form3()
        {
            InitializeComponent();

        }
        DataSet1TableAdapters.Platoon_MasterTableAdapter a1 = new Military_Project.DataSet1TableAdapters.Platoon_MasterTableAdapter();
        private void clearfn()
        {
            platoon_DescTextBox.Clear();
            platoon_IdTextBox.Clear();
            battalion_IdComboBox.Text = "";
            company_IdComboBox.Text = "";
            platoon_NameTextBox.Clear();
        }
        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void textBox3_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox4_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void platoon_MasterBindingNavigatorSaveItem_Click(object sender, EventArgs e)
        {
            if(battalion_IdComboBox.Text.Length!=0 && company_IdComboBox.Text.Length!=0 && platoon_IdTextBox.Text.Length!=0 && platoon_NameTextBox.Text.Length!=0 && platoon_DescTextBox.Text.Length!=0)
        {
            a1.insert_platoon(battalion_IdComboBox.Text,company_IdComboBox.Text, platoon_IdTextBox.Text, platoon_NameTextBox.Text, platoon_DescTextBox.Text);
            MessageBox.Show("Record Inserted Successfully !");

        }
        else
            {
                MessageBox.Show("Please enter all values !", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }
        private void Form3_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'dataSet1.Company_Master' table. You can move, or remove it, as needed.
            this.company_MasterTableAdapter.Fill(this.dataSet1.Company_Master);
            // TODO: This line of code loads data into the 'dataSet1.Battalion_Master' table. You can move, or remove it, as needed.
            this.battalion_MasterTableAdapter.Fill(this.dataSet1.Battalion_Master);
            // TODO: This line of code loads data into the 'dataSet1.Platoon_Master' table. You can move, or remove it, as needed.
            this.platoon_MasterTableAdapter.Fill(this.dataSet1.Platoon_Master);
            //clearfn();
        }

        private void battalion_IdTextBox_TextChanged(object sender, EventArgs e)
        {

        }

        private void button4_Click(object sender, EventArgs e)
        {
            
        }

        private void button2_Click(object sender, EventArgs e)
        {
            
        }

        private void button3_Click(object sender, EventArgs e)
        {
            Psearch P1 = new Psearch();
            P1.ShowDialog();
         
        }

        private void bindingNavigatorAddNewItem_Click(object sender, EventArgs e)
        {

        }

        private void bindingNavigatorDeleteItem_Click(object sender, EventArgs e)
        {

        }

        private void toolStripButton1_Click(object sender, EventArgs e)
        {
            a1.delete_platoon(platoon_IdTextBox.Text);
            MessageBox.Show("Record Deleted Successfully !");
        }

        private void toolStripButton2_Click(object sender, EventArgs e)
        {
            a1.update_platoon(battalion_IdComboBox.Text, company_IdComboBox.Text, platoon_IdTextBox.Text, platoon_NameTextBox.Text, platoon_DescTextBox.Text);
            MessageBox.Show("Record Updated Successfully !");
        }

        private void button1_Click(object sender, EventArgs e)
        {
            DataTable dt = new DataTable();
            dt = a1.search_platoon(platoon_IdTextBox.Text);
            if (dt.Rows.Count > 0)
            {
                MessageBox.Show("Already Exists !");
                platoon_IdTextBox.Text = "";
                platoon_IdTextBox.Focus();
            }
            else
            {
                MessageBox.Show("Platoon_Id Available !");
            }
        }

        private void battalion_IdLabel_Click(object sender, EventArgs e)
        {

        }

        private void battalion_IdComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void platoon_IdTextBox_TextChanged(object sender, EventArgs e)
        {

        }
    }
}