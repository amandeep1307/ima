namespace Military_Project
{
    partial class Psearch
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.Label platoon_IdLabel;
            this.dataSet1 = new Military_Project.DataSet1();
            this.platoon_MasterBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.platoon_MasterTableAdapter = new Military_Project.DataSet1TableAdapters.Platoon_MasterTableAdapter();
            this.platoon_MasterDataGridView = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            platoon_IdLabel = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.platoon_MasterBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.platoon_MasterDataGridView)).BeginInit();
            this.SuspendLayout();
            // 
            // platoon_IdLabel
            // 
            platoon_IdLabel.AutoSize = true;
            platoon_IdLabel.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            platoon_IdLabel.Location = new System.Drawing.Point(191, 148);
            platoon_IdLabel.Name = "platoon_IdLabel";
            platoon_IdLabel.Size = new System.Drawing.Size(71, 17);
            platoon_IdLabel.TabIndex = 1;
            platoon_IdLabel.Text = "Platoon Id:";
            platoon_IdLabel.Click += new System.EventHandler(this.platoon_IdLabel_Click);
            // 
            // dataSet1
            // 
            this.dataSet1.DataSetName = "DataSet1";
            this.dataSet1.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // platoon_MasterBindingSource
            // 
            this.platoon_MasterBindingSource.DataMember = "Platoon_Master";
            this.platoon_MasterBindingSource.DataSource = this.dataSet1;
            // 
            // platoon_MasterTableAdapter
            // 
            this.platoon_MasterTableAdapter.ClearBeforeFill = true;
            // 
            // platoon_MasterDataGridView
            // 
            this.platoon_MasterDataGridView.AutoGenerateColumns = false;
            this.platoon_MasterDataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn1,
            this.dataGridViewTextBoxColumn2,
            this.dataGridViewTextBoxColumn3,
            this.dataGridViewTextBoxColumn4,
            this.dataGridViewTextBoxColumn5});
            this.platoon_MasterDataGridView.DataSource = this.platoon_MasterBindingSource;
            this.platoon_MasterDataGridView.Location = new System.Drawing.Point(75, 200);
            this.platoon_MasterDataGridView.Name = "platoon_MasterDataGridView";
            this.platoon_MasterDataGridView.Size = new System.Drawing.Size(543, 224);
            this.platoon_MasterDataGridView.TabIndex = 3;
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.DataPropertyName = "Battalion_Id";
            this.dataGridViewTextBoxColumn1.HeaderText = "Battalion_Id";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.DataPropertyName = "Company_Id";
            this.dataGridViewTextBoxColumn2.HeaderText = "Company_Id";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.DataPropertyName = "Platoon_Id";
            this.dataGridViewTextBoxColumn3.HeaderText = "Platoon_Id";
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            // 
            // dataGridViewTextBoxColumn4
            // 
            this.dataGridViewTextBoxColumn4.DataPropertyName = "Platoon_Name";
            this.dataGridViewTextBoxColumn4.HeaderText = "Platoon_Name";
            this.dataGridViewTextBoxColumn4.Name = "dataGridViewTextBoxColumn4";
            // 
            // dataGridViewTextBoxColumn5
            // 
            this.dataGridViewTextBoxColumn5.DataPropertyName = "Platoon_Desc";
            this.dataGridViewTextBoxColumn5.HeaderText = "Platoon_Desc";
            this.dataGridViewTextBoxColumn5.Name = "dataGridViewTextBoxColumn5";
            // 
            // comboBox1
            // 
            this.comboBox1.DataSource = this.platoon_MasterBindingSource;
            this.comboBox1.DisplayMember = "Platoon_Id";
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Location = new System.Drawing.Point(304, 147);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(176, 21);
            this.comboBox1.TabIndex = 4;
            this.comboBox1.SelectedIndexChanged += new System.EventHandler(this.comboBox1_SelectedIndexChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Times New Roman", 15.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(270, 53);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(147, 24);
            this.label1.TabIndex = 5;
            this.label1.Text = "Platoon Search";
            // 
            // Psearch
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(684, 487);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.comboBox1);
            this.Controls.Add(this.platoon_MasterDataGridView);
            this.Controls.Add(platoon_IdLabel);
            this.Name = "Psearch";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Platoon_Search";
            this.Load += new System.EventHandler(this.Psearch_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataSet1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.platoon_MasterBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.platoon_MasterDataGridView)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DataSet1 dataSet1;
        private System.Windows.Forms.BindingSource platoon_MasterBindingSource;
        private Military_Project.DataSet1TableAdapters.Platoon_MasterTableAdapter platoon_MasterTableAdapter;
        private System.Windows.Forms.DataGridView platoon_MasterDataGridView;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn5;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.Label label1;
    }
}