using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Military_Project
{
    public partial class Psearch : Form
    {
        public Psearch()
        {
            InitializeComponent();
        }
        DataSet1TableAdapters.Platoon_MasterTableAdapter PS = new Military_Project.DataSet1TableAdapters.Platoon_MasterTableAdapter();

        private void platoon_MasterBindingNavigatorSaveItem_Click(object sender, EventArgs e)
        {
            this.Validate();
            this.platoon_MasterBindingSource.EndEdit();
            this.platoon_MasterTableAdapter.Update(this.dataSet1.Platoon_Master);

        }

        private void Psearch_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'dataSet1.Platoon_Master' table. You can move, or remove it, as needed.
            this.platoon_MasterTableAdapter.Fill(this.dataSet1.Platoon_Master);
            comboBox1.Text = "";
        }

        private void platoon_IdTextBox_TextChanged(object sender, EventArgs e)
        {
            DataTable dt = new DataTable();
            dt = PS.search_platoon(comboBox1.Text);
            if (dt.Rows.Count > 0)
            {
                platoon_MasterDataGridView.DataSource = dt;
            }
        }

        private void platoon_IdLabel_Click(object sender, EventArgs e)
        {

        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            DataTable dt = new DataTable();
            dt = PS.search_platoon(comboBox1.Text);
            if (dt.Rows.Count > 0)
            {
                platoon_MasterDataGridView.DataSource= dt;
            }
        }
    }
}