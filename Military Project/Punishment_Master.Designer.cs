namespace Military_Project
{
    partial class Punishment_Master
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.Label punishment_IdLabel;
            System.Windows.Forms.Label commentsLabel;
            System.Windows.Forms.Label soldier_NameLabel;
            System.Windows.Forms.Label type_Of_PunishmentLabel;
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Punishment_Master));
            this.punishment_MasterBindingNavigator = new System.Windows.Forms.BindingNavigator(this.components);
            this.bindingNavigatorAddNewItem = new System.Windows.Forms.ToolStripButton();
            this.punishment_MasterBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataSet1 = new Military_Project.DataSet1();
            this.bindingNavigatorCountItem = new System.Windows.Forms.ToolStripLabel();
            this.bindingNavigatorMoveFirstItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorMovePreviousItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorSeparator = new System.Windows.Forms.ToolStripSeparator();
            this.bindingNavigatorPositionItem = new System.Windows.Forms.ToolStripTextBox();
            this.bindingNavigatorSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.bindingNavigatorMoveNextItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorMoveLastItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripButton1 = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton2 = new System.Windows.Forms.ToolStripButton();
            this.punishment_MasterBindingNavigatorSaveItem = new System.Windows.Forms.ToolStripButton();
            this.punishment_IdTextBox = new System.Windows.Forms.TextBox();
            this.commentsTextBox = new System.Windows.Forms.TextBox();
            this.punishment_MasterTableAdapter = new Military_Project.DataSet1TableAdapters.Punishment_MasterTableAdapter();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.soldier_NameComboBox = new System.Windows.Forms.ComboBox();
            this.soldierMasterBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.type_Of_PunishmentComboBox = new System.Windows.Forms.ComboBox();
            this.soldier_MasterTableAdapter = new Military_Project.DataSet1TableAdapters.Soldier_MasterTableAdapter();
            this.label1 = new System.Windows.Forms.Label();
            punishment_IdLabel = new System.Windows.Forms.Label();
            commentsLabel = new System.Windows.Forms.Label();
            soldier_NameLabel = new System.Windows.Forms.Label();
            type_Of_PunishmentLabel = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.punishment_MasterBindingNavigator)).BeginInit();
            this.punishment_MasterBindingNavigator.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.punishment_MasterBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.soldierMasterBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // punishment_IdLabel
            // 
            punishment_IdLabel.AutoSize = true;
            punishment_IdLabel.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            punishment_IdLabel.Location = new System.Drawing.Point(156, 174);
            punishment_IdLabel.Name = "punishment_IdLabel";
            punishment_IdLabel.Size = new System.Drawing.Size(95, 17);
            punishment_IdLabel.TabIndex = 1;
            punishment_IdLabel.Text = "Punishment Id:";
            // 
            // commentsLabel
            // 
            commentsLabel.AutoSize = true;
            commentsLabel.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            commentsLabel.Location = new System.Drawing.Point(156, 299);
            commentsLabel.Name = "commentsLabel";
            commentsLabel.Size = new System.Drawing.Size(74, 17);
            commentsLabel.TabIndex = 7;
            commentsLabel.Text = "Comments:";
            // 
            // soldier_NameLabel
            // 
            soldier_NameLabel.AutoSize = true;
            soldier_NameLabel.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            soldier_NameLabel.Location = new System.Drawing.Point(156, 215);
            soldier_NameLabel.Name = "soldier_NameLabel";
            soldier_NameLabel.Size = new System.Drawing.Size(91, 17);
            soldier_NameLabel.TabIndex = 15;
            soldier_NameLabel.Text = "Soldier Name:";
            // 
            // type_Of_PunishmentLabel
            // 
            type_Of_PunishmentLabel.AutoSize = true;
            type_Of_PunishmentLabel.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            type_Of_PunishmentLabel.Location = new System.Drawing.Point(156, 257);
            type_Of_PunishmentLabel.Name = "type_Of_PunishmentLabel";
            type_Of_PunishmentLabel.Size = new System.Drawing.Size(132, 17);
            type_Of_PunishmentLabel.TabIndex = 16;
            type_Of_PunishmentLabel.Text = "Type Of Punishment:";
            // 
            // punishment_MasterBindingNavigator
            // 
            this.punishment_MasterBindingNavigator.AddNewItem = this.bindingNavigatorAddNewItem;
            this.punishment_MasterBindingNavigator.BindingSource = this.punishment_MasterBindingSource;
            this.punishment_MasterBindingNavigator.CountItem = this.bindingNavigatorCountItem;
            this.punishment_MasterBindingNavigator.DeleteItem = null;
            this.punishment_MasterBindingNavigator.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.bindingNavigatorMoveFirstItem,
            this.bindingNavigatorMovePreviousItem,
            this.bindingNavigatorSeparator,
            this.bindingNavigatorPositionItem,
            this.bindingNavigatorCountItem,
            this.bindingNavigatorSeparator1,
            this.bindingNavigatorMoveNextItem,
            this.bindingNavigatorMoveLastItem,
            this.bindingNavigatorSeparator2,
            this.bindingNavigatorAddNewItem,
            this.toolStripButton1,
            this.toolStripButton2,
            this.punishment_MasterBindingNavigatorSaveItem});
            this.punishment_MasterBindingNavigator.Location = new System.Drawing.Point(0, 0);
            this.punishment_MasterBindingNavigator.MoveFirstItem = this.bindingNavigatorMoveFirstItem;
            this.punishment_MasterBindingNavigator.MoveLastItem = this.bindingNavigatorMoveLastItem;
            this.punishment_MasterBindingNavigator.MoveNextItem = this.bindingNavigatorMoveNextItem;
            this.punishment_MasterBindingNavigator.MovePreviousItem = this.bindingNavigatorMovePreviousItem;
            this.punishment_MasterBindingNavigator.Name = "punishment_MasterBindingNavigator";
            this.punishment_MasterBindingNavigator.PositionItem = this.bindingNavigatorPositionItem;
            this.punishment_MasterBindingNavigator.Size = new System.Drawing.Size(684, 25);
            this.punishment_MasterBindingNavigator.TabIndex = 0;
            this.punishment_MasterBindingNavigator.Text = "bindingNavigator1";
            // 
            // bindingNavigatorAddNewItem
            // 
            this.bindingNavigatorAddNewItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorAddNewItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorAddNewItem.Image")));
            this.bindingNavigatorAddNewItem.Name = "bindingNavigatorAddNewItem";
            this.bindingNavigatorAddNewItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorAddNewItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorAddNewItem.Text = "Add new data";
            this.bindingNavigatorAddNewItem.Click += new System.EventHandler(this.bindingNavigatorAddNewItem_Click);
            // 
            // punishment_MasterBindingSource
            // 
            this.punishment_MasterBindingSource.DataMember = "Punishment_Master";
            this.punishment_MasterBindingSource.DataSource = this.dataSet1;
            // 
            // dataSet1
            // 
            this.dataSet1.DataSetName = "DataSet1";
            this.dataSet1.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // bindingNavigatorCountItem
            // 
            this.bindingNavigatorCountItem.Name = "bindingNavigatorCountItem";
            this.bindingNavigatorCountItem.Size = new System.Drawing.Size(35, 22);
            this.bindingNavigatorCountItem.Text = "of {0}";
            this.bindingNavigatorCountItem.ToolTipText = "Total number of items";
            // 
            // bindingNavigatorMoveFirstItem
            // 
            this.bindingNavigatorMoveFirstItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMoveFirstItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveFirstItem.Image")));
            this.bindingNavigatorMoveFirstItem.Name = "bindingNavigatorMoveFirstItem";
            this.bindingNavigatorMoveFirstItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMoveFirstItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorMoveFirstItem.Text = "Move first";
            // 
            // bindingNavigatorMovePreviousItem
            // 
            this.bindingNavigatorMovePreviousItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMovePreviousItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMovePreviousItem.Image")));
            this.bindingNavigatorMovePreviousItem.Name = "bindingNavigatorMovePreviousItem";
            this.bindingNavigatorMovePreviousItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMovePreviousItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorMovePreviousItem.Text = "Move previous";
            // 
            // bindingNavigatorSeparator
            // 
            this.bindingNavigatorSeparator.Name = "bindingNavigatorSeparator";
            this.bindingNavigatorSeparator.Size = new System.Drawing.Size(6, 25);
            // 
            // bindingNavigatorPositionItem
            // 
            this.bindingNavigatorPositionItem.AccessibleName = "Position";
            this.bindingNavigatorPositionItem.AutoSize = false;
            this.bindingNavigatorPositionItem.Name = "bindingNavigatorPositionItem";
            this.bindingNavigatorPositionItem.Size = new System.Drawing.Size(50, 23);
            this.bindingNavigatorPositionItem.Text = "0";
            this.bindingNavigatorPositionItem.ToolTipText = "Current position";
            // 
            // bindingNavigatorSeparator1
            // 
            this.bindingNavigatorSeparator1.Name = "bindingNavigatorSeparator1";
            this.bindingNavigatorSeparator1.Size = new System.Drawing.Size(6, 25);
            // 
            // bindingNavigatorMoveNextItem
            // 
            this.bindingNavigatorMoveNextItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMoveNextItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveNextItem.Image")));
            this.bindingNavigatorMoveNextItem.Name = "bindingNavigatorMoveNextItem";
            this.bindingNavigatorMoveNextItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMoveNextItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorMoveNextItem.Text = "Move next";
            // 
            // bindingNavigatorMoveLastItem
            // 
            this.bindingNavigatorMoveLastItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMoveLastItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveLastItem.Image")));
            this.bindingNavigatorMoveLastItem.Name = "bindingNavigatorMoveLastItem";
            this.bindingNavigatorMoveLastItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMoveLastItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorMoveLastItem.Text = "Move last";
            // 
            // bindingNavigatorSeparator2
            // 
            this.bindingNavigatorSeparator2.Name = "bindingNavigatorSeparator2";
            this.bindingNavigatorSeparator2.Size = new System.Drawing.Size(6, 25);
            // 
            // toolStripButton1
            // 
            this.toolStripButton1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton1.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton1.Image")));
            this.toolStripButton1.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton1.Name = "toolStripButton1";
            this.toolStripButton1.Size = new System.Drawing.Size(23, 22);
            this.toolStripButton1.Text = "Delete";
            this.toolStripButton1.Click += new System.EventHandler(this.toolStripButton1_Click);
            // 
            // toolStripButton2
            // 
            this.toolStripButton2.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton2.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton2.Image")));
            this.toolStripButton2.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton2.Name = "toolStripButton2";
            this.toolStripButton2.Size = new System.Drawing.Size(23, 22);
            this.toolStripButton2.Text = "Update";
            this.toolStripButton2.Click += new System.EventHandler(this.toolStripButton2_Click);
            // 
            // punishment_MasterBindingNavigatorSaveItem
            // 
            this.punishment_MasterBindingNavigatorSaveItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.punishment_MasterBindingNavigatorSaveItem.Image = ((System.Drawing.Image)(resources.GetObject("punishment_MasterBindingNavigatorSaveItem.Image")));
            this.punishment_MasterBindingNavigatorSaveItem.Name = "punishment_MasterBindingNavigatorSaveItem";
            this.punishment_MasterBindingNavigatorSaveItem.Size = new System.Drawing.Size(23, 22);
            this.punishment_MasterBindingNavigatorSaveItem.Text = "Save";
            this.punishment_MasterBindingNavigatorSaveItem.Click += new System.EventHandler(this.punishment_MasterBindingNavigatorSaveItem_Click);
            // 
            // punishment_IdTextBox
            // 
            this.punishment_IdTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.punishment_MasterBindingSource, "Punishment_Id", true));
            this.punishment_IdTextBox.Location = new System.Drawing.Point(294, 174);
            this.punishment_IdTextBox.Name = "punishment_IdTextBox";
            this.punishment_IdTextBox.Size = new System.Drawing.Size(181, 20);
            this.punishment_IdTextBox.TabIndex = 2;
            // 
            // commentsTextBox
            // 
            this.commentsTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.punishment_MasterBindingSource, "Comments", true));
            this.commentsTextBox.Location = new System.Drawing.Point(294, 299);
            this.commentsTextBox.Multiline = true;
            this.commentsTextBox.Name = "commentsTextBox";
            this.commentsTextBox.Size = new System.Drawing.Size(181, 62);
            this.commentsTextBox.TabIndex = 8;
            // 
            // punishment_MasterTableAdapter
            // 
            this.punishment_MasterTableAdapter.ClearBeforeFill = true;
            // 
            // button1
            // 
            this.button1.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.Location = new System.Drawing.Point(513, 338);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 9;
            this.button1.Text = "Search";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button2
            // 
            this.button2.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button2.Location = new System.Drawing.Point(481, 171);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(129, 23);
            this.button2.TabIndex = 15;
            this.button2.Text = "Check Availability?";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // soldier_NameComboBox
            // 
            this.soldier_NameComboBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.punishment_MasterBindingSource, "Soldier_Name", true));
            this.soldier_NameComboBox.DataSource = this.soldierMasterBindingSource;
            this.soldier_NameComboBox.DisplayMember = "Ename";
            this.soldier_NameComboBox.FormattingEnabled = true;
            this.soldier_NameComboBox.Location = new System.Drawing.Point(294, 215);
            this.soldier_NameComboBox.Name = "soldier_NameComboBox";
            this.soldier_NameComboBox.Size = new System.Drawing.Size(181, 21);
            this.soldier_NameComboBox.TabIndex = 16;
            // 
            // soldierMasterBindingSource
            // 
            this.soldierMasterBindingSource.DataMember = "Soldier_Master";
            this.soldierMasterBindingSource.DataSource = this.dataSet1;
            // 
            // type_Of_PunishmentComboBox
            // 
            this.type_Of_PunishmentComboBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.punishment_MasterBindingSource, "Type_Of_Punishment", true));
            this.type_Of_PunishmentComboBox.FormattingEnabled = true;
            this.type_Of_PunishmentComboBox.Items.AddRange(new object[] {
            "Red Ink",
            "Black Ink"});
            this.type_Of_PunishmentComboBox.Location = new System.Drawing.Point(294, 257);
            this.type_Of_PunishmentComboBox.Name = "type_Of_PunishmentComboBox";
            this.type_Of_PunishmentComboBox.Size = new System.Drawing.Size(181, 21);
            this.type_Of_PunishmentComboBox.TabIndex = 17;
            // 
            // soldier_MasterTableAdapter
            // 
            this.soldier_MasterTableAdapter.ClearBeforeFill = true;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Times New Roman", 15.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(275, 69);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(183, 24);
            this.label1.TabIndex = 18;
            this.label1.Text = "Punishment Master";
            // 
            // Punishment_Master
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(684, 487);
            this.Controls.Add(this.label1);
            this.Controls.Add(type_Of_PunishmentLabel);
            this.Controls.Add(this.type_Of_PunishmentComboBox);
            this.Controls.Add(soldier_NameLabel);
            this.Controls.Add(this.soldier_NameComboBox);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(punishment_IdLabel);
            this.Controls.Add(this.punishment_IdTextBox);
            this.Controls.Add(commentsLabel);
            this.Controls.Add(this.commentsTextBox);
            this.Controls.Add(this.punishment_MasterBindingNavigator);
            this.Name = "Punishment_Master";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Punishment_Master";
            this.Load += new System.EventHandler(this.Punishment_Master_Load);
            ((System.ComponentModel.ISupportInitialize)(this.punishment_MasterBindingNavigator)).EndInit();
            this.punishment_MasterBindingNavigator.ResumeLayout(false);
            this.punishment_MasterBindingNavigator.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.punishment_MasterBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.soldierMasterBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DataSet1 dataSet1;
        private System.Windows.Forms.BindingSource punishment_MasterBindingSource;
        private Military_Project.DataSet1TableAdapters.Punishment_MasterTableAdapter punishment_MasterTableAdapter;
        private System.Windows.Forms.BindingNavigator punishment_MasterBindingNavigator;
        private System.Windows.Forms.ToolStripButton bindingNavigatorAddNewItem;
        private System.Windows.Forms.ToolStripLabel bindingNavigatorCountItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMoveFirstItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMovePreviousItem;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator;
        private System.Windows.Forms.ToolStripTextBox bindingNavigatorPositionItem;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator1;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMoveNextItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMoveLastItem;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator2;
        private System.Windows.Forms.ToolStripButton punishment_MasterBindingNavigatorSaveItem;
        private System.Windows.Forms.TextBox punishment_IdTextBox;
        private System.Windows.Forms.TextBox commentsTextBox;
        private System.Windows.Forms.ToolStripButton toolStripButton1;
        private System.Windows.Forms.ToolStripButton toolStripButton2;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.ComboBox soldier_NameComboBox;
        private System.Windows.Forms.ComboBox type_Of_PunishmentComboBox;
        private System.Windows.Forms.BindingSource soldierMasterBindingSource;
        private Military_Project.DataSet1TableAdapters.Soldier_MasterTableAdapter soldier_MasterTableAdapter;
        private System.Windows.Forms.Label label1;
    }
}