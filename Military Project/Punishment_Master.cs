using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Military_Project
{
    public partial class Punishment_Master : Form
    {
        public Punishment_Master()
        {
            InitializeComponent();
        }
        DataSet1TableAdapters.Punishment_MasterTableAdapter a1 = new Military_Project.DataSet1TableAdapters.Punishment_MasterTableAdapter();
        private void clearfn()
        {
            punishment_IdTextBox.Clear();
            soldier_NameComboBox.Text = "";
            type_Of_PunishmentComboBox.Text = "";
            commentsTextBox.Clear();
        }
        private void punishment_MasterBindingNavigatorSaveItem_Click(object sender, EventArgs e)
        {
            if(punishment_IdTextBox.Text.Length!=0 && soldier_NameComboBox.Text.Length!=0 && type_Of_PunishmentComboBox.Text.Length!=0 && commentsTextBox.Text.Length!=0)
            {
                 a1.insert_punishment(punishment_IdTextBox.Text, soldier_NameComboBox.Text, type_Of_PunishmentComboBox.Text, commentsTextBox.Text);
                 MessageBox.Show("Record Inserted Successfully !");

            }
          else
            {
                MessageBox.Show("Please enter all values !", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }
        private void Punishment_Master_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'dataSet1.Soldier_Master' table. You can move, or remove it, as needed.
            this.soldier_MasterTableAdapter.Fill(this.dataSet1.Soldier_Master);
            // TODO: This line of code loads data into the 'dataSet1.Punishment_Master' table. You can move, or remove it, as needed.
            this.punishment_MasterTableAdapter.Fill(this.dataSet1.Punishment_Master);

        }

        private void bindingNavigatorAddNewItem_Click(object sender, EventArgs e)
        {

        }

        private void toolStripButton1_Click(object sender, EventArgs e)
        {
            a1.delete_punishment(punishment_IdTextBox.Text);
            MessageBox.Show("Record Deleted Successfully !");
        }

        private void toolStripButton2_Click(object sender, EventArgs e)
        {
            a1.update_punishment(punishment_IdTextBox.Text, soldier_NameComboBox.Text, type_Of_PunishmentComboBox.Text, commentsTextBox.Text);
            MessageBox.Show("Record Updated Successfully !");
        }

        private void button1_Click(object sender, EventArgs e)
        {
            punish_search P1=new punish_search();
            P1.ShowDialog();
            
        }

        private void button2_Click(object sender, EventArgs e)
        {
            DataTable dt = new DataTable();
            dt = a1.search_punishment(punishment_IdTextBox.Text);
            if (dt.Rows.Count > 0)
            {
                MessageBox.Show("Already Exists !");
                punishment_IdTextBox.Text= "";
                punishment_IdTextBox.Focus();
            }
            else
            {
                MessageBox.Show("Punishment_Id Available !");
            }
        }

    }
}