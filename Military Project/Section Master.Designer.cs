namespace Military_Project
{
    partial class Section_Master
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.Label section_NameLabel;
            System.Windows.Forms.Label section_DescLabel;
            System.Windows.Forms.Label battalion_IdLabel;
            System.Windows.Forms.Label company_IdLabel;
            System.Windows.Forms.Label platoon_IdLabel;
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Section_Master));
            this.label1 = new System.Windows.Forms.Label();
            this.section_MasterBindingNavigator = new System.Windows.Forms.BindingNavigator(this.components);
            this.bindingNavigatorAddNewItem = new System.Windows.Forms.ToolStripButton();
            this.section_MasterBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataSet1 = new Military_Project.DataSet1();
            this.bindingNavigatorCountItem = new System.Windows.Forms.ToolStripLabel();
            this.bindingNavigatorMoveFirstItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorMovePreviousItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorSeparator = new System.Windows.Forms.ToolStripSeparator();
            this.bindingNavigatorPositionItem = new System.Windows.Forms.ToolStripTextBox();
            this.bindingNavigatorSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.bindingNavigatorMoveNextItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorMoveLastItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripButton2 = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton1 = new System.Windows.Forms.ToolStripButton();
            this.section_MasterBindingNavigatorSaveItem = new System.Windows.Forms.ToolStripButton();
            this.section_NameTextBox = new System.Windows.Forms.TextBox();
            this.section_DescTextBox = new System.Windows.Forms.TextBox();
            this.button3 = new System.Windows.Forms.Button();
            this.section_MasterTableAdapter = new Military_Project.DataSet1TableAdapters.Section_MasterTableAdapter();
            this.battalion_IdComboBox = new System.Windows.Forms.ComboBox();
            this.battalionMasterBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.platoonMasterBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.company_IdComboBox = new System.Windows.Forms.ComboBox();
            this.companyMasterBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.platoon_IdComboBox = new System.Windows.Forms.ComboBox();
            this.platoon_MasterTableAdapter = new Military_Project.DataSet1TableAdapters.Platoon_MasterTableAdapter();
            this.battalion_MasterTableAdapter = new Military_Project.DataSet1TableAdapters.Battalion_MasterTableAdapter();
            this.company_MasterTableAdapter = new Military_Project.DataSet1TableAdapters.Company_MasterTableAdapter();
            section_NameLabel = new System.Windows.Forms.Label();
            section_DescLabel = new System.Windows.Forms.Label();
            battalion_IdLabel = new System.Windows.Forms.Label();
            company_IdLabel = new System.Windows.Forms.Label();
            platoon_IdLabel = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.section_MasterBindingNavigator)).BeginInit();
            this.section_MasterBindingNavigator.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.section_MasterBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.battalionMasterBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.platoonMasterBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.companyMasterBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // section_NameLabel
            // 
            section_NameLabel.AutoSize = true;
            section_NameLabel.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            section_NameLabel.Location = new System.Drawing.Point(193, 293);
            section_NameLabel.Name = "section_NameLabel";
            section_NameLabel.Size = new System.Drawing.Size(94, 17);
            section_NameLabel.TabIndex = 10;
            section_NameLabel.Text = "Section Name:";
            // 
            // section_DescLabel
            // 
            section_DescLabel.AutoSize = true;
            section_DescLabel.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            section_DescLabel.Location = new System.Drawing.Point(193, 336);
            section_DescLabel.Name = "section_DescLabel";
            section_DescLabel.Size = new System.Drawing.Size(89, 17);
            section_DescLabel.TabIndex = 12;
            section_DescLabel.Text = "Section Desc:";
            // 
            // battalion_IdLabel
            // 
            battalion_IdLabel.AutoSize = true;
            battalion_IdLabel.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            battalion_IdLabel.Location = new System.Drawing.Point(193, 161);
            battalion_IdLabel.Name = "battalion_IdLabel";
            battalion_IdLabel.Size = new System.Drawing.Size(79, 17);
            battalion_IdLabel.TabIndex = 17;
            battalion_IdLabel.Text = "Battalion Id:";
            battalion_IdLabel.Click += new System.EventHandler(this.battalion_IdLabel_Click);
            // 
            // company_IdLabel
            // 
            company_IdLabel.AutoSize = true;
            company_IdLabel.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            company_IdLabel.Location = new System.Drawing.Point(193, 204);
            company_IdLabel.Name = "company_IdLabel";
            company_IdLabel.Size = new System.Drawing.Size(83, 17);
            company_IdLabel.TabIndex = 18;
            company_IdLabel.Text = "Company Id:";
            // 
            // platoon_IdLabel
            // 
            platoon_IdLabel.AutoSize = true;
            platoon_IdLabel.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            platoon_IdLabel.Location = new System.Drawing.Point(193, 248);
            platoon_IdLabel.Name = "platoon_IdLabel";
            platoon_IdLabel.Size = new System.Drawing.Size(71, 17);
            platoon_IdLabel.TabIndex = 19;
            platoon_IdLabel.Text = "Platoon Id:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Times New Roman", 15.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(280, 66);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(144, 24);
            this.label1.TabIndex = 0;
            this.label1.Text = "Section Master";
            // 
            // section_MasterBindingNavigator
            // 
            this.section_MasterBindingNavigator.AddNewItem = this.bindingNavigatorAddNewItem;
            this.section_MasterBindingNavigator.BindingSource = this.section_MasterBindingSource;
            this.section_MasterBindingNavigator.CountItem = this.bindingNavigatorCountItem;
            this.section_MasterBindingNavigator.DeleteItem = null;
            this.section_MasterBindingNavigator.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.bindingNavigatorMoveFirstItem,
            this.bindingNavigatorMovePreviousItem,
            this.bindingNavigatorSeparator,
            this.bindingNavigatorPositionItem,
            this.bindingNavigatorCountItem,
            this.bindingNavigatorSeparator1,
            this.bindingNavigatorMoveNextItem,
            this.bindingNavigatorMoveLastItem,
            this.bindingNavigatorSeparator2,
            this.bindingNavigatorAddNewItem,
            this.toolStripButton2,
            this.toolStripButton1,
            this.section_MasterBindingNavigatorSaveItem});
            this.section_MasterBindingNavigator.Location = new System.Drawing.Point(0, 0);
            this.section_MasterBindingNavigator.MoveFirstItem = this.bindingNavigatorMoveFirstItem;
            this.section_MasterBindingNavigator.MoveLastItem = this.bindingNavigatorMoveLastItem;
            this.section_MasterBindingNavigator.MoveNextItem = this.bindingNavigatorMoveNextItem;
            this.section_MasterBindingNavigator.MovePreviousItem = this.bindingNavigatorMovePreviousItem;
            this.section_MasterBindingNavigator.Name = "section_MasterBindingNavigator";
            this.section_MasterBindingNavigator.PositionItem = this.bindingNavigatorPositionItem;
            this.section_MasterBindingNavigator.Size = new System.Drawing.Size(684, 25);
            this.section_MasterBindingNavigator.TabIndex = 1;
            this.section_MasterBindingNavigator.Text = "bindingNavigator1";
            this.section_MasterBindingNavigator.RefreshItems += new System.EventHandler(this.section_MasterBindingNavigator_RefreshItems);
            // 
            // bindingNavigatorAddNewItem
            // 
            this.bindingNavigatorAddNewItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorAddNewItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorAddNewItem.Image")));
            this.bindingNavigatorAddNewItem.Name = "bindingNavigatorAddNewItem";
            this.bindingNavigatorAddNewItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorAddNewItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorAddNewItem.Text = "Add new data";
            this.bindingNavigatorAddNewItem.Click += new System.EventHandler(this.bindingNavigatorAddNewItem_Click);
            // 
            // section_MasterBindingSource
            // 
            this.section_MasterBindingSource.DataMember = "Section_Master";
            this.section_MasterBindingSource.DataSource = this.dataSet1;
            // 
            // dataSet1
            // 
            this.dataSet1.DataSetName = "DataSet1";
            this.dataSet1.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // bindingNavigatorCountItem
            // 
            this.bindingNavigatorCountItem.Name = "bindingNavigatorCountItem";
            this.bindingNavigatorCountItem.Size = new System.Drawing.Size(35, 22);
            this.bindingNavigatorCountItem.Text = "of {0}";
            this.bindingNavigatorCountItem.ToolTipText = "Total number of items";
            // 
            // bindingNavigatorMoveFirstItem
            // 
            this.bindingNavigatorMoveFirstItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMoveFirstItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveFirstItem.Image")));
            this.bindingNavigatorMoveFirstItem.Name = "bindingNavigatorMoveFirstItem";
            this.bindingNavigatorMoveFirstItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMoveFirstItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorMoveFirstItem.Text = "Move first";
            // 
            // bindingNavigatorMovePreviousItem
            // 
            this.bindingNavigatorMovePreviousItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMovePreviousItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMovePreviousItem.Image")));
            this.bindingNavigatorMovePreviousItem.Name = "bindingNavigatorMovePreviousItem";
            this.bindingNavigatorMovePreviousItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMovePreviousItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorMovePreviousItem.Text = "Move previous";
            // 
            // bindingNavigatorSeparator
            // 
            this.bindingNavigatorSeparator.Name = "bindingNavigatorSeparator";
            this.bindingNavigatorSeparator.Size = new System.Drawing.Size(6, 25);
            // 
            // bindingNavigatorPositionItem
            // 
            this.bindingNavigatorPositionItem.AccessibleName = "Position";
            this.bindingNavigatorPositionItem.AutoSize = false;
            this.bindingNavigatorPositionItem.Name = "bindingNavigatorPositionItem";
            this.bindingNavigatorPositionItem.Size = new System.Drawing.Size(50, 23);
            this.bindingNavigatorPositionItem.Text = "0";
            this.bindingNavigatorPositionItem.ToolTipText = "Current position";
            // 
            // bindingNavigatorSeparator1
            // 
            this.bindingNavigatorSeparator1.Name = "bindingNavigatorSeparator1";
            this.bindingNavigatorSeparator1.Size = new System.Drawing.Size(6, 25);
            // 
            // bindingNavigatorMoveNextItem
            // 
            this.bindingNavigatorMoveNextItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMoveNextItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveNextItem.Image")));
            this.bindingNavigatorMoveNextItem.Name = "bindingNavigatorMoveNextItem";
            this.bindingNavigatorMoveNextItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMoveNextItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorMoveNextItem.Text = "Move next";
            // 
            // bindingNavigatorMoveLastItem
            // 
            this.bindingNavigatorMoveLastItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMoveLastItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveLastItem.Image")));
            this.bindingNavigatorMoveLastItem.Name = "bindingNavigatorMoveLastItem";
            this.bindingNavigatorMoveLastItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMoveLastItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorMoveLastItem.Text = "Move last";
            // 
            // bindingNavigatorSeparator2
            // 
            this.bindingNavigatorSeparator2.Name = "bindingNavigatorSeparator2";
            this.bindingNavigatorSeparator2.Size = new System.Drawing.Size(6, 25);
            // 
            // toolStripButton2
            // 
            this.toolStripButton2.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton2.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton2.Image")));
            this.toolStripButton2.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton2.Name = "toolStripButton2";
            this.toolStripButton2.Size = new System.Drawing.Size(23, 22);
            this.toolStripButton2.Text = "Delete";
            this.toolStripButton2.Click += new System.EventHandler(this.toolStripButton2_Click);
            // 
            // toolStripButton1
            // 
            this.toolStripButton1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton1.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton1.Image")));
            this.toolStripButton1.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton1.Name = "toolStripButton1";
            this.toolStripButton1.Size = new System.Drawing.Size(23, 22);
            this.toolStripButton1.Text = "Update";
            this.toolStripButton1.Click += new System.EventHandler(this.toolStripButton1_Click);
            // 
            // section_MasterBindingNavigatorSaveItem
            // 
            this.section_MasterBindingNavigatorSaveItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.section_MasterBindingNavigatorSaveItem.Image = ((System.Drawing.Image)(resources.GetObject("section_MasterBindingNavigatorSaveItem.Image")));
            this.section_MasterBindingNavigatorSaveItem.Name = "section_MasterBindingNavigatorSaveItem";
            this.section_MasterBindingNavigatorSaveItem.Size = new System.Drawing.Size(23, 22);
            this.section_MasterBindingNavigatorSaveItem.Text = "Save";
            this.section_MasterBindingNavigatorSaveItem.Click += new System.EventHandler(this.section_MasterBindingNavigatorSaveItem_Click);
            // 
            // section_NameTextBox
            // 
            this.section_NameTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.section_MasterBindingSource, "Platoon_Id", true));
            this.section_NameTextBox.Location = new System.Drawing.Point(284, 290);
            this.section_NameTextBox.Name = "section_NameTextBox";
            this.section_NameTextBox.Size = new System.Drawing.Size(190, 20);
            this.section_NameTextBox.TabIndex = 11;
            // 
            // section_DescTextBox
            // 
            this.section_DescTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.section_MasterBindingSource, "Section_Desc", true));
            this.section_DescTextBox.Location = new System.Drawing.Point(284, 333);
            this.section_DescTextBox.Name = "section_DescTextBox";
            this.section_DescTextBox.Size = new System.Drawing.Size(190, 20);
            this.section_DescTextBox.TabIndex = 13;
            // 
            // button3
            // 
            this.button3.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button3.Location = new System.Drawing.Point(533, 330);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(75, 23);
            this.button3.TabIndex = 16;
            this.button3.Text = "Search";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // section_MasterTableAdapter
            // 
            this.section_MasterTableAdapter.ClearBeforeFill = true;
            // 
            // battalion_IdComboBox
            // 
            this.battalion_IdComboBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.section_MasterBindingSource, "Battalion_Id", true));
            this.battalion_IdComboBox.DataSource = this.battalionMasterBindingSource;
            this.battalion_IdComboBox.DisplayMember = "Battalion_Id";
            this.battalion_IdComboBox.FormattingEnabled = true;
            this.battalion_IdComboBox.Location = new System.Drawing.Point(284, 156);
            this.battalion_IdComboBox.Name = "battalion_IdComboBox";
            this.battalion_IdComboBox.Size = new System.Drawing.Size(190, 21);
            this.battalion_IdComboBox.TabIndex = 18;
            // 
            // battalionMasterBindingSource
            // 
            this.battalionMasterBindingSource.DataMember = "Battalion_Master";
            this.battalionMasterBindingSource.DataSource = this.dataSet1;
            // 
            // platoonMasterBindingSource
            // 
            this.platoonMasterBindingSource.DataMember = "Platoon_Master";
            this.platoonMasterBindingSource.DataSource = this.dataSet1;
            // 
            // company_IdComboBox
            // 
            this.company_IdComboBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.section_MasterBindingSource, "Company_Id", true));
            this.company_IdComboBox.DataSource = this.companyMasterBindingSource;
            this.company_IdComboBox.DisplayMember = "Company_Id";
            this.company_IdComboBox.FormattingEnabled = true;
            this.company_IdComboBox.Location = new System.Drawing.Point(284, 201);
            this.company_IdComboBox.Name = "company_IdComboBox";
            this.company_IdComboBox.Size = new System.Drawing.Size(190, 21);
            this.company_IdComboBox.TabIndex = 19;
            // 
            // companyMasterBindingSource
            // 
            this.companyMasterBindingSource.DataMember = "Company_Master";
            this.companyMasterBindingSource.DataSource = this.dataSet1;
            // 
            // platoon_IdComboBox
            // 
            this.platoon_IdComboBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.section_MasterBindingSource, "Platoon_Id", true));
            this.platoon_IdComboBox.DataSource = this.platoonMasterBindingSource;
            this.platoon_IdComboBox.DisplayMember = "Platoon_Id";
            this.platoon_IdComboBox.FormattingEnabled = true;
            this.platoon_IdComboBox.Location = new System.Drawing.Point(284, 245);
            this.platoon_IdComboBox.Name = "platoon_IdComboBox";
            this.platoon_IdComboBox.Size = new System.Drawing.Size(190, 21);
            this.platoon_IdComboBox.TabIndex = 20;
            // 
            // platoon_MasterTableAdapter
            // 
            this.platoon_MasterTableAdapter.ClearBeforeFill = true;
            // 
            // battalion_MasterTableAdapter
            // 
            this.battalion_MasterTableAdapter.ClearBeforeFill = true;
            // 
            // company_MasterTableAdapter
            // 
            this.company_MasterTableAdapter.ClearBeforeFill = true;
            // 
            // Section_Master
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(684, 487);
            this.Controls.Add(platoon_IdLabel);
            this.Controls.Add(this.platoon_IdComboBox);
            this.Controls.Add(company_IdLabel);
            this.Controls.Add(this.company_IdComboBox);
            this.Controls.Add(battalion_IdLabel);
            this.Controls.Add(this.battalion_IdComboBox);
            this.Controls.Add(this.button3);
            this.Controls.Add(section_NameLabel);
            this.Controls.Add(this.section_NameTextBox);
            this.Controls.Add(section_DescLabel);
            this.Controls.Add(this.section_DescTextBox);
            this.Controls.Add(this.section_MasterBindingNavigator);
            this.Controls.Add(this.label1);
            this.Name = "Section_Master";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Section_Master";
            this.Load += new System.EventHandler(this.Section_Master_Load);
            ((System.ComponentModel.ISupportInitialize)(this.section_MasterBindingNavigator)).EndInit();
            this.section_MasterBindingNavigator.ResumeLayout(false);
            this.section_MasterBindingNavigator.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.section_MasterBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.battalionMasterBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.platoonMasterBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.companyMasterBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private DataSet1 dataSet1;
        private System.Windows.Forms.BindingSource section_MasterBindingSource;
        private Military_Project.DataSet1TableAdapters.Section_MasterTableAdapter section_MasterTableAdapter;
        private System.Windows.Forms.BindingNavigator section_MasterBindingNavigator;
        private System.Windows.Forms.ToolStripButton bindingNavigatorAddNewItem;
        private System.Windows.Forms.ToolStripLabel bindingNavigatorCountItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMoveFirstItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMovePreviousItem;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator;
        private System.Windows.Forms.ToolStripTextBox bindingNavigatorPositionItem;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator1;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMoveNextItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMoveLastItem;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator2;
        private System.Windows.Forms.ToolStripButton section_MasterBindingNavigatorSaveItem;
        private System.Windows.Forms.TextBox section_NameTextBox;
        private System.Windows.Forms.TextBox section_DescTextBox;
        private System.Windows.Forms.Button button3;
       // private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
       // private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
        private System.Windows.Forms.ToolStripButton toolStripButton1;
        private System.Windows.Forms.ToolStripButton toolStripButton2;
        private System.Windows.Forms.ComboBox battalion_IdComboBox;
        private System.Windows.Forms.ComboBox company_IdComboBox;
        private System.Windows.Forms.ComboBox platoon_IdComboBox;
        private System.Windows.Forms.BindingSource platoonMasterBindingSource;
        private Military_Project.DataSet1TableAdapters.Platoon_MasterTableAdapter platoon_MasterTableAdapter;
        private System.Windows.Forms.BindingSource battalionMasterBindingSource;
        private Military_Project.DataSet1TableAdapters.Battalion_MasterTableAdapter battalion_MasterTableAdapter;
        private System.Windows.Forms.BindingSource companyMasterBindingSource;
        private Military_Project.DataSet1TableAdapters.Company_MasterTableAdapter company_MasterTableAdapter;
    }
}