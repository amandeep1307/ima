using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Military_Project
{
    public partial class Section_Master : Form
    {
        public Section_Master()
        {
            InitializeComponent();
        }
        DataSet1TableAdapters.Section_MasterTableAdapter a1 = new Military_Project.DataSet1TableAdapters.Section_MasterTableAdapter();
        //private void clearfn()
        //{
        //    section_DescTextBox.Clear();
        //    section_NameTextBox.Clear();
        //    battalion_IdComboBox.Text = "";
        //    company_IdComboBox.Text = "";
        //    platoon_IdComboBox.Text = "";
        //}

        private void section_MasterBindingNavigatorSaveItem_Click(object sender, EventArgs e)
        {
            if (battalion_IdComboBox.Text.Length != 0 && company_IdComboBox.Text.Length != 0 && platoon_IdComboBox.Text.Length != 0 && section_NameTextBox.Text.Length != 0 && section_DescTextBox.Text.Length != 0)
            {
                a1.insert_section(battalion_IdComboBox.Text, company_IdComboBox.Text, platoon_IdComboBox.Text, section_NameTextBox.Text, section_DescTextBox.Text);
                MessageBox.Show("Record Inserted Successfully !");

            }
            else
            {
                MessageBox.Show("Please enter all values", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }
        private void Section_Master_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'dataSet1.Company_Master' table. You can move, or remove it, as needed.
            this.company_MasterTableAdapter.Fill(this.dataSet1.Company_Master);
            // TODO: This line of code loads data into the 'dataSet1.Battalion_Master' table. You can move, or remove it, as needed.
            this.battalion_MasterTableAdapter.Fill(this.dataSet1.Battalion_Master);
            // TODO: This line of code loads data into the 'dataSet1.Platoon_Master' table. You can move, or remove it, as needed.
            this.platoon_MasterTableAdapter.Fill(this.dataSet1.Platoon_Master);
            // TODO: This line of code loads data into the 'dataSet1.Section_Master' table. You can move, or remove it, as needed.
            //this.section_MasterTableAdapter.Fill(this.dataSet1.Section_Master);
            //clearfn();
        }

        private void battalion_IdTextBox_TextChanged(object sender, EventArgs e)
        {

        }
        private void bindingNavigatorAddNewItem_Click(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            
        }

        private void button4_Click(object sender, EventArgs e)
        {
            
        }

        private void button2_Click(object sender, EventArgs e)
        {
            
        }

        private void button3_Click(object sender, EventArgs e)
        {
            sec_search SEC1 = new sec_search();
            SEC1.ShowDialog();
         
        }

        
        private void company_IdLabel_Click(object sender, EventArgs e)
        {

        }

        private void toolStripButton2_Click(object sender, EventArgs e)
        {
            a1.delete_section(section_NameTextBox.Text);
            MessageBox.Show("Record Deleted Successfully !");
        }

        private void toolStripButton1_Click(object sender, EventArgs e)
        {
            a1.update_section(battalion_IdComboBox.Text, company_IdComboBox.Text, platoon_IdComboBox.Text, section_NameTextBox.Text, section_DescTextBox.Text);
            MessageBox.Show("Record Updated Successfully !");
        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            DataTable dt = new DataTable();
            dt = a1.search_section(section_NameTextBox.Text);
            if (dt.Rows.Count > 0)
            {
                MessageBox.Show("Already Exists !");
                section_NameTextBox.Text= "";
                section_NameTextBox.Focus();
            }
            else
            {
                MessageBox.Show("Section_Name Available !");
            }
        }

        private void battalion_IdLabel_Click(object sender, EventArgs e)
        {

        }

        private void section_MasterBindingNavigator_RefreshItems(object sender, EventArgs e)
        {

        }
    }
}