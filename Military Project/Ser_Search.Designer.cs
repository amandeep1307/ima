namespace Military_Project
{
    partial class Ser_Search
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.Label army_NoLabel;
            this.dataSet1 = new Military_Project.DataSet1();
            this.service_RecordBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.service_RecordTableAdapter = new Military_Project.DataSet1TableAdapters.Service_RecordTableAdapter();
            this.army_NoComboBox = new System.Windows.Forms.ComboBox();
            this.service_RecordDataGridView = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn10 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn11 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn12 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn13 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.label1 = new System.Windows.Forms.Label();
            army_NoLabel = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.service_RecordBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.service_RecordDataGridView)).BeginInit();
            this.SuspendLayout();
            // 
            // army_NoLabel
            // 
            army_NoLabel.AutoSize = true;
            army_NoLabel.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            army_NoLabel.Location = new System.Drawing.Point(237, 150);
            army_NoLabel.Name = "army_NoLabel";
            army_NoLabel.Size = new System.Drawing.Size(67, 17);
            army_NoLabel.TabIndex = 1;
            army_NoLabel.Text = "Army No:";
            // 
            // dataSet1
            // 
            this.dataSet1.DataSetName = "DataSet1";
            this.dataSet1.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // service_RecordBindingSource
            // 
            this.service_RecordBindingSource.DataMember = "Service_Record";
            this.service_RecordBindingSource.DataSource = this.dataSet1;
            // 
            // service_RecordTableAdapter
            // 
            this.service_RecordTableAdapter.ClearBeforeFill = true;
            // 
            // army_NoComboBox
            // 
            this.army_NoComboBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.service_RecordBindingSource, "Army_No", true));
            this.army_NoComboBox.DataSource = this.service_RecordBindingSource;
            this.army_NoComboBox.DisplayMember = "Army_No";
            this.army_NoComboBox.FormattingEnabled = true;
            this.army_NoComboBox.Location = new System.Drawing.Point(344, 149);
            this.army_NoComboBox.Name = "army_NoComboBox";
            this.army_NoComboBox.Size = new System.Drawing.Size(159, 21);
            this.army_NoComboBox.TabIndex = 2;
            this.army_NoComboBox.SelectedIndexChanged += new System.EventHandler(this.army_NoComboBox_SelectedIndexChanged);
            // 
            // service_RecordDataGridView
            // 
            this.service_RecordDataGridView.AutoGenerateColumns = false;
            this.service_RecordDataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn1,
            this.dataGridViewTextBoxColumn2,
            this.dataGridViewTextBoxColumn3,
            this.dataGridViewTextBoxColumn10,
            this.dataGridViewTextBoxColumn11,
            this.dataGridViewTextBoxColumn12,
            this.dataGridViewTextBoxColumn13});
            this.service_RecordDataGridView.DataSource = this.service_RecordBindingSource;
            this.service_RecordDataGridView.Location = new System.Drawing.Point(88, 202);
            this.service_RecordDataGridView.Name = "service_RecordDataGridView";
            this.service_RecordDataGridView.Size = new System.Drawing.Size(541, 231);
            this.service_RecordDataGridView.TabIndex = 2;
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.DataPropertyName = "Army_No";
            this.dataGridViewTextBoxColumn1.HeaderText = "Army_No";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.DataPropertyName = "Date_Of_Disembodiment";
            this.dataGridViewTextBoxColumn2.HeaderText = "Date_Of_Disembodiment";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.DataPropertyName = "Date_Of_Embodiment";
            this.dataGridViewTextBoxColumn3.HeaderText = "Date_Of_Embodiment";
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            // 
            // dataGridViewTextBoxColumn10
            // 
            this.dataGridViewTextBoxColumn10.DataPropertyName = "Year_Diff";
            this.dataGridViewTextBoxColumn10.HeaderText = "Year_Diff";
            this.dataGridViewTextBoxColumn10.Name = "dataGridViewTextBoxColumn10";
            // 
            // dataGridViewTextBoxColumn11
            // 
            this.dataGridViewTextBoxColumn11.DataPropertyName = "Month_Diff";
            this.dataGridViewTextBoxColumn11.HeaderText = "Month_Diff";
            this.dataGridViewTextBoxColumn11.Name = "dataGridViewTextBoxColumn11";
            // 
            // dataGridViewTextBoxColumn12
            // 
            this.dataGridViewTextBoxColumn12.DataPropertyName = "Day_Diff";
            this.dataGridViewTextBoxColumn12.HeaderText = "Day_Diff";
            this.dataGridViewTextBoxColumn12.Name = "dataGridViewTextBoxColumn12";
            // 
            // dataGridViewTextBoxColumn13
            // 
            this.dataGridViewTextBoxColumn13.DataPropertyName = "Description";
            this.dataGridViewTextBoxColumn13.HeaderText = "Description";
            this.dataGridViewTextBoxColumn13.Name = "dataGridViewTextBoxColumn13";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Times New Roman", 15.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(246, 64);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(213, 24);
            this.label1.TabIndex = 3;
            this.label1.Text = "Service Record Search";
            // 
            // Ser_Search
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(684, 487);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.service_RecordDataGridView);
            this.Controls.Add(army_NoLabel);
            this.Controls.Add(this.army_NoComboBox);
            this.Name = "Ser_Search";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Service_Record_Search";
            this.Load += new System.EventHandler(this.Ser_Search_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataSet1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.service_RecordBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.service_RecordDataGridView)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DataSet1 dataSet1;
        private System.Windows.Forms.BindingSource service_RecordBindingSource;
        private Military_Project.DataSet1TableAdapters.Service_RecordTableAdapter service_RecordTableAdapter;
        private System.Windows.Forms.ComboBox army_NoComboBox;
       // private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
      //  private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn5;
      //  private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn6;
       // private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn7;
      //  private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn8;
      //  private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn9;
        private System.Windows.Forms.DataGridView service_RecordDataGridView;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn10;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn11;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn12;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn13;
        private System.Windows.Forms.Label label1;
    }
}