using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Military_Project
{
    public partial class Ser_Search : Form
    {
        public Ser_Search()
        {
            InitializeComponent();
        }
        DataSet1TableAdapters.Service_RecordTableAdapter SS = new Military_Project.DataSet1TableAdapters.Service_RecordTableAdapter();
        private void service_RecordBindingNavigatorSaveItem_Click(object sender, EventArgs e)
        {
            this.Validate();
            this.service_RecordBindingSource.EndEdit();
            this.service_RecordTableAdapter.Update(this.dataSet1.Service_Record);

        }

        private void Ser_Search_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'dataSet1.Service_Record' table. You can move, or remove it, as needed.
            this.service_RecordTableAdapter.Fill(this.dataSet1.Service_Record);
            army_NoComboBox.Text = "";
        }

        private void army_NoComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            DataTable dt = new DataTable();
            dt = SS.search_service(army_NoComboBox.Text);
            if (dt.Rows.Count > 0)
            {
                service_RecordDataGridView.DataSource = dt;
            }
        }
    }
}