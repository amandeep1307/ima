namespace Military_Project
{
    partial class Service_Record
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.Label date_Of_DisembodimentLabel;
            System.Windows.Forms.Label date_Of_EmbodimentLabel;
            System.Windows.Forms.Label descriptionLabel;
            System.Windows.Forms.Label army_NoLabel;
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Service_Record));
            this.service_RecordBindingNavigator = new System.Windows.Forms.BindingNavigator(this.components);
            this.bindingNavigatorAddNewItem = new System.Windows.Forms.ToolStripButton();
            this.service_RecordBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataSet1 = new Military_Project.DataSet1();
            this.bindingNavigatorCountItem = new System.Windows.Forms.ToolStripLabel();
            this.bindingNavigatorMoveFirstItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorMovePreviousItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorSeparator = new System.Windows.Forms.ToolStripSeparator();
            this.bindingNavigatorPositionItem = new System.Windows.Forms.ToolStripTextBox();
            this.bindingNavigatorSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.bindingNavigatorMoveNextItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorMoveLastItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.service_RecordBindingNavigatorSaveItem = new System.Windows.Forms.ToolStripButton();
            this.date_Of_DisembodimentDateTimePicker = new System.Windows.Forms.DateTimePicker();
            this.date_Of_EmbodimentDateTimePicker = new System.Windows.Forms.DateTimePicker();
            this.descriptionTextBox = new System.Windows.Forms.TextBox();
            this.service_RecordTableAdapter = new Military_Project.DataSet1TableAdapters.Service_RecordTableAdapter();
            this.army_NoComboBox = new System.Windows.Forms.ComboBox();
            this.soldierMasterBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.button2 = new System.Windows.Forms.Button();
            this.soldier_MasterTableAdapter = new Military_Project.DataSet1TableAdapters.Soldier_MasterTableAdapter();
            this.label1 = new System.Windows.Forms.Label();
            date_Of_DisembodimentLabel = new System.Windows.Forms.Label();
            date_Of_EmbodimentLabel = new System.Windows.Forms.Label();
            descriptionLabel = new System.Windows.Forms.Label();
            army_NoLabel = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.service_RecordBindingNavigator)).BeginInit();
            this.service_RecordBindingNavigator.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.service_RecordBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.soldierMasterBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // date_Of_DisembodimentLabel
            // 
            date_Of_DisembodimentLabel.AutoSize = true;
            date_Of_DisembodimentLabel.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            date_Of_DisembodimentLabel.Location = new System.Drawing.Point(143, 251);
            date_Of_DisembodimentLabel.Name = "date_Of_DisembodimentLabel";
            date_Of_DisembodimentLabel.Size = new System.Drawing.Size(155, 17);
            date_Of_DisembodimentLabel.TabIndex = 3;
            date_Of_DisembodimentLabel.Text = "Date Of Disembodiment:";
            // 
            // date_Of_EmbodimentLabel
            // 
            date_Of_EmbodimentLabel.AutoSize = true;
            date_Of_EmbodimentLabel.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            date_Of_EmbodimentLabel.Location = new System.Drawing.Point(143, 209);
            date_Of_EmbodimentLabel.Name = "date_Of_EmbodimentLabel";
            date_Of_EmbodimentLabel.Size = new System.Drawing.Size(137, 17);
            date_Of_EmbodimentLabel.TabIndex = 5;
            date_Of_EmbodimentLabel.Text = "Date Of Embodiment:";
            // 
            // descriptionLabel
            // 
            descriptionLabel.AutoSize = true;
            descriptionLabel.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            descriptionLabel.Location = new System.Drawing.Point(143, 294);
            descriptionLabel.Name = "descriptionLabel";
            descriptionLabel.Size = new System.Drawing.Size(78, 17);
            descriptionLabel.TabIndex = 19;
            descriptionLabel.Text = "Description:";
            // 
            // army_NoLabel
            // 
            army_NoLabel.AutoSize = true;
            army_NoLabel.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            army_NoLabel.Location = new System.Drawing.Point(143, 165);
            army_NoLabel.Name = "army_NoLabel";
            army_NoLabel.Size = new System.Drawing.Size(67, 17);
            army_NoLabel.TabIndex = 24;
            army_NoLabel.Text = "Army No:";
            // 
            // service_RecordBindingNavigator
            // 
            this.service_RecordBindingNavigator.AddNewItem = this.bindingNavigatorAddNewItem;
            this.service_RecordBindingNavigator.BindingSource = this.service_RecordBindingSource;
            this.service_RecordBindingNavigator.CountItem = this.bindingNavigatorCountItem;
            this.service_RecordBindingNavigator.DeleteItem = null;
            this.service_RecordBindingNavigator.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.bindingNavigatorMoveFirstItem,
            this.bindingNavigatorMovePreviousItem,
            this.bindingNavigatorSeparator,
            this.bindingNavigatorPositionItem,
            this.bindingNavigatorCountItem,
            this.bindingNavigatorSeparator1,
            this.bindingNavigatorMoveNextItem,
            this.bindingNavigatorMoveLastItem,
            this.bindingNavigatorSeparator2,
            this.bindingNavigatorAddNewItem,
            this.service_RecordBindingNavigatorSaveItem});
            this.service_RecordBindingNavigator.Location = new System.Drawing.Point(0, 0);
            this.service_RecordBindingNavigator.MoveFirstItem = this.bindingNavigatorMoveFirstItem;
            this.service_RecordBindingNavigator.MoveLastItem = this.bindingNavigatorMoveLastItem;
            this.service_RecordBindingNavigator.MoveNextItem = this.bindingNavigatorMoveNextItem;
            this.service_RecordBindingNavigator.MovePreviousItem = this.bindingNavigatorMovePreviousItem;
            this.service_RecordBindingNavigator.Name = "service_RecordBindingNavigator";
            this.service_RecordBindingNavigator.PositionItem = this.bindingNavigatorPositionItem;
            this.service_RecordBindingNavigator.Size = new System.Drawing.Size(684, 25);
            this.service_RecordBindingNavigator.TabIndex = 0;
            this.service_RecordBindingNavigator.Text = "bindingNavigator1";
            // 
            // bindingNavigatorAddNewItem
            // 
            this.bindingNavigatorAddNewItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorAddNewItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorAddNewItem.Image")));
            this.bindingNavigatorAddNewItem.Name = "bindingNavigatorAddNewItem";
            this.bindingNavigatorAddNewItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorAddNewItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorAddNewItem.Text = "Add new";
            // 
            // service_RecordBindingSource
            // 
            this.service_RecordBindingSource.DataMember = "Service_Record";
            this.service_RecordBindingSource.DataSource = this.dataSet1;
            // 
            // dataSet1
            // 
            this.dataSet1.DataSetName = "DataSet1";
            this.dataSet1.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // bindingNavigatorCountItem
            // 
            this.bindingNavigatorCountItem.Name = "bindingNavigatorCountItem";
            this.bindingNavigatorCountItem.Size = new System.Drawing.Size(35, 22);
            this.bindingNavigatorCountItem.Text = "of {0}";
            this.bindingNavigatorCountItem.ToolTipText = "Total number of items";
            // 
            // bindingNavigatorMoveFirstItem
            // 
            this.bindingNavigatorMoveFirstItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMoveFirstItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveFirstItem.Image")));
            this.bindingNavigatorMoveFirstItem.Name = "bindingNavigatorMoveFirstItem";
            this.bindingNavigatorMoveFirstItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMoveFirstItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorMoveFirstItem.Text = "Move first";
            // 
            // bindingNavigatorMovePreviousItem
            // 
            this.bindingNavigatorMovePreviousItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMovePreviousItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMovePreviousItem.Image")));
            this.bindingNavigatorMovePreviousItem.Name = "bindingNavigatorMovePreviousItem";
            this.bindingNavigatorMovePreviousItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMovePreviousItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorMovePreviousItem.Text = "Move previous";
            // 
            // bindingNavigatorSeparator
            // 
            this.bindingNavigatorSeparator.Name = "bindingNavigatorSeparator";
            this.bindingNavigatorSeparator.Size = new System.Drawing.Size(6, 25);
            // 
            // bindingNavigatorPositionItem
            // 
            this.bindingNavigatorPositionItem.AccessibleName = "Position";
            this.bindingNavigatorPositionItem.AutoSize = false;
            this.bindingNavigatorPositionItem.Name = "bindingNavigatorPositionItem";
            this.bindingNavigatorPositionItem.Size = new System.Drawing.Size(50, 23);
            this.bindingNavigatorPositionItem.Text = "0";
            this.bindingNavigatorPositionItem.ToolTipText = "Current position";
            // 
            // bindingNavigatorSeparator1
            // 
            this.bindingNavigatorSeparator1.Name = "bindingNavigatorSeparator1";
            this.bindingNavigatorSeparator1.Size = new System.Drawing.Size(6, 25);
            // 
            // bindingNavigatorMoveNextItem
            // 
            this.bindingNavigatorMoveNextItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMoveNextItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveNextItem.Image")));
            this.bindingNavigatorMoveNextItem.Name = "bindingNavigatorMoveNextItem";
            this.bindingNavigatorMoveNextItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMoveNextItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorMoveNextItem.Text = "Move next";
            // 
            // bindingNavigatorMoveLastItem
            // 
            this.bindingNavigatorMoveLastItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMoveLastItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveLastItem.Image")));
            this.bindingNavigatorMoveLastItem.Name = "bindingNavigatorMoveLastItem";
            this.bindingNavigatorMoveLastItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMoveLastItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorMoveLastItem.Text = "Move last";
            // 
            // bindingNavigatorSeparator2
            // 
            this.bindingNavigatorSeparator2.Name = "bindingNavigatorSeparator2";
            this.bindingNavigatorSeparator2.Size = new System.Drawing.Size(6, 25);
            // 
            // service_RecordBindingNavigatorSaveItem
            // 
            this.service_RecordBindingNavigatorSaveItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.service_RecordBindingNavigatorSaveItem.Image = ((System.Drawing.Image)(resources.GetObject("service_RecordBindingNavigatorSaveItem.Image")));
            this.service_RecordBindingNavigatorSaveItem.Name = "service_RecordBindingNavigatorSaveItem";
            this.service_RecordBindingNavigatorSaveItem.Size = new System.Drawing.Size(23, 22);
            this.service_RecordBindingNavigatorSaveItem.Text = "Save Data";
            this.service_RecordBindingNavigatorSaveItem.Click += new System.EventHandler(this.service_RecordBindingNavigatorSaveItem_Click);
            // 
            // date_Of_DisembodimentDateTimePicker
            // 
            this.date_Of_DisembodimentDateTimePicker.DataBindings.Add(new System.Windows.Forms.Binding("Value", this.service_RecordBindingSource, "Date_Of_Disembodiment", true));
            this.date_Of_DisembodimentDateTimePicker.Location = new System.Drawing.Point(305, 249);
            this.date_Of_DisembodimentDateTimePicker.Name = "date_Of_DisembodimentDateTimePicker";
            this.date_Of_DisembodimentDateTimePicker.Size = new System.Drawing.Size(200, 20);
            this.date_Of_DisembodimentDateTimePicker.TabIndex = 4;
            this.date_Of_DisembodimentDateTimePicker.ValueChanged += new System.EventHandler(this.date_Of_DisembodimentDateTimePicker_ValueChanged);
            // 
            // date_Of_EmbodimentDateTimePicker
            // 
            this.date_Of_EmbodimentDateTimePicker.DataBindings.Add(new System.Windows.Forms.Binding("Value", this.service_RecordBindingSource, "Date_Of_Embodiment", true));
            this.date_Of_EmbodimentDateTimePicker.Location = new System.Drawing.Point(305, 207);
            this.date_Of_EmbodimentDateTimePicker.Name = "date_Of_EmbodimentDateTimePicker";
            this.date_Of_EmbodimentDateTimePicker.Size = new System.Drawing.Size(200, 20);
            this.date_Of_EmbodimentDateTimePicker.TabIndex = 6;
            this.date_Of_EmbodimentDateTimePicker.ValueChanged += new System.EventHandler(this.date_Of_EmbodimentDateTimePicker_ValueChanged);
            // 
            // descriptionTextBox
            // 
            this.descriptionTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.service_RecordBindingSource, "Description", true));
            this.descriptionTextBox.Location = new System.Drawing.Point(305, 293);
            this.descriptionTextBox.Multiline = true;
            this.descriptionTextBox.Name = "descriptionTextBox";
            this.descriptionTextBox.Size = new System.Drawing.Size(200, 70);
            this.descriptionTextBox.TabIndex = 20;
            // 
            // service_RecordTableAdapter
            // 
            this.service_RecordTableAdapter.ClearBeforeFill = true;
            // 
            // army_NoComboBox
            // 
            this.army_NoComboBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.service_RecordBindingSource, "Army_No", true));
            this.army_NoComboBox.DataSource = this.soldierMasterBindingSource;
            this.army_NoComboBox.DisplayMember = "Army_No";
            this.army_NoComboBox.FormattingEnabled = true;
            this.army_NoComboBox.Location = new System.Drawing.Point(305, 164);
            this.army_NoComboBox.Name = "army_NoComboBox";
            this.army_NoComboBox.Size = new System.Drawing.Size(200, 21);
            this.army_NoComboBox.TabIndex = 25;
            this.army_NoComboBox.SelectedIndexChanged += new System.EventHandler(this.army_NoComboBox_SelectedIndexChanged);
            // 
            // soldierMasterBindingSource
            // 
            this.soldierMasterBindingSource.DataMember = "Soldier_Master";
            this.soldierMasterBindingSource.DataSource = this.dataSet1;
            // 
            // button2
            // 
            this.button2.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button2.Location = new System.Drawing.Point(560, 340);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(75, 23);
            this.button2.TabIndex = 26;
            this.button2.Text = "Search";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // soldier_MasterTableAdapter
            // 
            this.soldier_MasterTableAdapter.ClearBeforeFill = true;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Times New Roman", 15.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(267, 68);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(146, 24);
            this.label1.TabIndex = 27;
            this.label1.Text = "Service Record";
            this.label1.Click += new System.EventHandler(this.label1_Click);
            // 
            // Service_Record
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(684, 487);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.button2);
            this.Controls.Add(army_NoLabel);
            this.Controls.Add(this.army_NoComboBox);
            this.Controls.Add(date_Of_DisembodimentLabel);
            this.Controls.Add(this.date_Of_DisembodimentDateTimePicker);
            this.Controls.Add(date_Of_EmbodimentLabel);
            this.Controls.Add(this.date_Of_EmbodimentDateTimePicker);
            this.Controls.Add(descriptionLabel);
            this.Controls.Add(this.descriptionTextBox);
            this.Controls.Add(this.service_RecordBindingNavigator);
            this.Name = "Service_Record";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Service_Record";
            this.Load += new System.EventHandler(this.Service_Record_Load);
            ((System.ComponentModel.ISupportInitialize)(this.service_RecordBindingNavigator)).EndInit();
            this.service_RecordBindingNavigator.ResumeLayout(false);
            this.service_RecordBindingNavigator.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.service_RecordBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.soldierMasterBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DataSet1 dataSet1;
        private System.Windows.Forms.BindingSource service_RecordBindingSource;
        private Military_Project.DataSet1TableAdapters.Service_RecordTableAdapter service_RecordTableAdapter;
        private System.Windows.Forms.BindingNavigator service_RecordBindingNavigator;
        private System.Windows.Forms.ToolStripButton bindingNavigatorAddNewItem;
        private System.Windows.Forms.ToolStripLabel bindingNavigatorCountItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMoveFirstItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMovePreviousItem;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator;
        private System.Windows.Forms.ToolStripTextBox bindingNavigatorPositionItem;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator1;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMoveNextItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMoveLastItem;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator2;
        private System.Windows.Forms.ToolStripButton service_RecordBindingNavigatorSaveItem;
        private System.Windows.Forms.DateTimePicker date_Of_DisembodimentDateTimePicker;
        private System.Windows.Forms.DateTimePicker date_Of_EmbodimentDateTimePicker;
        private System.Windows.Forms.TextBox descriptionTextBox;
        private System.Windows.Forms.ComboBox army_NoComboBox;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.BindingSource soldierMasterBindingSource;
        private Military_Project.DataSet1TableAdapters.Soldier_MasterTableAdapter soldier_MasterTableAdapter;
        private System.Windows.Forms.Label label1;
    }
}