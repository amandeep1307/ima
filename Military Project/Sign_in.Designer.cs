namespace Military_Project
{
    partial class Sign_in
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.Label user_IdLabel;
            System.Windows.Forms.Label passwordLabel;
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Sign_in));
            this.dataSet1 = new Military_Project.DataSet1();
            this.user_MasterBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.user_MasterTableAdapter = new Military_Project.DataSet1TableAdapters.User_MasterTableAdapter();
            this.user_IdTextBox = new System.Windows.Forms.TextBox();
            this.passwordTextBox = new System.Windows.Forms.TextBox();
            this.button1 = new System.Windows.Forms.Button();
            this.progressBar1 = new System.Windows.Forms.ProgressBar();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.panel1 = new System.Windows.Forms.Panel();
            user_IdLabel = new System.Windows.Forms.Label();
            passwordLabel = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.user_MasterBindingSource)).BeginInit();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // user_IdLabel
            // 
            user_IdLabel.AutoSize = true;
            user_IdLabel.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            user_IdLabel.Location = new System.Drawing.Point(29, 10);
            user_IdLabel.Name = "user_IdLabel";
            user_IdLabel.Size = new System.Drawing.Size(44, 13);
            user_IdLabel.TabIndex = 20;
            user_IdLabel.Text = "User Id:";
            // 
            // passwordLabel
            // 
            passwordLabel.AutoSize = true;
            passwordLabel.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            passwordLabel.Location = new System.Drawing.Point(29, 55);
            passwordLabel.Name = "passwordLabel";
            passwordLabel.Size = new System.Drawing.Size(56, 13);
            passwordLabel.TabIndex = 21;
            passwordLabel.Text = "Password:";
            // 
            // dataSet1
            // 
            this.dataSet1.DataSetName = "DataSet1";
            this.dataSet1.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // user_MasterBindingSource
            // 
            this.user_MasterBindingSource.DataMember = "User_Master";
            this.user_MasterBindingSource.DataSource = this.dataSet1;
            // 
            // user_MasterTableAdapter
            // 
            this.user_MasterTableAdapter.ClearBeforeFill = true;
            // 
            // user_IdTextBox
            // 
            this.user_IdTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.user_MasterBindingSource, "User_Id", true));
            this.user_IdTextBox.Location = new System.Drawing.Point(32, 26);
            this.user_IdTextBox.Name = "user_IdTextBox";
            this.user_IdTextBox.Size = new System.Drawing.Size(229, 20);
            this.user_IdTextBox.TabIndex = 21;
            this.user_IdTextBox.TextChanged += new System.EventHandler(this.user_IdTextBox_TextChanged);
            this.user_IdTextBox.KeyDown += new System.Windows.Forms.KeyEventHandler(this.user_IdTextBox_KeyDown);
            // 
            // passwordTextBox
            // 
            this.passwordTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.user_MasterBindingSource, "Password", true));
            this.passwordTextBox.Location = new System.Drawing.Point(32, 71);
            this.passwordTextBox.Name = "passwordTextBox";
            this.passwordTextBox.PasswordChar = '*';
            this.passwordTextBox.Size = new System.Drawing.Size(228, 20);
            this.passwordTextBox.TabIndex = 22;
            this.passwordTextBox.TextChanged += new System.EventHandler(this.passwordTextBox_TextChanged);
            this.passwordTextBox.KeyDown += new System.Windows.Forms.KeyEventHandler(this.passwordTextBox_KeyDown);
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.DodgerBlue;
            this.button1.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.button1.Location = new System.Drawing.Point(32, 107);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(228, 23);
            this.button1.TabIndex = 23;
            this.button1.Text = "Login";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // progressBar1
            // 
            this.progressBar1.Location = new System.Drawing.Point(162, 458);
            this.progressBar1.Name = "progressBar1";
            this.progressBar1.Size = new System.Drawing.Size(376, 17);
            this.progressBar1.TabIndex = 24;
            this.progressBar1.Click += new System.EventHandler(this.progressBar1_Click);
            // 
            // timer1
            // 
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(user_IdLabel);
            this.panel1.Controls.Add(this.user_IdTextBox);
            this.panel1.Controls.Add(this.button1);
            this.panel1.Controls.Add(passwordLabel);
            this.panel1.Controls.Add(this.passwordTextBox);
            this.panel1.Location = new System.Drawing.Point(384, 23);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(288, 148);
            this.panel1.TabIndex = 25;
            this.panel1.Paint += new System.Windows.Forms.PaintEventHandler(this.panel1_Paint);
            // 
            // Sign_in
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.ClientSize = new System.Drawing.Size(684, 487);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.progressBar1);
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(700, 525);
            this.MinimumSize = new System.Drawing.Size(700, 525);
            this.Name = "Sign_in";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Login";
            this.Load += new System.EventHandler(this.Sign_in_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataSet1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.user_MasterBindingSource)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private DataSet1 dataSet1;
        private System.Windows.Forms.BindingSource user_MasterBindingSource;
        private Military_Project.DataSet1TableAdapters.User_MasterTableAdapter user_MasterTableAdapter;
        private System.Windows.Forms.TextBox user_IdTextBox;
        private System.Windows.Forms.TextBox passwordTextBox;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.ProgressBar progressBar1;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.Panel panel1;
    }
}