using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Military_Project
{
    public partial class Sign_in : Form
    {
        public Sign_in()
        {
            InitializeComponent();
        }
        DataSet1TableAdapters.User_MasterTableAdapter a1 = new Military_Project.DataSet1TableAdapters.User_MasterTableAdapter();
        private void user_MasterBindingNavigatorSaveItem_Click(object sender, EventArgs e)
        {
            this.Validate();
            this.user_MasterBindingSource.EndEdit();
            this.user_MasterTableAdapter.Update(this.dataSet1.User_Master);

        }

        private void Sign_in_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'dataSet1.User_Master' table. You can move, or remove it, as needed.
            this.user_MasterTableAdapter.Fill(this.dataSet1.User_Master);
            user_IdTextBox.Clear();
            passwordTextBox.Clear();

        }

        private void button1_Click(object sender, EventArgs e)
        {
            DataTable dt = new DataTable();
            dt = a1.sign_in_user(user_IdTextBox.Text, passwordTextBox.Text);
            if (dt.Rows.Count > 0)
            {
                this.progressBar1.Value = 0;
                this.timer1.Interval = 3;
                this.timer1.Enabled = true; 
                timer1_Tick(sender, e);
                user_IdTextBox.Clear();
                passwordTextBox.Clear();
               
            }
            else
            {
                MessageBox.Show(" Invalid Username or Password !");
            }

        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            if (this.progressBar1.Value < 100)
            {
                this.progressBar1.Value++;
                if (this.progressBar1.Value == 100)
                {
                    Navigation_Master sp = new Navigation_Master();
                    sp.ShowDialog();
                }

            }

        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {

        }

        private void progressBar1_Click(object sender, EventArgs e)
        {

        }

        private void user_IdTextBox_TextChanged(object sender, EventArgs e)
        {

        }

        private void user_IdTextBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyData == Keys.Enter)
            {
                passwordTextBox.Focus();
            }
        }

        private void passwordTextBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyData == Keys.Enter)
            {
                button1.Focus();
            }
        }

        private void passwordTextBox_TextChanged(object sender, EventArgs e)
        {

        }
    }
}