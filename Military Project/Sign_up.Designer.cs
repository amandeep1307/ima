namespace Military_Project
{
    partial class Sign_up
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.Label user_IdLabel;
            System.Windows.Forms.Label usernameLabel;
            System.Windows.Forms.Label passwordLabel;
            System.Windows.Forms.Label answerLabel;
            System.Windows.Forms.Label user_TypeLabel;
            System.Windows.Forms.Label security_QuestionLabel;
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Sign_up));
            this.dataSet1 = new Military_Project.DataSet1();
            this.user_MasterBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.user_MasterTableAdapter = new Military_Project.DataSet1TableAdapters.User_MasterTableAdapter();
            this.user_MasterBindingNavigator = new System.Windows.Forms.BindingNavigator(this.components);
            this.bindingNavigatorAddNewItem = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton1 = new System.Windows.Forms.ToolStripButton();
            this.user_MasterBindingNavigatorSaveItem = new System.Windows.Forms.ToolStripButton();
            this.user_IdTextBox = new System.Windows.Forms.TextBox();
            this.usernameTextBox = new System.Windows.Forms.TextBox();
            this.passwordTextBox = new System.Windows.Forms.TextBox();
            this.answerTextBox = new System.Windows.Forms.TextBox();
            this.user_TypeTextBox = new System.Windows.Forms.TextBox();
            this.button1 = new System.Windows.Forms.Button();
            this.security_QuestionComboBox = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            user_IdLabel = new System.Windows.Forms.Label();
            usernameLabel = new System.Windows.Forms.Label();
            passwordLabel = new System.Windows.Forms.Label();
            answerLabel = new System.Windows.Forms.Label();
            user_TypeLabel = new System.Windows.Forms.Label();
            security_QuestionLabel = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.user_MasterBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.user_MasterBindingNavigator)).BeginInit();
            this.user_MasterBindingNavigator.SuspendLayout();
            this.SuspendLayout();
            // 
            // user_IdLabel
            // 
            user_IdLabel.AutoSize = true;
            user_IdLabel.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            user_IdLabel.Location = new System.Drawing.Point(144, 141);
            user_IdLabel.Name = "user_IdLabel";
            user_IdLabel.Size = new System.Drawing.Size(56, 17);
            user_IdLabel.TabIndex = 1;
            user_IdLabel.Text = "User Id:";
            // 
            // usernameLabel
            // 
            usernameLabel.AutoSize = true;
            usernameLabel.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            usernameLabel.Location = new System.Drawing.Point(144, 182);
            usernameLabel.Name = "usernameLabel";
            usernameLabel.Size = new System.Drawing.Size(72, 17);
            usernameLabel.TabIndex = 3;
            usernameLabel.Text = "Username:";
            // 
            // passwordLabel
            // 
            passwordLabel.AutoSize = true;
            passwordLabel.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            passwordLabel.Location = new System.Drawing.Point(144, 224);
            passwordLabel.Name = "passwordLabel";
            passwordLabel.Size = new System.Drawing.Size(69, 17);
            passwordLabel.TabIndex = 5;
            passwordLabel.Text = "Password:";
            // 
            // answerLabel
            // 
            answerLabel.AutoSize = true;
            answerLabel.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            answerLabel.Location = new System.Drawing.Point(144, 308);
            answerLabel.Name = "answerLabel";
            answerLabel.Size = new System.Drawing.Size(58, 17);
            answerLabel.TabIndex = 9;
            answerLabel.Text = "Answer:";
            // 
            // user_TypeLabel
            // 
            user_TypeLabel.AutoSize = true;
            user_TypeLabel.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            user_TypeLabel.Location = new System.Drawing.Point(144, 350);
            user_TypeLabel.Name = "user_TypeLabel";
            user_TypeLabel.Size = new System.Drawing.Size(73, 17);
            user_TypeLabel.TabIndex = 11;
            user_TypeLabel.Text = "User Type:";
            // 
            // security_QuestionLabel
            // 
            security_QuestionLabel.AutoSize = true;
            security_QuestionLabel.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            security_QuestionLabel.Location = new System.Drawing.Point(144, 266);
            security_QuestionLabel.Name = "security_QuestionLabel";
            security_QuestionLabel.Size = new System.Drawing.Size(115, 17);
            security_QuestionLabel.TabIndex = 15;
            security_QuestionLabel.Text = "Security Question:";
            // 
            // dataSet1
            // 
            this.dataSet1.DataSetName = "DataSet1";
            this.dataSet1.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // user_MasterBindingSource
            // 
            this.user_MasterBindingSource.DataMember = "User_Master";
            this.user_MasterBindingSource.DataSource = this.dataSet1;
            // 
            // user_MasterTableAdapter
            // 
            this.user_MasterTableAdapter.ClearBeforeFill = true;
            // 
            // user_MasterBindingNavigator
            // 
            this.user_MasterBindingNavigator.AddNewItem = this.bindingNavigatorAddNewItem;
            this.user_MasterBindingNavigator.BindingSource = this.user_MasterBindingSource;
            this.user_MasterBindingNavigator.CountItem = null;
            this.user_MasterBindingNavigator.DeleteItem = null;
            this.user_MasterBindingNavigator.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.bindingNavigatorAddNewItem,
            this.toolStripButton1,
            this.user_MasterBindingNavigatorSaveItem});
            this.user_MasterBindingNavigator.Location = new System.Drawing.Point(0, 0);
            this.user_MasterBindingNavigator.MoveFirstItem = null;
            this.user_MasterBindingNavigator.MoveLastItem = null;
            this.user_MasterBindingNavigator.MoveNextItem = null;
            this.user_MasterBindingNavigator.MovePreviousItem = null;
            this.user_MasterBindingNavigator.Name = "user_MasterBindingNavigator";
            this.user_MasterBindingNavigator.PositionItem = null;
            this.user_MasterBindingNavigator.Size = new System.Drawing.Size(684, 25);
            this.user_MasterBindingNavigator.TabIndex = 0;
            this.user_MasterBindingNavigator.Text = "bindingNavigator1";
            // 
            // bindingNavigatorAddNewItem
            // 
            this.bindingNavigatorAddNewItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorAddNewItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorAddNewItem.Image")));
            this.bindingNavigatorAddNewItem.Name = "bindingNavigatorAddNewItem";
            this.bindingNavigatorAddNewItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorAddNewItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorAddNewItem.Text = "Add new data";
            // 
            // toolStripButton1
            // 
            this.toolStripButton1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton1.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton1.Image")));
            this.toolStripButton1.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton1.Name = "toolStripButton1";
            this.toolStripButton1.Size = new System.Drawing.Size(23, 22);
            this.toolStripButton1.Text = "Update";
            this.toolStripButton1.Click += new System.EventHandler(this.toolStripButton1_Click);
            // 
            // user_MasterBindingNavigatorSaveItem
            // 
            this.user_MasterBindingNavigatorSaveItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.user_MasterBindingNavigatorSaveItem.Image = ((System.Drawing.Image)(resources.GetObject("user_MasterBindingNavigatorSaveItem.Image")));
            this.user_MasterBindingNavigatorSaveItem.Name = "user_MasterBindingNavigatorSaveItem";
            this.user_MasterBindingNavigatorSaveItem.Size = new System.Drawing.Size(23, 22);
            this.user_MasterBindingNavigatorSaveItem.Text = "Save";
            this.user_MasterBindingNavigatorSaveItem.Click += new System.EventHandler(this.user_MasterBindingNavigatorSaveItem_Click);
            // 
            // user_IdTextBox
            // 
            this.user_IdTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.user_MasterBindingSource, "User_Id", true));
            this.user_IdTextBox.Location = new System.Drawing.Point(275, 138);
            this.user_IdTextBox.Name = "user_IdTextBox";
            this.user_IdTextBox.Size = new System.Drawing.Size(214, 20);
            this.user_IdTextBox.TabIndex = 2;
            // 
            // usernameTextBox
            // 
            this.usernameTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.user_MasterBindingSource, "Username", true));
            this.usernameTextBox.Location = new System.Drawing.Point(275, 179);
            this.usernameTextBox.Name = "usernameTextBox";
            this.usernameTextBox.Size = new System.Drawing.Size(214, 20);
            this.usernameTextBox.TabIndex = 4;
            // 
            // passwordTextBox
            // 
            this.passwordTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.user_MasterBindingSource, "Password", true));
            this.passwordTextBox.Location = new System.Drawing.Point(275, 221);
            this.passwordTextBox.Name = "passwordTextBox";
            this.passwordTextBox.PasswordChar = '*';
            this.passwordTextBox.Size = new System.Drawing.Size(214, 20);
            this.passwordTextBox.TabIndex = 6;
            this.passwordTextBox.TextChanged += new System.EventHandler(this.passwordTextBox_TextChanged);
            // 
            // answerTextBox
            // 
            this.answerTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.user_MasterBindingSource, "Answer", true));
            this.answerTextBox.Location = new System.Drawing.Point(275, 305);
            this.answerTextBox.Name = "answerTextBox";
            this.answerTextBox.Size = new System.Drawing.Size(214, 20);
            this.answerTextBox.TabIndex = 10;
            // 
            // user_TypeTextBox
            // 
            this.user_TypeTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.user_MasterBindingSource, "User_Type", true));
            this.user_TypeTextBox.Location = new System.Drawing.Point(275, 347);
            this.user_TypeTextBox.Name = "user_TypeTextBox";
            this.user_TypeTextBox.Size = new System.Drawing.Size(214, 20);
            this.user_TypeTextBox.TabIndex = 12;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(495, 135);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(110, 23);
            this.button1.TabIndex = 15;
            this.button1.Text = "Check Availability?";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // security_QuestionComboBox
            // 
            this.security_QuestionComboBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.user_MasterBindingSource, "Security_Question", true));
            this.security_QuestionComboBox.FormattingEnabled = true;
            this.security_QuestionComboBox.Items.AddRange(new object[] {
            "What is your birth place ?",
            "What is your best friend\'s name ?",
            "What is the name of your high-school ?"});
            this.security_QuestionComboBox.Location = new System.Drawing.Point(275, 263);
            this.security_QuestionComboBox.Name = "security_QuestionComboBox";
            this.security_QuestionComboBox.Size = new System.Drawing.Size(214, 21);
            this.security_QuestionComboBox.TabIndex = 16;
            this.security_QuestionComboBox.SelectedIndexChanged += new System.EventHandler(this.security_QuestionComboBox_SelectedIndexChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Times New Roman", 15.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(271, 63);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(162, 24);
            this.label1.TabIndex = 17;
            this.label1.Text = "Create New User";
            // 
            // Sign_up
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(684, 487);
            this.Controls.Add(this.label1);
            this.Controls.Add(security_QuestionLabel);
            this.Controls.Add(this.security_QuestionComboBox);
            this.Controls.Add(this.button1);
            this.Controls.Add(user_IdLabel);
            this.Controls.Add(this.user_IdTextBox);
            this.Controls.Add(usernameLabel);
            this.Controls.Add(this.usernameTextBox);
            this.Controls.Add(passwordLabel);
            this.Controls.Add(this.passwordTextBox);
            this.Controls.Add(answerLabel);
            this.Controls.Add(this.answerTextBox);
            this.Controls.Add(user_TypeLabel);
            this.Controls.Add(this.user_TypeTextBox);
            this.Controls.Add(this.user_MasterBindingNavigator);
            this.Name = "Sign_up";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Create_New_User";
            this.Load += new System.EventHandler(this.Sign_up_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataSet1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.user_MasterBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.user_MasterBindingNavigator)).EndInit();
            this.user_MasterBindingNavigator.ResumeLayout(false);
            this.user_MasterBindingNavigator.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DataSet1 dataSet1;
        private System.Windows.Forms.BindingSource user_MasterBindingSource;
        private Military_Project.DataSet1TableAdapters.User_MasterTableAdapter user_MasterTableAdapter;
        private System.Windows.Forms.BindingNavigator user_MasterBindingNavigator;
        private System.Windows.Forms.ToolStripButton bindingNavigatorAddNewItem;
        private System.Windows.Forms.ToolStripButton user_MasterBindingNavigatorSaveItem;
        private System.Windows.Forms.TextBox user_IdTextBox;
        private System.Windows.Forms.TextBox usernameTextBox;
        private System.Windows.Forms.TextBox passwordTextBox;
        private System.Windows.Forms.TextBox answerTextBox;
        private System.Windows.Forms.TextBox user_TypeTextBox;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.ToolStripButton toolStripButton1;
        private System.Windows.Forms.ComboBox security_QuestionComboBox;
        private System.Windows.Forms.Label label1;
    }
}