using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
//using System.Text.RegularExpressions;

namespace Military_Project
{
    public partial class Sign_up : Form
    {
        public Sign_up()
        {
            InitializeComponent();
        }
        DataSet1TableAdapters.User_MasterTableAdapter a1 = new Military_Project.DataSet1TableAdapters.User_MasterTableAdapter();
        private void clearfn()
        {
            user_IdTextBox.Clear();
            user_TypeTextBox.Clear();
            usernameTextBox.Clear();
            passwordTextBox.Clear();
            security_QuestionComboBox.Text = "";
            answerTextBox.Clear();
        }
        private void user_MasterBindingNavigatorSaveItem_Click(object sender, EventArgs e)
        {
            if (user_IdTextBox.Text.Length != 0 && usernameTextBox.Text.Length != 0 && passwordTextBox.Text.Length != 0 && security_QuestionComboBox.Text.Length != 0 && answerTextBox.Text.Length != 0 && user_TypeTextBox.Text.Length != 0)
            {
                a1.insert_user(user_IdTextBox.Text, usernameTextBox.Text, passwordTextBox.Text, security_QuestionComboBox.Text, answerTextBox.Text, user_TypeTextBox.Text);
                MessageBox.Show("Record Inserted Successfully !");
            }
            else
            {
                MessageBox.Show("Please enter all values", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }
        private void Sign_up_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'dataSet1.User_Master' table. You can move, or remove it, as needed.
            this.user_MasterTableAdapter.Fill(this.dataSet1.User_Master);
            clearfn();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            DataTable dt = new DataTable();
            dt = a1.search_user(user_IdTextBox.Text);
            if (dt.Rows.Count > 0)
            {
                MessageBox.Show("Already Exists !");
                user_IdTextBox.Text = "";
                user_IdTextBox.Focus();
            }
            else
            {
                MessageBox.Show("User_Id Available !");
            }
        }

        private void toolStripButton1_Click(object sender, EventArgs e)
        {
            a1.update_user(user_IdTextBox.Text, usernameTextBox.Text, passwordTextBox.Text, security_QuestionComboBox.Text, answerTextBox.Text, user_TypeTextBox.Text);
            MessageBox.Show("Record Updated Successfully !");
        }

        private void security_QuestionComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
       
        private void passwordTextBox_TextChanged(object sender, EventArgs e)
        {
            
        }
    }
}