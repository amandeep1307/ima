namespace Military_Project
{
    partial class Soldier_Master
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.Label army_NoLabel;
            System.Windows.Forms.Label nOKLabel;
            System.Windows.Forms.Label rankLabel;
            System.Windows.Forms.Label dOJLabel;
            System.Windows.Forms.Label childrenLabel;
            System.Windows.Forms.Label sonLabel;
            System.Windows.Forms.Label daughterLabel;
            System.Windows.Forms.Label bank_Acc_NoLabel;
            System.Windows.Forms.Label pANLabel;
            System.Windows.Forms.Label billLabel;
            System.Windows.Forms.Label joint_Acc_NoLabel;
            System.Windows.Forms.Label civil_OccupationLabel;
            System.Windows.Forms.Label addressLabel;
            System.Windows.Forms.Label enameLabel;
            System.Windows.Forms.Label dOBLabel;
            System.Windows.Forms.Label grade_3Label;
            System.Windows.Forms.Label grade_2Label;
            System.Windows.Forms.Label grade_1Label;
            System.Windows.Forms.Label qualificationLabel;
            System.Windows.Forms.Label course_3Label;
            System.Windows.Forms.Label army_QualiLabel;
            System.Windows.Forms.Label course_2Label;
            System.Windows.Forms.Label red_InkLabel;
            System.Windows.Forms.Label black_InkLabel;
            System.Windows.Forms.Label descriptionLabel;
            System.Windows.Forms.Label battalion_IdLabel;
            System.Windows.Forms.Label company_IdLabel;
            System.Windows.Forms.Label platoon_IdLabel;
            System.Windows.Forms.Label section_NameLabel;
            System.Windows.Forms.Label level_Of_IncomeLabel;
            System.Windows.Forms.Label sport_1Label;
            System.Windows.Forms.Label sport_2Label;
            System.Windows.Forms.Label sport_3Label;
            System.Windows.Forms.Label level_1Label;
            System.Windows.Forms.Label level_2Label;
            System.Windows.Forms.Label level_3Label;
            System.Windows.Forms.Label medal_1Label;
            System.Windows.Forms.Label medal_2Label;
            System.Windows.Forms.Label medal_3Label;
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Soldier_Master));
            this.label1 = new System.Windows.Forms.Label();
            this.soldier_MasterBindingNavigator = new System.Windows.Forms.BindingNavigator(this.components);
            this.bindingNavigatorAddNewItem = new System.Windows.Forms.ToolStripButton();
            this.soldier_MasterBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataSet1 = new Military_Project.DataSet1();
            this.bindingNavigatorCountItem = new System.Windows.Forms.ToolStripLabel();
            this.bindingNavigatorMoveFirstItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorMovePreviousItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorSeparator = new System.Windows.Forms.ToolStripSeparator();
            this.bindingNavigatorPositionItem = new System.Windows.Forms.ToolStripTextBox();
            this.bindingNavigatorSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.bindingNavigatorMoveNextItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorMoveLastItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripButton1 = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton2 = new System.Windows.Forms.ToolStripButton();
            this.soldier_MasterBindingNavigatorSaveItem = new System.Windows.Forms.ToolStripButton();
            this.label2 = new System.Windows.Forms.Label();
            this.button3 = new System.Windows.Forms.Button();
            this.soldier_MasterTableAdapter = new Military_Project.DataSet1TableAdapters.Soldier_MasterTableAdapter();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.section_NameComboBox = new System.Windows.Forms.ComboBox();
            this.sectionMasterBindingSource1 = new System.Windows.Forms.BindingSource(this.components);
            this.platoon_IdComboBox = new System.Windows.Forms.ComboBox();
            this.platoonMasterBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.company_IdComboBox = new System.Windows.Forms.ComboBox();
            this.companyMasterBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.battalion_IdComboBox = new System.Windows.Forms.ComboBox();
            this.battalionMasterBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.button5 = new System.Windows.Forms.Button();
            this.army_NoTextBox = new System.Windows.Forms.TextBox();
            this.rankTextBox = new System.Windows.Forms.TextBox();
            this.nOKTextBox = new System.Windows.Forms.TextBox();
            this.dOJDateTimePicker = new System.Windows.Forms.DateTimePicker();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.level_Of_IncomeComboBox = new System.Windows.Forms.ComboBox();
            this.childrenTextBox = new System.Windows.Forms.TextBox();
            this.sonTextBox = new System.Windows.Forms.TextBox();
            this.daughterTextBox = new System.Windows.Forms.TextBox();
            this.bank_Acc_NoTextBox = new System.Windows.Forms.TextBox();
            this.pANTextBox = new System.Windows.Forms.TextBox();
            this.billTextBox = new System.Windows.Forms.TextBox();
            this.joint_Acc_NoTextBox = new System.Windows.Forms.TextBox();
            this.addressTextBox = new System.Windows.Forms.TextBox();
            this.civil_OccupationTextBox = new System.Windows.Forms.TextBox();
            this.dOBDateTimePicker = new System.Windows.Forms.DateTimePicker();
            this.enameTextBox = new System.Windows.Forms.TextBox();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.grade_1TextBox = new System.Windows.Forms.TextBox();
            this.grade_3TextBox = new System.Windows.Forms.TextBox();
            this.grade_2TextBox = new System.Windows.Forms.TextBox();
            this.course_3TextBox = new System.Windows.Forms.TextBox();
            this.qualificationTextBox = new System.Windows.Forms.TextBox();
            this.course_2TextBox = new System.Windows.Forms.TextBox();
            this.army_QualiTextBox = new System.Windows.Forms.TextBox();
            this.tabPage4 = new System.Windows.Forms.TabPage();
            this.medal_3ComboBox = new System.Windows.Forms.ComboBox();
            this.medal_2ComboBox = new System.Windows.Forms.ComboBox();
            this.medal_1ComboBox = new System.Windows.Forms.ComboBox();
            this.level_3ComboBox = new System.Windows.Forms.ComboBox();
            this.level_2ComboBox = new System.Windows.Forms.ComboBox();
            this.level_1ComboBox = new System.Windows.Forms.ComboBox();
            this.sport_3ComboBox = new System.Windows.Forms.ComboBox();
            this.sport_2ComboBox = new System.Windows.Forms.ComboBox();
            this.sport_1ComboBox = new System.Windows.Forms.ComboBox();
            this.tabPage5 = new System.Windows.Forms.TabPage();
            this.red_InkTextBox = new System.Windows.Forms.TextBox();
            this.black_InkTextBox = new System.Windows.Forms.TextBox();
            this.descriptionTextBox = new System.Windows.Forms.TextBox();
            this.sectionMasterBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.section_MasterTableAdapter = new Military_Project.DataSet1TableAdapters.Section_MasterTableAdapter();
            this.battalion_MasterTableAdapter = new Military_Project.DataSet1TableAdapters.Battalion_MasterTableAdapter();
            this.company_MasterTableAdapter = new Military_Project.DataSet1TableAdapters.Company_MasterTableAdapter();
            this.platoon_MasterTableAdapter = new Military_Project.DataSet1TableAdapters.Platoon_MasterTableAdapter();
            army_NoLabel = new System.Windows.Forms.Label();
            nOKLabel = new System.Windows.Forms.Label();
            rankLabel = new System.Windows.Forms.Label();
            dOJLabel = new System.Windows.Forms.Label();
            childrenLabel = new System.Windows.Forms.Label();
            sonLabel = new System.Windows.Forms.Label();
            daughterLabel = new System.Windows.Forms.Label();
            bank_Acc_NoLabel = new System.Windows.Forms.Label();
            pANLabel = new System.Windows.Forms.Label();
            billLabel = new System.Windows.Forms.Label();
            joint_Acc_NoLabel = new System.Windows.Forms.Label();
            civil_OccupationLabel = new System.Windows.Forms.Label();
            addressLabel = new System.Windows.Forms.Label();
            enameLabel = new System.Windows.Forms.Label();
            dOBLabel = new System.Windows.Forms.Label();
            grade_3Label = new System.Windows.Forms.Label();
            grade_2Label = new System.Windows.Forms.Label();
            grade_1Label = new System.Windows.Forms.Label();
            qualificationLabel = new System.Windows.Forms.Label();
            course_3Label = new System.Windows.Forms.Label();
            army_QualiLabel = new System.Windows.Forms.Label();
            course_2Label = new System.Windows.Forms.Label();
            red_InkLabel = new System.Windows.Forms.Label();
            black_InkLabel = new System.Windows.Forms.Label();
            descriptionLabel = new System.Windows.Forms.Label();
            battalion_IdLabel = new System.Windows.Forms.Label();
            company_IdLabel = new System.Windows.Forms.Label();
            platoon_IdLabel = new System.Windows.Forms.Label();
            section_NameLabel = new System.Windows.Forms.Label();
            level_Of_IncomeLabel = new System.Windows.Forms.Label();
            sport_1Label = new System.Windows.Forms.Label();
            sport_2Label = new System.Windows.Forms.Label();
            sport_3Label = new System.Windows.Forms.Label();
            level_1Label = new System.Windows.Forms.Label();
            level_2Label = new System.Windows.Forms.Label();
            level_3Label = new System.Windows.Forms.Label();
            medal_1Label = new System.Windows.Forms.Label();
            medal_2Label = new System.Windows.Forms.Label();
            medal_3Label = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.soldier_MasterBindingNavigator)).BeginInit();
            this.soldier_MasterBindingNavigator.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.soldier_MasterBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet1)).BeginInit();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.sectionMasterBindingSource1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.platoonMasterBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.companyMasterBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.battalionMasterBindingSource)).BeginInit();
            this.tabPage2.SuspendLayout();
            this.tabPage3.SuspendLayout();
            this.tabPage4.SuspendLayout();
            this.tabPage5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.sectionMasterBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // army_NoLabel
            // 
            army_NoLabel.AutoSize = true;
            army_NoLabel.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            army_NoLabel.Location = new System.Drawing.Point(35, 174);
            army_NoLabel.Name = "army_NoLabel";
            army_NoLabel.Size = new System.Drawing.Size(67, 17);
            army_NoLabel.TabIndex = 10;
            army_NoLabel.Text = "Army No:";
            // 
            // nOKLabel
            // 
            nOKLabel.AutoSize = true;
            nOKLabel.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            nOKLabel.Location = new System.Drawing.Point(35, 277);
            nOKLabel.Name = "nOKLabel";
            nOKLabel.Size = new System.Drawing.Size(44, 17);
            nOKLabel.TabIndex = 40;
            nOKLabel.Text = "NOK:";
            // 
            // rankLabel
            // 
            rankLabel.AutoSize = true;
            rankLabel.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            rankLabel.Location = new System.Drawing.Point(35, 242);
            rankLabel.Name = "rankLabel";
            rankLabel.Size = new System.Drawing.Size(42, 17);
            rankLabel.TabIndex = 38;
            rankLabel.Text = "Rank:";
            // 
            // dOJLabel
            // 
            dOJLabel.AutoSize = true;
            dOJLabel.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dOJLabel.Location = new System.Drawing.Point(35, 209);
            dOJLabel.Name = "dOJLabel";
            dOJLabel.Size = new System.Drawing.Size(39, 17);
            dOJLabel.TabIndex = 36;
            dOJLabel.Text = "DOJ:";
            // 
            // childrenLabel
            // 
            childrenLabel.AutoSize = true;
            childrenLabel.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            childrenLabel.Location = new System.Drawing.Point(40, 167);
            childrenLabel.Name = "childrenLabel";
            childrenLabel.Size = new System.Drawing.Size(60, 17);
            childrenLabel.TabIndex = 74;
            childrenLabel.Text = "Children:";
            // 
            // sonLabel
            // 
            sonLabel.AutoSize = true;
            sonLabel.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            sonLabel.Location = new System.Drawing.Point(40, 193);
            sonLabel.Name = "sonLabel";
            sonLabel.Size = new System.Drawing.Size(33, 17);
            sonLabel.TabIndex = 76;
            sonLabel.Text = "Son:";
            // 
            // daughterLabel
            // 
            daughterLabel.AutoSize = true;
            daughterLabel.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            daughterLabel.Location = new System.Drawing.Point(40, 219);
            daughterLabel.Name = "daughterLabel";
            daughterLabel.Size = new System.Drawing.Size(66, 17);
            daughterLabel.TabIndex = 78;
            daughterLabel.Text = "Daughter:";
            // 
            // bank_Acc_NoLabel
            // 
            bank_Acc_NoLabel.AutoSize = true;
            bank_Acc_NoLabel.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            bank_Acc_NoLabel.Location = new System.Drawing.Point(40, 245);
            bank_Acc_NoLabel.Name = "bank_Acc_NoLabel";
            bank_Acc_NoLabel.Size = new System.Drawing.Size(92, 17);
            bank_Acc_NoLabel.TabIndex = 80;
            bank_Acc_NoLabel.Text = "Bank Acc No:";
            // 
            // pANLabel
            // 
            pANLabel.AutoSize = true;
            pANLabel.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            pANLabel.Location = new System.Drawing.Point(40, 271);
            pANLabel.Name = "pANLabel";
            pANLabel.Size = new System.Drawing.Size(41, 17);
            pANLabel.TabIndex = 82;
            pANLabel.Text = "PAN:";
            // 
            // billLabel
            // 
            billLabel.AutoSize = true;
            billLabel.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            billLabel.Location = new System.Drawing.Point(40, 297);
            billLabel.Name = "billLabel";
            billLabel.Size = new System.Drawing.Size(30, 17);
            billLabel.TabIndex = 84;
            billLabel.Text = "Bill:";
            // 
            // joint_Acc_NoLabel
            // 
            joint_Acc_NoLabel.AutoSize = true;
            joint_Acc_NoLabel.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            joint_Acc_NoLabel.Location = new System.Drawing.Point(40, 323);
            joint_Acc_NoLabel.Name = "joint_Acc_NoLabel";
            joint_Acc_NoLabel.Size = new System.Drawing.Size(88, 17);
            joint_Acc_NoLabel.TabIndex = 86;
            joint_Acc_NoLabel.Text = "Joint Acc No:";
            // 
            // civil_OccupationLabel
            // 
            civil_OccupationLabel.AutoSize = true;
            civil_OccupationLabel.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            civil_OccupationLabel.Location = new System.Drawing.Point(40, 106);
            civil_OccupationLabel.Name = "civil_OccupationLabel";
            civil_OccupationLabel.Size = new System.Drawing.Size(108, 17);
            civil_OccupationLabel.TabIndex = 20;
            civil_OccupationLabel.Text = "Civil Occupation:";
            // 
            // addressLabel
            // 
            addressLabel.AutoSize = true;
            addressLabel.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            addressLabel.Location = new System.Drawing.Point(40, 80);
            addressLabel.Name = "addressLabel";
            addressLabel.Size = new System.Drawing.Size(60, 17);
            addressLabel.TabIndex = 16;
            addressLabel.Text = "Address:";
            // 
            // enameLabel
            // 
            enameLabel.AutoSize = true;
            enameLabel.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            enameLabel.Location = new System.Drawing.Point(40, 28);
            enameLabel.Name = "enameLabel";
            enameLabel.Size = new System.Drawing.Size(52, 17);
            enameLabel.TabIndex = 12;
            enameLabel.Text = "Ename:";
            // 
            // dOBLabel
            // 
            dOBLabel.AutoSize = true;
            dOBLabel.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dOBLabel.Location = new System.Drawing.Point(40, 55);
            dOBLabel.Name = "dOBLabel";
            dOBLabel.Size = new System.Drawing.Size(43, 17);
            dOBLabel.TabIndex = 14;
            dOBLabel.Text = "DOB:";
            // 
            // grade_3Label
            // 
            grade_3Label.AutoSize = true;
            grade_3Label.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            grade_3Label.Location = new System.Drawing.Point(85, 279);
            grade_3Label.Name = "grade_3Label";
            grade_3Label.Size = new System.Drawing.Size(58, 17);
            grade_3Label.TabIndex = 34;
            grade_3Label.Text = "Grade 3:";
            // 
            // grade_2Label
            // 
            grade_2Label.AutoSize = true;
            grade_2Label.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            grade_2Label.Location = new System.Drawing.Point(85, 238);
            grade_2Label.Name = "grade_2Label";
            grade_2Label.Size = new System.Drawing.Size(58, 17);
            grade_2Label.TabIndex = 32;
            grade_2Label.Text = "Grade 2:";
            // 
            // grade_1Label
            // 
            grade_1Label.AutoSize = true;
            grade_1Label.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            grade_1Label.Location = new System.Drawing.Point(85, 198);
            grade_1Label.Name = "grade_1Label";
            grade_1Label.Size = new System.Drawing.Size(58, 17);
            grade_1Label.TabIndex = 30;
            grade_1Label.Text = "Grade 1:";
            // 
            // qualificationLabel
            // 
            qualificationLabel.AutoSize = true;
            qualificationLabel.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            qualificationLabel.Location = new System.Drawing.Point(58, 43);
            qualificationLabel.Name = "qualificationLabel";
            qualificationLabel.Size = new System.Drawing.Size(85, 17);
            qualificationLabel.TabIndex = 18;
            qualificationLabel.Text = "Qualification:";
            // 
            // course_3Label
            // 
            course_3Label.AutoSize = true;
            course_3Label.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            course_3Label.Location = new System.Drawing.Point(79, 159);
            course_3Label.Name = "course_3Label";
            course_3Label.Size = new System.Drawing.Size(64, 17);
            course_3Label.TabIndex = 28;
            course_3Label.Text = "Course 3:";
            // 
            // army_QualiLabel
            // 
            army_QualiLabel.AutoSize = true;
            army_QualiLabel.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            army_QualiLabel.Location = new System.Drawing.Point(20, 80);
            army_QualiLabel.Name = "army_QualiLabel";
            army_QualiLabel.Size = new System.Drawing.Size(123, 17);
            army_QualiLabel.TabIndex = 24;
            army_QualiLabel.Text = "Army Qualification:";
            // 
            // course_2Label
            // 
            course_2Label.AutoSize = true;
            course_2Label.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            course_2Label.Location = new System.Drawing.Point(79, 120);
            course_2Label.Name = "course_2Label";
            course_2Label.Size = new System.Drawing.Size(64, 17);
            course_2Label.TabIndex = 26;
            course_2Label.Text = "Course 2:";
            // 
            // red_InkLabel
            // 
            red_InkLabel.AutoSize = true;
            red_InkLabel.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            red_InkLabel.Location = new System.Drawing.Point(47, 43);
            red_InkLabel.Name = "red_InkLabel";
            red_InkLabel.Size = new System.Drawing.Size(58, 17);
            red_InkLabel.TabIndex = 80;
            red_InkLabel.Text = "Red Ink:";
            // 
            // black_InkLabel
            // 
            black_InkLabel.AutoSize = true;
            black_InkLabel.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            black_InkLabel.Location = new System.Drawing.Point(37, 86);
            black_InkLabel.Name = "black_InkLabel";
            black_InkLabel.Size = new System.Drawing.Size(68, 17);
            black_InkLabel.TabIndex = 82;
            black_InkLabel.Text = "Black Ink:";
            // 
            // descriptionLabel
            // 
            descriptionLabel.AutoSize = true;
            descriptionLabel.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            descriptionLabel.Location = new System.Drawing.Point(27, 125);
            descriptionLabel.Name = "descriptionLabel";
            descriptionLabel.Size = new System.Drawing.Size(78, 17);
            descriptionLabel.TabIndex = 84;
            descriptionLabel.Text = "Description:";
            // 
            // battalion_IdLabel
            // 
            battalion_IdLabel.AutoSize = true;
            battalion_IdLabel.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            battalion_IdLabel.Location = new System.Drawing.Point(36, 33);
            battalion_IdLabel.Name = "battalion_IdLabel";
            battalion_IdLabel.Size = new System.Drawing.Size(79, 17);
            battalion_IdLabel.TabIndex = 42;
            battalion_IdLabel.Text = "Battalion Id:";
            // 
            // company_IdLabel
            // 
            company_IdLabel.AutoSize = true;
            company_IdLabel.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            company_IdLabel.Location = new System.Drawing.Point(36, 67);
            company_IdLabel.Name = "company_IdLabel";
            company_IdLabel.Size = new System.Drawing.Size(83, 17);
            company_IdLabel.TabIndex = 43;
            company_IdLabel.Text = "Company Id:";
            // 
            // platoon_IdLabel
            // 
            platoon_IdLabel.AutoSize = true;
            platoon_IdLabel.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            platoon_IdLabel.Location = new System.Drawing.Point(36, 102);
            platoon_IdLabel.Name = "platoon_IdLabel";
            platoon_IdLabel.Size = new System.Drawing.Size(71, 17);
            platoon_IdLabel.TabIndex = 44;
            platoon_IdLabel.Text = "Platoon Id:";
            // 
            // section_NameLabel
            // 
            section_NameLabel.AutoSize = true;
            section_NameLabel.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            section_NameLabel.Location = new System.Drawing.Point(36, 138);
            section_NameLabel.Name = "section_NameLabel";
            section_NameLabel.Size = new System.Drawing.Size(94, 17);
            section_NameLabel.TabIndex = 45;
            section_NameLabel.Text = "Section Name:";
            // 
            // level_Of_IncomeLabel
            // 
            level_Of_IncomeLabel.AutoSize = true;
            level_Of_IncomeLabel.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            level_Of_IncomeLabel.Location = new System.Drawing.Point(40, 140);
            level_Of_IncomeLabel.Name = "level_Of_IncomeLabel";
            level_Of_IncomeLabel.Size = new System.Drawing.Size(112, 17);
            level_Of_IncomeLabel.TabIndex = 87;
            level_Of_IncomeLabel.Text = "Level Of Income:";
            // 
            // sport_1Label
            // 
            sport_1Label.AutoSize = true;
            sport_1Label.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            sport_1Label.Location = new System.Drawing.Point(60, 39);
            sport_1Label.Name = "sport_1Label";
            sport_1Label.Size = new System.Drawing.Size(53, 17);
            sport_1Label.TabIndex = 0;
            sport_1Label.Text = "Sport 1:";
            // 
            // sport_2Label
            // 
            sport_2Label.AutoSize = true;
            sport_2Label.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            sport_2Label.Location = new System.Drawing.Point(60, 74);
            sport_2Label.Name = "sport_2Label";
            sport_2Label.Size = new System.Drawing.Size(53, 17);
            sport_2Label.TabIndex = 2;
            sport_2Label.Text = "Sport 2:";
            // 
            // sport_3Label
            // 
            sport_3Label.AutoSize = true;
            sport_3Label.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            sport_3Label.Location = new System.Drawing.Point(60, 110);
            sport_3Label.Name = "sport_3Label";
            sport_3Label.Size = new System.Drawing.Size(53, 17);
            sport_3Label.TabIndex = 4;
            sport_3Label.Text = "Sport 3:";
            // 
            // level_1Label
            // 
            level_1Label.AutoSize = true;
            level_1Label.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            level_1Label.Location = new System.Drawing.Point(59, 147);
            level_1Label.Name = "level_1Label";
            level_1Label.Size = new System.Drawing.Size(55, 17);
            level_1Label.TabIndex = 6;
            level_1Label.Text = "Level 1:";
            // 
            // level_2Label
            // 
            level_2Label.AutoSize = true;
            level_2Label.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            level_2Label.Location = new System.Drawing.Point(59, 184);
            level_2Label.Name = "level_2Label";
            level_2Label.Size = new System.Drawing.Size(55, 17);
            level_2Label.TabIndex = 8;
            level_2Label.Text = "Level 2:";
            // 
            // level_3Label
            // 
            level_3Label.AutoSize = true;
            level_3Label.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            level_3Label.Location = new System.Drawing.Point(59, 222);
            level_3Label.Name = "level_3Label";
            level_3Label.Size = new System.Drawing.Size(55, 17);
            level_3Label.TabIndex = 10;
            level_3Label.Text = "Level 3:";
            // 
            // medal_1Label
            // 
            medal_1Label.AutoSize = true;
            medal_1Label.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            medal_1Label.Location = new System.Drawing.Point(56, 260);
            medal_1Label.Name = "medal_1Label";
            medal_1Label.Size = new System.Drawing.Size(59, 17);
            medal_1Label.TabIndex = 12;
            medal_1Label.Text = "Medal 1:";
            // 
            // medal_2Label
            // 
            medal_2Label.AutoSize = true;
            medal_2Label.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            medal_2Label.Location = new System.Drawing.Point(56, 298);
            medal_2Label.Name = "medal_2Label";
            medal_2Label.Size = new System.Drawing.Size(59, 17);
            medal_2Label.TabIndex = 14;
            medal_2Label.Text = "Medal 2:";
            // 
            // medal_3Label
            // 
            medal_3Label.AutoSize = true;
            medal_3Label.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            medal_3Label.Location = new System.Drawing.Point(56, 335);
            medal_3Label.Name = "medal_3Label";
            medal_3Label.Size = new System.Drawing.Size(59, 17);
            medal_3Label.TabIndex = 16;
            medal_3Label.Text = "Medal 3:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(211, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(125, 20);
            this.label1.TabIndex = 0;
            this.label1.Text = "Soldier Master";
            // 
            // soldier_MasterBindingNavigator
            // 
            this.soldier_MasterBindingNavigator.AddNewItem = this.bindingNavigatorAddNewItem;
            this.soldier_MasterBindingNavigator.BindingSource = this.soldier_MasterBindingSource;
            this.soldier_MasterBindingNavigator.CountItem = this.bindingNavigatorCountItem;
            this.soldier_MasterBindingNavigator.DeleteItem = null;
            this.soldier_MasterBindingNavigator.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.bindingNavigatorMoveFirstItem,
            this.bindingNavigatorMovePreviousItem,
            this.bindingNavigatorSeparator,
            this.bindingNavigatorPositionItem,
            this.bindingNavigatorCountItem,
            this.bindingNavigatorSeparator1,
            this.bindingNavigatorMoveNextItem,
            this.bindingNavigatorMoveLastItem,
            this.bindingNavigatorSeparator2,
            this.bindingNavigatorAddNewItem,
            this.toolStripButton1,
            this.toolStripButton2,
            this.soldier_MasterBindingNavigatorSaveItem});
            this.soldier_MasterBindingNavigator.Location = new System.Drawing.Point(0, 0);
            this.soldier_MasterBindingNavigator.MoveFirstItem = this.bindingNavigatorMoveFirstItem;
            this.soldier_MasterBindingNavigator.MoveLastItem = this.bindingNavigatorMoveLastItem;
            this.soldier_MasterBindingNavigator.MoveNextItem = this.bindingNavigatorMoveNextItem;
            this.soldier_MasterBindingNavigator.MovePreviousItem = this.bindingNavigatorMovePreviousItem;
            this.soldier_MasterBindingNavigator.Name = "soldier_MasterBindingNavigator";
            this.soldier_MasterBindingNavigator.PositionItem = this.bindingNavigatorPositionItem;
            this.soldier_MasterBindingNavigator.Size = new System.Drawing.Size(684, 25);
            this.soldier_MasterBindingNavigator.TabIndex = 1;
            this.soldier_MasterBindingNavigator.Text = "bindingNavigator1";
            // 
            // bindingNavigatorAddNewItem
            // 
            this.bindingNavigatorAddNewItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorAddNewItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorAddNewItem.Image")));
            this.bindingNavigatorAddNewItem.Name = "bindingNavigatorAddNewItem";
            this.bindingNavigatorAddNewItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorAddNewItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorAddNewItem.Text = "Add new data";
            this.bindingNavigatorAddNewItem.Click += new System.EventHandler(this.bindingNavigatorAddNewItem_Click);
            // 
            // soldier_MasterBindingSource
            // 
            this.soldier_MasterBindingSource.DataMember = "Soldier_Master";
            this.soldier_MasterBindingSource.DataSource = this.dataSet1;
            // 
            // dataSet1
            // 
            this.dataSet1.DataSetName = "DataSet1";
            this.dataSet1.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // bindingNavigatorCountItem
            // 
            this.bindingNavigatorCountItem.Name = "bindingNavigatorCountItem";
            this.bindingNavigatorCountItem.Size = new System.Drawing.Size(35, 22);
            this.bindingNavigatorCountItem.Text = "of {0}";
            this.bindingNavigatorCountItem.ToolTipText = "Total number of items";
            // 
            // bindingNavigatorMoveFirstItem
            // 
            this.bindingNavigatorMoveFirstItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMoveFirstItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveFirstItem.Image")));
            this.bindingNavigatorMoveFirstItem.Name = "bindingNavigatorMoveFirstItem";
            this.bindingNavigatorMoveFirstItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMoveFirstItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorMoveFirstItem.Text = "Move first";
            // 
            // bindingNavigatorMovePreviousItem
            // 
            this.bindingNavigatorMovePreviousItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMovePreviousItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMovePreviousItem.Image")));
            this.bindingNavigatorMovePreviousItem.Name = "bindingNavigatorMovePreviousItem";
            this.bindingNavigatorMovePreviousItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMovePreviousItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorMovePreviousItem.Text = "Move previous";
            // 
            // bindingNavigatorSeparator
            // 
            this.bindingNavigatorSeparator.Name = "bindingNavigatorSeparator";
            this.bindingNavigatorSeparator.Size = new System.Drawing.Size(6, 25);
            // 
            // bindingNavigatorPositionItem
            // 
            this.bindingNavigatorPositionItem.AccessibleName = "Position";
            this.bindingNavigatorPositionItem.AutoSize = false;
            this.bindingNavigatorPositionItem.Name = "bindingNavigatorPositionItem";
            this.bindingNavigatorPositionItem.Size = new System.Drawing.Size(50, 23);
            this.bindingNavigatorPositionItem.Text = "0";
            this.bindingNavigatorPositionItem.ToolTipText = "Current position";
            // 
            // bindingNavigatorSeparator1
            // 
            this.bindingNavigatorSeparator1.Name = "bindingNavigatorSeparator1";
            this.bindingNavigatorSeparator1.Size = new System.Drawing.Size(6, 25);
            // 
            // bindingNavigatorMoveNextItem
            // 
            this.bindingNavigatorMoveNextItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMoveNextItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveNextItem.Image")));
            this.bindingNavigatorMoveNextItem.Name = "bindingNavigatorMoveNextItem";
            this.bindingNavigatorMoveNextItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMoveNextItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorMoveNextItem.Text = "Move next";
            // 
            // bindingNavigatorMoveLastItem
            // 
            this.bindingNavigatorMoveLastItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMoveLastItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveLastItem.Image")));
            this.bindingNavigatorMoveLastItem.Name = "bindingNavigatorMoveLastItem";
            this.bindingNavigatorMoveLastItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMoveLastItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorMoveLastItem.Text = "Move last";
            // 
            // bindingNavigatorSeparator2
            // 
            this.bindingNavigatorSeparator2.Name = "bindingNavigatorSeparator2";
            this.bindingNavigatorSeparator2.Size = new System.Drawing.Size(6, 25);
            // 
            // toolStripButton1
            // 
            this.toolStripButton1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton1.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton1.Image")));
            this.toolStripButton1.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton1.Name = "toolStripButton1";
            this.toolStripButton1.Size = new System.Drawing.Size(23, 22);
            this.toolStripButton1.Text = "Delete";
            this.toolStripButton1.Click += new System.EventHandler(this.toolStripButton1_Click);
            // 
            // toolStripButton2
            // 
            this.toolStripButton2.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton2.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton2.Image")));
            this.toolStripButton2.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton2.Name = "toolStripButton2";
            this.toolStripButton2.Size = new System.Drawing.Size(23, 22);
            this.toolStripButton2.Text = "Update";
            this.toolStripButton2.Click += new System.EventHandler(this.toolStripButton2_Click);
            // 
            // soldier_MasterBindingNavigatorSaveItem
            // 
            this.soldier_MasterBindingNavigatorSaveItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.soldier_MasterBindingNavigatorSaveItem.Image = ((System.Drawing.Image)(resources.GetObject("soldier_MasterBindingNavigatorSaveItem.Image")));
            this.soldier_MasterBindingNavigatorSaveItem.Name = "soldier_MasterBindingNavigatorSaveItem";
            this.soldier_MasterBindingNavigatorSaveItem.Size = new System.Drawing.Size(23, 22);
            this.soldier_MasterBindingNavigatorSaveItem.Text = "Save";
            this.soldier_MasterBindingNavigatorSaveItem.Click += new System.EventHandler(this.soldier_MasterBindingNavigatorSaveItem_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Times New Roman", 15.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(275, 29);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(140, 24);
            this.label2.TabIndex = 80;
            this.label2.Text = "Soldier Master";
            // 
            // button3
            // 
            this.button3.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button3.Location = new System.Drawing.Point(501, 41);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(75, 23);
            this.button3.TabIndex = 83;
            this.button3.Text = "Search";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // soldier_MasterTableAdapter
            // 
            this.soldier_MasterTableAdapter.ClearBeforeFill = true;
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Controls.Add(this.tabPage4);
            this.tabControl1.Controls.Add(this.tabPage5);
            this.tabControl1.Location = new System.Drawing.Point(25, 70);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(630, 400);
            this.tabControl1.TabIndex = 87;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(section_NameLabel);
            this.tabPage1.Controls.Add(this.section_NameComboBox);
            this.tabPage1.Controls.Add(platoon_IdLabel);
            this.tabPage1.Controls.Add(this.platoon_IdComboBox);
            this.tabPage1.Controls.Add(company_IdLabel);
            this.tabPage1.Controls.Add(this.company_IdComboBox);
            this.tabPage1.Controls.Add(battalion_IdLabel);
            this.tabPage1.Controls.Add(this.battalion_IdComboBox);
            this.tabPage1.Controls.Add(this.button5);
            this.tabPage1.Controls.Add(this.army_NoTextBox);
            this.tabPage1.Controls.Add(army_NoLabel);
            this.tabPage1.Controls.Add(this.rankTextBox);
            this.tabPage1.Controls.Add(this.nOKTextBox);
            this.tabPage1.Controls.Add(nOKLabel);
            this.tabPage1.Controls.Add(rankLabel);
            this.tabPage1.Controls.Add(this.dOJDateTimePicker);
            this.tabPage1.Controls.Add(dOJLabel);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(622, 374);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Basic_info";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // section_NameComboBox
            // 
            this.section_NameComboBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.soldier_MasterBindingSource, "Section_Name", true));
            this.section_NameComboBox.DataSource = this.sectionMasterBindingSource1;
            this.section_NameComboBox.DisplayMember = "Section_Name";
            this.section_NameComboBox.FormattingEnabled = true;
            this.section_NameComboBox.Location = new System.Drawing.Point(136, 137);
            this.section_NameComboBox.Name = "section_NameComboBox";
            this.section_NameComboBox.Size = new System.Drawing.Size(202, 21);
            this.section_NameComboBox.TabIndex = 46;
            // 
            // sectionMasterBindingSource1
            // 
            this.sectionMasterBindingSource1.DataMember = "Section_Master";
            this.sectionMasterBindingSource1.DataSource = this.dataSet1;
            // 
            // platoon_IdComboBox
            // 
            this.platoon_IdComboBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.soldier_MasterBindingSource, "Platoon_Id", true));
            this.platoon_IdComboBox.DataSource = this.platoonMasterBindingSource;
            this.platoon_IdComboBox.DisplayMember = "Platoon_Id";
            this.platoon_IdComboBox.FormattingEnabled = true;
            this.platoon_IdComboBox.Location = new System.Drawing.Point(136, 101);
            this.platoon_IdComboBox.Name = "platoon_IdComboBox";
            this.platoon_IdComboBox.Size = new System.Drawing.Size(202, 21);
            this.platoon_IdComboBox.TabIndex = 45;
            // 
            // platoonMasterBindingSource
            // 
            this.platoonMasterBindingSource.DataMember = "Platoon_Master";
            this.platoonMasterBindingSource.DataSource = this.dataSet1;
            // 
            // company_IdComboBox
            // 
            this.company_IdComboBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.soldier_MasterBindingSource, "Company_Id", true));
            this.company_IdComboBox.DataSource = this.companyMasterBindingSource;
            this.company_IdComboBox.DisplayMember = "Company_Id";
            this.company_IdComboBox.FormattingEnabled = true;
            this.company_IdComboBox.Location = new System.Drawing.Point(136, 66);
            this.company_IdComboBox.Name = "company_IdComboBox";
            this.company_IdComboBox.Size = new System.Drawing.Size(202, 21);
            this.company_IdComboBox.TabIndex = 44;
            // 
            // companyMasterBindingSource
            // 
            this.companyMasterBindingSource.DataMember = "Company_Master";
            this.companyMasterBindingSource.DataSource = this.dataSet1;
            // 
            // battalion_IdComboBox
            // 
            this.battalion_IdComboBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.soldier_MasterBindingSource, "Battalion_Id", true));
            this.battalion_IdComboBox.DataSource = this.battalionMasterBindingSource;
            this.battalion_IdComboBox.DisplayMember = "Battalion_Id";
            this.battalion_IdComboBox.FormattingEnabled = true;
            this.battalion_IdComboBox.Location = new System.Drawing.Point(136, 32);
            this.battalion_IdComboBox.Name = "battalion_IdComboBox";
            this.battalion_IdComboBox.Size = new System.Drawing.Size(202, 21);
            this.battalion_IdComboBox.TabIndex = 43;
            // 
            // battalionMasterBindingSource
            // 
            this.battalionMasterBindingSource.DataMember = "Battalion_Master";
            this.battalionMasterBindingSource.DataSource = this.dataSet1;
            // 
            // button5
            // 
            this.button5.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button5.Location = new System.Drawing.Point(343, 173);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(135, 23);
            this.button5.TabIndex = 42;
            this.button5.Text = "Check Availability?";
            this.button5.UseVisualStyleBackColor = true;
            this.button5.Click += new System.EventHandler(this.button5_Click);
            // 
            // army_NoTextBox
            // 
            this.army_NoTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.soldier_MasterBindingSource, "Army_No", true));
            this.army_NoTextBox.Location = new System.Drawing.Point(137, 173);
            this.army_NoTextBox.Name = "army_NoTextBox";
            this.army_NoTextBox.Size = new System.Drawing.Size(200, 20);
            this.army_NoTextBox.TabIndex = 11;
            // 
            // rankTextBox
            // 
            this.rankTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.soldier_MasterBindingSource, "Rank", true));
            this.rankTextBox.Location = new System.Drawing.Point(137, 241);
            this.rankTextBox.Name = "rankTextBox";
            this.rankTextBox.Size = new System.Drawing.Size(200, 20);
            this.rankTextBox.TabIndex = 39;
            // 
            // nOKTextBox
            // 
            this.nOKTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.soldier_MasterBindingSource, "NOK", true));
            this.nOKTextBox.Location = new System.Drawing.Point(137, 276);
            this.nOKTextBox.Name = "nOKTextBox";
            this.nOKTextBox.Size = new System.Drawing.Size(200, 20);
            this.nOKTextBox.TabIndex = 41;
            // 
            // dOJDateTimePicker
            // 
            this.dOJDateTimePicker.DataBindings.Add(new System.Windows.Forms.Binding("Value", this.soldier_MasterBindingSource, "DOJ", true));
            this.dOJDateTimePicker.Location = new System.Drawing.Point(137, 207);
            this.dOJDateTimePicker.Name = "dOJDateTimePicker";
            this.dOJDateTimePicker.Size = new System.Drawing.Size(200, 20);
            this.dOJDateTimePicker.TabIndex = 37;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(level_Of_IncomeLabel);
            this.tabPage2.Controls.Add(this.level_Of_IncomeComboBox);
            this.tabPage2.Controls.Add(childrenLabel);
            this.tabPage2.Controls.Add(this.childrenTextBox);
            this.tabPage2.Controls.Add(sonLabel);
            this.tabPage2.Controls.Add(this.sonTextBox);
            this.tabPage2.Controls.Add(daughterLabel);
            this.tabPage2.Controls.Add(this.daughterTextBox);
            this.tabPage2.Controls.Add(bank_Acc_NoLabel);
            this.tabPage2.Controls.Add(this.bank_Acc_NoTextBox);
            this.tabPage2.Controls.Add(pANLabel);
            this.tabPage2.Controls.Add(this.pANTextBox);
            this.tabPage2.Controls.Add(billLabel);
            this.tabPage2.Controls.Add(this.billTextBox);
            this.tabPage2.Controls.Add(joint_Acc_NoLabel);
            this.tabPage2.Controls.Add(this.joint_Acc_NoTextBox);
            this.tabPage2.Controls.Add(this.addressTextBox);
            this.tabPage2.Controls.Add(this.civil_OccupationTextBox);
            this.tabPage2.Controls.Add(civil_OccupationLabel);
            this.tabPage2.Controls.Add(addressLabel);
            this.tabPage2.Controls.Add(this.dOBDateTimePicker);
            this.tabPage2.Controls.Add(enameLabel);
            this.tabPage2.Controls.Add(dOBLabel);
            this.tabPage2.Controls.Add(this.enameTextBox);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(622, 374);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Personal";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // level_Of_IncomeComboBox
            // 
            this.level_Of_IncomeComboBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.soldier_MasterBindingSource, "Level_Of_Income", true));
            this.level_Of_IncomeComboBox.FormattingEnabled = true;
            this.level_Of_IncomeComboBox.Items.AddRange(new object[] {
            "10,000 or below",
            "10,000 to 50,000",
            "50,000 to 75,000",
            "75,000 to 1,00,000",
            "1,00,000 or above"});
            this.level_Of_IncomeComboBox.Location = new System.Drawing.Point(186, 139);
            this.level_Of_IncomeComboBox.Name = "level_Of_IncomeComboBox";
            this.level_Of_IncomeComboBox.Size = new System.Drawing.Size(226, 21);
            this.level_Of_IncomeComboBox.TabIndex = 88;
            // 
            // childrenTextBox
            // 
            this.childrenTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.soldier_MasterBindingSource, "Children", true));
            this.childrenTextBox.Location = new System.Drawing.Point(186, 166);
            this.childrenTextBox.Name = "childrenTextBox";
            this.childrenTextBox.Size = new System.Drawing.Size(226, 20);
            this.childrenTextBox.TabIndex = 75;
            // 
            // sonTextBox
            // 
            this.sonTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.soldier_MasterBindingSource, "Son", true));
            this.sonTextBox.Location = new System.Drawing.Point(186, 192);
            this.sonTextBox.Name = "sonTextBox";
            this.sonTextBox.Size = new System.Drawing.Size(226, 20);
            this.sonTextBox.TabIndex = 77;
            // 
            // daughterTextBox
            // 
            this.daughterTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.soldier_MasterBindingSource, "Daughter", true));
            this.daughterTextBox.Location = new System.Drawing.Point(186, 218);
            this.daughterTextBox.Name = "daughterTextBox";
            this.daughterTextBox.Size = new System.Drawing.Size(226, 20);
            this.daughterTextBox.TabIndex = 79;
            // 
            // bank_Acc_NoTextBox
            // 
            this.bank_Acc_NoTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.soldier_MasterBindingSource, "Bank_Acc_No", true));
            this.bank_Acc_NoTextBox.Location = new System.Drawing.Point(186, 244);
            this.bank_Acc_NoTextBox.Name = "bank_Acc_NoTextBox";
            this.bank_Acc_NoTextBox.Size = new System.Drawing.Size(226, 20);
            this.bank_Acc_NoTextBox.TabIndex = 81;
            // 
            // pANTextBox
            // 
            this.pANTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.soldier_MasterBindingSource, "PAN", true));
            this.pANTextBox.Location = new System.Drawing.Point(186, 270);
            this.pANTextBox.Name = "pANTextBox";
            this.pANTextBox.Size = new System.Drawing.Size(226, 20);
            this.pANTextBox.TabIndex = 83;
            // 
            // billTextBox
            // 
            this.billTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.soldier_MasterBindingSource, "Bill", true));
            this.billTextBox.Location = new System.Drawing.Point(186, 296);
            this.billTextBox.Name = "billTextBox";
            this.billTextBox.Size = new System.Drawing.Size(226, 20);
            this.billTextBox.TabIndex = 85;
            // 
            // joint_Acc_NoTextBox
            // 
            this.joint_Acc_NoTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.soldier_MasterBindingSource, "Joint_Acc_No", true));
            this.joint_Acc_NoTextBox.Location = new System.Drawing.Point(186, 322);
            this.joint_Acc_NoTextBox.Name = "joint_Acc_NoTextBox";
            this.joint_Acc_NoTextBox.Size = new System.Drawing.Size(226, 20);
            this.joint_Acc_NoTextBox.TabIndex = 87;
            // 
            // addressTextBox
            // 
            this.addressTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.soldier_MasterBindingSource, "Address", true));
            this.addressTextBox.Location = new System.Drawing.Point(186, 79);
            this.addressTextBox.Name = "addressTextBox";
            this.addressTextBox.Size = new System.Drawing.Size(226, 20);
            this.addressTextBox.TabIndex = 17;
            // 
            // civil_OccupationTextBox
            // 
            this.civil_OccupationTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.soldier_MasterBindingSource, "Civil_Occupation", true));
            this.civil_OccupationTextBox.Location = new System.Drawing.Point(186, 105);
            this.civil_OccupationTextBox.Name = "civil_OccupationTextBox";
            this.civil_OccupationTextBox.Size = new System.Drawing.Size(226, 20);
            this.civil_OccupationTextBox.TabIndex = 21;
            // 
            // dOBDateTimePicker
            // 
            this.dOBDateTimePicker.DataBindings.Add(new System.Windows.Forms.Binding("Value", this.soldier_MasterBindingSource, "DOB", true));
            this.dOBDateTimePicker.Location = new System.Drawing.Point(186, 53);
            this.dOBDateTimePicker.Name = "dOBDateTimePicker";
            this.dOBDateTimePicker.Size = new System.Drawing.Size(226, 20);
            this.dOBDateTimePicker.TabIndex = 15;
            // 
            // enameTextBox
            // 
            this.enameTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.soldier_MasterBindingSource, "Ename", true));
            this.enameTextBox.Location = new System.Drawing.Point(186, 27);
            this.enameTextBox.Name = "enameTextBox";
            this.enameTextBox.Size = new System.Drawing.Size(226, 20);
            this.enameTextBox.TabIndex = 13;
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.grade_1TextBox);
            this.tabPage3.Controls.Add(this.grade_3TextBox);
            this.tabPage3.Controls.Add(grade_3Label);
            this.tabPage3.Controls.Add(this.grade_2TextBox);
            this.tabPage3.Controls.Add(grade_2Label);
            this.tabPage3.Controls.Add(grade_1Label);
            this.tabPage3.Controls.Add(this.course_3TextBox);
            this.tabPage3.Controls.Add(qualificationLabel);
            this.tabPage3.Controls.Add(course_3Label);
            this.tabPage3.Controls.Add(this.qualificationTextBox);
            this.tabPage3.Controls.Add(this.course_2TextBox);
            this.tabPage3.Controls.Add(army_QualiLabel);
            this.tabPage3.Controls.Add(course_2Label);
            this.tabPage3.Controls.Add(this.army_QualiTextBox);
            this.tabPage3.Location = new System.Drawing.Point(4, 22);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage3.Size = new System.Drawing.Size(622, 374);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "Education";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // grade_1TextBox
            // 
            this.grade_1TextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.soldier_MasterBindingSource, "Grade_1", true));
            this.grade_1TextBox.Location = new System.Drawing.Point(151, 200);
            this.grade_1TextBox.Name = "grade_1TextBox";
            this.grade_1TextBox.Size = new System.Drawing.Size(200, 20);
            this.grade_1TextBox.TabIndex = 31;
            // 
            // grade_3TextBox
            // 
            this.grade_3TextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.soldier_MasterBindingSource, "Grade_3", true));
            this.grade_3TextBox.Location = new System.Drawing.Point(151, 281);
            this.grade_3TextBox.Name = "grade_3TextBox";
            this.grade_3TextBox.Size = new System.Drawing.Size(200, 20);
            this.grade_3TextBox.TabIndex = 35;
            // 
            // grade_2TextBox
            // 
            this.grade_2TextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.soldier_MasterBindingSource, "Grade_2", true));
            this.grade_2TextBox.Location = new System.Drawing.Point(151, 240);
            this.grade_2TextBox.Name = "grade_2TextBox";
            this.grade_2TextBox.Size = new System.Drawing.Size(200, 20);
            this.grade_2TextBox.TabIndex = 33;
            // 
            // course_3TextBox
            // 
            this.course_3TextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.soldier_MasterBindingSource, "Course_3", true));
            this.course_3TextBox.Location = new System.Drawing.Point(151, 160);
            this.course_3TextBox.Name = "course_3TextBox";
            this.course_3TextBox.Size = new System.Drawing.Size(200, 20);
            this.course_3TextBox.TabIndex = 29;
            // 
            // qualificationTextBox
            // 
            this.qualificationTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.soldier_MasterBindingSource, "Qualification", true));
            this.qualificationTextBox.Location = new System.Drawing.Point(151, 42);
            this.qualificationTextBox.Name = "qualificationTextBox";
            this.qualificationTextBox.Size = new System.Drawing.Size(200, 20);
            this.qualificationTextBox.TabIndex = 19;
            // 
            // course_2TextBox
            // 
            this.course_2TextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.soldier_MasterBindingSource, "Course_2", true));
            this.course_2TextBox.Location = new System.Drawing.Point(151, 121);
            this.course_2TextBox.Name = "course_2TextBox";
            this.course_2TextBox.Size = new System.Drawing.Size(200, 20);
            this.course_2TextBox.TabIndex = 27;
            // 
            // army_QualiTextBox
            // 
            this.army_QualiTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.soldier_MasterBindingSource, "Army_Quali", true));
            this.army_QualiTextBox.Location = new System.Drawing.Point(151, 81);
            this.army_QualiTextBox.Name = "army_QualiTextBox";
            this.army_QualiTextBox.Size = new System.Drawing.Size(200, 20);
            this.army_QualiTextBox.TabIndex = 25;
            // 
            // tabPage4
            // 
            this.tabPage4.Controls.Add(medal_3Label);
            this.tabPage4.Controls.Add(this.medal_3ComboBox);
            this.tabPage4.Controls.Add(medal_2Label);
            this.tabPage4.Controls.Add(this.medal_2ComboBox);
            this.tabPage4.Controls.Add(medal_1Label);
            this.tabPage4.Controls.Add(this.medal_1ComboBox);
            this.tabPage4.Controls.Add(level_3Label);
            this.tabPage4.Controls.Add(this.level_3ComboBox);
            this.tabPage4.Controls.Add(level_2Label);
            this.tabPage4.Controls.Add(this.level_2ComboBox);
            this.tabPage4.Controls.Add(level_1Label);
            this.tabPage4.Controls.Add(this.level_1ComboBox);
            this.tabPage4.Controls.Add(sport_3Label);
            this.tabPage4.Controls.Add(this.sport_3ComboBox);
            this.tabPage4.Controls.Add(sport_2Label);
            this.tabPage4.Controls.Add(this.sport_2ComboBox);
            this.tabPage4.Controls.Add(sport_1Label);
            this.tabPage4.Controls.Add(this.sport_1ComboBox);
            this.tabPage4.Location = new System.Drawing.Point(4, 22);
            this.tabPage4.Name = "tabPage4";
            this.tabPage4.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage4.Size = new System.Drawing.Size(622, 374);
            this.tabPage4.TabIndex = 3;
            this.tabPage4.Text = "Sports";
            this.tabPage4.UseVisualStyleBackColor = true;
            // 
            // medal_3ComboBox
            // 
            this.medal_3ComboBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.soldier_MasterBindingSource, "Medal_3", true));
            this.medal_3ComboBox.FormattingEnabled = true;
            this.medal_3ComboBox.Items.AddRange(new object[] {
            "Gold",
            "Silver",
            "Bronze"});
            this.medal_3ComboBox.Location = new System.Drawing.Point(132, 334);
            this.medal_3ComboBox.Name = "medal_3ComboBox";
            this.medal_3ComboBox.Size = new System.Drawing.Size(207, 21);
            this.medal_3ComboBox.TabIndex = 17;
            // 
            // medal_2ComboBox
            // 
            this.medal_2ComboBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.soldier_MasterBindingSource, "Medal_2", true));
            this.medal_2ComboBox.FormattingEnabled = true;
            this.medal_2ComboBox.Items.AddRange(new object[] {
            "Gold",
            "Silver",
            "Bronze"});
            this.medal_2ComboBox.Location = new System.Drawing.Point(132, 297);
            this.medal_2ComboBox.Name = "medal_2ComboBox";
            this.medal_2ComboBox.Size = new System.Drawing.Size(207, 21);
            this.medal_2ComboBox.TabIndex = 15;
            // 
            // medal_1ComboBox
            // 
            this.medal_1ComboBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.soldier_MasterBindingSource, "Medal_1", true));
            this.medal_1ComboBox.FormattingEnabled = true;
            this.medal_1ComboBox.Items.AddRange(new object[] {
            "Gold",
            "Silver",
            "Bronze"});
            this.medal_1ComboBox.Location = new System.Drawing.Point(132, 259);
            this.medal_1ComboBox.Name = "medal_1ComboBox";
            this.medal_1ComboBox.Size = new System.Drawing.Size(207, 21);
            this.medal_1ComboBox.TabIndex = 13;
            // 
            // level_3ComboBox
            // 
            this.level_3ComboBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.soldier_MasterBindingSource, "Level_3", true));
            this.level_3ComboBox.FormattingEnabled = true;
            this.level_3ComboBox.Location = new System.Drawing.Point(132, 221);
            this.level_3ComboBox.Name = "level_3ComboBox";
            this.level_3ComboBox.Size = new System.Drawing.Size(207, 21);
            this.level_3ComboBox.TabIndex = 11;
            // 
            // level_2ComboBox
            // 
            this.level_2ComboBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.soldier_MasterBindingSource, "Level_2", true));
            this.level_2ComboBox.FormattingEnabled = true;
            this.level_2ComboBox.Location = new System.Drawing.Point(132, 183);
            this.level_2ComboBox.Name = "level_2ComboBox";
            this.level_2ComboBox.Size = new System.Drawing.Size(207, 21);
            this.level_2ComboBox.TabIndex = 9;
            // 
            // level_1ComboBox
            // 
            this.level_1ComboBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.soldier_MasterBindingSource, "Level_1", true));
            this.level_1ComboBox.FormattingEnabled = true;
            this.level_1ComboBox.Location = new System.Drawing.Point(132, 146);
            this.level_1ComboBox.Name = "level_1ComboBox";
            this.level_1ComboBox.Size = new System.Drawing.Size(207, 21);
            this.level_1ComboBox.TabIndex = 7;
            // 
            // sport_3ComboBox
            // 
            this.sport_3ComboBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.soldier_MasterBindingSource, "Sport_3", true));
            this.sport_3ComboBox.FormattingEnabled = true;
            this.sport_3ComboBox.Items.AddRange(new object[] {
            "Army Ten Miler",
            "Basketball - Men\'s",
            "Basketball - Women\'s",
            "Boxing",
            "Bowling ",
            "Cross Country ",
            "Golf  ",
            "Marathon ",
            "Rugby",
            "Soccer - Men\'s ",
            "Soccer - Women\'s ",
            "Softball - Men\'s ",
            "Softball - Women\'s ",
            "Taekwondo ",
            "Triathlon ",
            "Volleyball - Men\'s ",
            "Volleyball - Women\'s ",
            "Wrestling "});
            this.sport_3ComboBox.Location = new System.Drawing.Point(132, 109);
            this.sport_3ComboBox.Name = "sport_3ComboBox";
            this.sport_3ComboBox.Size = new System.Drawing.Size(207, 21);
            this.sport_3ComboBox.TabIndex = 5;
            // 
            // sport_2ComboBox
            // 
            this.sport_2ComboBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.soldier_MasterBindingSource, "Sport_2", true));
            this.sport_2ComboBox.FormattingEnabled = true;
            this.sport_2ComboBox.Items.AddRange(new object[] {
            "Army Ten Miler",
            "Basketball - Men\'s",
            "Basketball - Women\'s",
            "Boxing",
            "Bowling ",
            "Cross Country ",
            "Golf  ",
            "Marathon ",
            "Rugby",
            "Soccer - Men\'s ",
            "Soccer - Women\'s ",
            "Softball - Men\'s ",
            "Softball - Women\'s ",
            "Taekwondo ",
            "Triathlon ",
            "Volleyball - Men\'s ",
            "Volleyball - Women\'s ",
            "Wrestling "});
            this.sport_2ComboBox.Location = new System.Drawing.Point(132, 73);
            this.sport_2ComboBox.Name = "sport_2ComboBox";
            this.sport_2ComboBox.Size = new System.Drawing.Size(207, 21);
            this.sport_2ComboBox.TabIndex = 3;
            // 
            // sport_1ComboBox
            // 
            this.sport_1ComboBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.soldier_MasterBindingSource, "Sport_1", true));
            this.sport_1ComboBox.FormattingEnabled = true;
            this.sport_1ComboBox.Items.AddRange(new object[] {
            "Army Ten Miler",
            "Basketball - Men\'s",
            "Basketball - Women\'s",
            "Boxing",
            "Bowling ",
            "Cross Country ",
            "Golf  ",
            "Marathon ",
            "Rugby",
            "Soccer - Men\'s ",
            "Soccer - Women\'s ",
            "Softball - Men\'s ",
            "Softball - Women\'s ",
            "Taekwondo ",
            "Triathlon ",
            "Volleyball - Men\'s ",
            "Volleyball - Women\'s ",
            "Wrestling "});
            this.sport_1ComboBox.Location = new System.Drawing.Point(132, 38);
            this.sport_1ComboBox.Name = "sport_1ComboBox";
            this.sport_1ComboBox.Size = new System.Drawing.Size(207, 21);
            this.sport_1ComboBox.TabIndex = 1;
            // 
            // tabPage5
            // 
            this.tabPage5.Controls.Add(red_InkLabel);
            this.tabPage5.Controls.Add(this.red_InkTextBox);
            this.tabPage5.Controls.Add(black_InkLabel);
            this.tabPage5.Controls.Add(this.black_InkTextBox);
            this.tabPage5.Controls.Add(descriptionLabel);
            this.tabPage5.Controls.Add(this.descriptionTextBox);
            this.tabPage5.Location = new System.Drawing.Point(4, 22);
            this.tabPage5.Name = "tabPage5";
            this.tabPage5.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage5.Size = new System.Drawing.Size(622, 374);
            this.tabPage5.TabIndex = 4;
            this.tabPage5.Text = "Punishment";
            this.tabPage5.UseVisualStyleBackColor = true;
            // 
            // red_InkTextBox
            // 
            this.red_InkTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.soldier_MasterBindingSource, "Red_Ink", true));
            this.red_InkTextBox.Location = new System.Drawing.Point(131, 43);
            this.red_InkTextBox.Name = "red_InkTextBox";
            this.red_InkTextBox.Size = new System.Drawing.Size(200, 20);
            this.red_InkTextBox.TabIndex = 81;
            // 
            // black_InkTextBox
            // 
            this.black_InkTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.soldier_MasterBindingSource, "Black_Ink", true));
            this.black_InkTextBox.Location = new System.Drawing.Point(131, 85);
            this.black_InkTextBox.Name = "black_InkTextBox";
            this.black_InkTextBox.Size = new System.Drawing.Size(200, 20);
            this.black_InkTextBox.TabIndex = 83;
            // 
            // descriptionTextBox
            // 
            this.descriptionTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.soldier_MasterBindingSource, "Description", true));
            this.descriptionTextBox.Location = new System.Drawing.Point(131, 124);
            this.descriptionTextBox.Multiline = true;
            this.descriptionTextBox.Name = "descriptionTextBox";
            this.descriptionTextBox.Size = new System.Drawing.Size(200, 66);
            this.descriptionTextBox.TabIndex = 85;
            // 
            // sectionMasterBindingSource
            // 
            this.sectionMasterBindingSource.DataMember = "Section_Master";
            this.sectionMasterBindingSource.DataSource = this.dataSet1;
            // 
            // section_MasterTableAdapter
            // 
            this.section_MasterTableAdapter.ClearBeforeFill = true;
            // 
            // battalion_MasterTableAdapter
            // 
            this.battalion_MasterTableAdapter.ClearBeforeFill = true;
            // 
            // company_MasterTableAdapter
            // 
            this.company_MasterTableAdapter.ClearBeforeFill = true;
            // 
            // platoon_MasterTableAdapter
            // 
            this.platoon_MasterTableAdapter.ClearBeforeFill = true;
            // 
            // Soldier_Master
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(684, 487);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.soldier_MasterBindingNavigator);
            this.Controls.Add(this.label1);
            this.Name = "Soldier_Master";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Soldier_Master";
            this.Load += new System.EventHandler(this.Soldier_Master_Load);
            ((System.ComponentModel.ISupportInitialize)(this.soldier_MasterBindingNavigator)).EndInit();
            this.soldier_MasterBindingNavigator.ResumeLayout(false);
            this.soldier_MasterBindingNavigator.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.soldier_MasterBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet1)).EndInit();
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.sectionMasterBindingSource1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.platoonMasterBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.companyMasterBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.battalionMasterBindingSource)).EndInit();
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            this.tabPage3.ResumeLayout(false);
            this.tabPage3.PerformLayout();
            this.tabPage4.ResumeLayout(false);
            this.tabPage4.PerformLayout();
            this.tabPage5.ResumeLayout(false);
            this.tabPage5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.sectionMasterBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private DataSet1 dataSet1;
        private System.Windows.Forms.BindingSource soldier_MasterBindingSource;
        private Military_Project.DataSet1TableAdapters.Soldier_MasterTableAdapter soldier_MasterTableAdapter;
        private System.Windows.Forms.BindingNavigator soldier_MasterBindingNavigator;
        private System.Windows.Forms.ToolStripButton bindingNavigatorAddNewItem;
        private System.Windows.Forms.ToolStripLabel bindingNavigatorCountItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMoveFirstItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMovePreviousItem;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator;
        private System.Windows.Forms.ToolStripTextBox bindingNavigatorPositionItem;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator1;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMoveNextItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMoveLastItem;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator2;
        private System.Windows.Forms.ToolStripButton soldier_MasterBindingNavigatorSaveItem;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TextBox army_NoTextBox;
        private System.Windows.Forms.TextBox rankTextBox;
        private System.Windows.Forms.TextBox nOKTextBox;
        private System.Windows.Forms.DateTimePicker dOJDateTimePicker;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.TextBox childrenTextBox;
        private System.Windows.Forms.TextBox sonTextBox;
        private System.Windows.Forms.TextBox daughterTextBox;
        private System.Windows.Forms.TextBox bank_Acc_NoTextBox;
        private System.Windows.Forms.TextBox pANTextBox;
        private System.Windows.Forms.TextBox billTextBox;
        private System.Windows.Forms.TextBox joint_Acc_NoTextBox;
        private System.Windows.Forms.TextBox addressTextBox;
        private System.Windows.Forms.TextBox civil_OccupationTextBox;
        private System.Windows.Forms.DateTimePicker dOBDateTimePicker;
        private System.Windows.Forms.TextBox enameTextBox;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.TextBox grade_1TextBox;
        private System.Windows.Forms.TextBox grade_3TextBox;
        private System.Windows.Forms.TextBox grade_2TextBox;
        private System.Windows.Forms.TextBox course_3TextBox;
        private System.Windows.Forms.TextBox qualificationTextBox;
        private System.Windows.Forms.TextBox course_2TextBox;
        private System.Windows.Forms.TextBox army_QualiTextBox;
        private System.Windows.Forms.TabPage tabPage4;
        private System.Windows.Forms.TabPage tabPage5;
        private System.Windows.Forms.TextBox red_InkTextBox;
        private System.Windows.Forms.TextBox black_InkTextBox;
        private System.Windows.Forms.TextBox descriptionTextBox;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.ToolStripButton toolStripButton1;
        private System.Windows.Forms.ToolStripButton toolStripButton2;
        private System.Windows.Forms.ComboBox section_NameComboBox;
        private System.Windows.Forms.ComboBox platoon_IdComboBox;
        private System.Windows.Forms.ComboBox company_IdComboBox;
        private System.Windows.Forms.ComboBox battalion_IdComboBox;
        private System.Windows.Forms.ComboBox level_Of_IncomeComboBox;
        private System.Windows.Forms.ComboBox level_3ComboBox;
        private System.Windows.Forms.ComboBox level_2ComboBox;
        private System.Windows.Forms.ComboBox level_1ComboBox;
        private System.Windows.Forms.ComboBox sport_3ComboBox;
        private System.Windows.Forms.ComboBox sport_2ComboBox;
        private System.Windows.Forms.ComboBox sport_1ComboBox;
        private System.Windows.Forms.ComboBox medal_3ComboBox;
        private System.Windows.Forms.ComboBox medal_2ComboBox;
        private System.Windows.Forms.ComboBox medal_1ComboBox;
        private System.Windows.Forms.BindingSource sectionMasterBindingSource;
        private Military_Project.DataSet1TableAdapters.Section_MasterTableAdapter section_MasterTableAdapter;
        private System.Windows.Forms.BindingSource battalionMasterBindingSource;
        private Military_Project.DataSet1TableAdapters.Battalion_MasterTableAdapter battalion_MasterTableAdapter;
        private System.Windows.Forms.BindingSource companyMasterBindingSource;
        private Military_Project.DataSet1TableAdapters.Company_MasterTableAdapter company_MasterTableAdapter;
        private System.Windows.Forms.BindingSource platoonMasterBindingSource;
        private Military_Project.DataSet1TableAdapters.Platoon_MasterTableAdapter platoon_MasterTableAdapter;
        private System.Windows.Forms.BindingSource sectionMasterBindingSource1;
    }
}