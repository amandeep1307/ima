using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Military_Project
{
    public partial class Soldier_Master : Form
    {
        public Soldier_Master()
        {
            InitializeComponent();
        }
        DataSet1TableAdapters.Soldier_MasterTableAdapter a1 = new Military_Project.DataSet1TableAdapters.Soldier_MasterTableAdapter();
        private void clearfn()
        {
            battalion_IdComboBox.Text = "";
            company_IdComboBox.Text = "";
            platoon_IdComboBox.Text = "";
            section_NameComboBox.Text = "";
            army_NoTextBox.Clear();
            enameTextBox.Clear();
            addressTextBox.Clear();
            qualificationTextBox.Clear();
            civil_OccupationTextBox.Clear();
            level_Of_IncomeComboBox.Text = "";
            army_QualiTextBox.Clear();
            course_2TextBox.Clear();
            course_3TextBox.Clear();
            grade_1TextBox.Clear();
            grade_2TextBox.Clear();
            grade_3TextBox.Clear();
            rankTextBox.Clear();
            nOKTextBox.Clear();
            sport_1ComboBox.Text = "";
            sport_2ComboBox.Text = "";
            sport_3ComboBox.Text = "";
            level_1ComboBox.Text = "";
            level_2ComboBox.Text = "";
            level_3ComboBox.Text = "";
            medal_1ComboBox.Text = "";
            medal_2ComboBox.Text = "";
            medal_3ComboBox.Text = "";
            childrenTextBox.Clear();
            sonTextBox.Clear();
            daughterTextBox.Clear();
            bank_Acc_NoTextBox.Clear();
            pANTextBox.Clear();
            billTextBox.Clear();
            joint_Acc_NoTextBox.Clear();
            red_InkTextBox.Clear();
            black_InkTextBox.Clear();
            descriptionTextBox.Clear();
        }

        private void soldier_MasterBindingNavigatorSaveItem_Click(object sender, EventArgs e)
        {
            if (battalion_IdComboBox.Text.Length != 0 && company_IdComboBox.Text.Length != 0 && platoon_IdComboBox.Text.Length != 0 && section_NameComboBox.Text.Length != 0 && army_NoTextBox.Text.Length != 0 && enameTextBox.Text.Length != 0 && addressTextBox.Text.Length != 0 && qualificationTextBox.Text.Length != 0 && civil_OccupationTextBox.Text.Length != 0 && level_Of_IncomeComboBox.Text.Length != 0 && army_QualiTextBox.Text.Length != 0 && course_2TextBox.Text.Length != 0 && course_3TextBox.Text.Length != 0 && grade_1TextBox.Text.Length != 0 && grade_2TextBox.Text.Length != 0 && grade_3TextBox.Text.Length != 0 && rankTextBox.Text.Length != 0 && nOKTextBox.Text.Length != 0 && sport_1ComboBox.Text.Length != 0 && sport_2ComboBox.Text.Length != 0 && sport_3ComboBox.Text.Length != 0 && level_1ComboBox.Text.Length != 0 && level_2ComboBox.Text.Length != 0 && level_3ComboBox.Text.Length != 0 && medal_1ComboBox.Text.Length != 0 && medal_2ComboBox.Text.Length != 0 && medal_3ComboBox.Text.Length != 0 && childrenTextBox.Text.Length != 0 && sonTextBox.Text.Length != 0 && daughterTextBox.Text.Length != 0 && bank_Acc_NoTextBox.Text.Length != 0 && pANTextBox.Text.Length != 0 && billTextBox.Text.Length != 0 && joint_Acc_NoTextBox.Text.Length != 0 && red_InkTextBox.Text.Length != 0 && black_InkTextBox.Text.Length != 0 && descriptionTextBox.Text.Length != 0)
            {
                a1.insert_soldier(battalion_IdComboBox.Text, company_IdComboBox.Text, platoon_IdComboBox.Text, section_NameComboBox.Text, army_NoTextBox.Text, enameTextBox.Text, dOBDateTimePicker.Value, addressTextBox.Text, qualificationTextBox.Text, civil_OccupationTextBox.Text, level_Of_IncomeComboBox.Text, army_QualiTextBox.Text, course_2TextBox.Text, course_3TextBox.Text, grade_1TextBox.Text, grade_2TextBox.Text, grade_3TextBox.Text, dOJDateTimePicker.Value, rankTextBox.Text, nOKTextBox.Text, sport_1ComboBox.Text, sport_2ComboBox.Text, sport_3ComboBox.Text, level_1ComboBox.Text, level_2ComboBox.Text, level_3ComboBox.Text, medal_1ComboBox.Text, medal_2ComboBox.Text, medal_3ComboBox.Text, childrenTextBox.Text, sonTextBox.Text, daughterTextBox.Text, bank_Acc_NoTextBox.Text, pANTextBox.Text, billTextBox.Text, joint_Acc_NoTextBox.Text, red_InkTextBox.Text, black_InkTextBox.Text, descriptionTextBox.Text);
                MessageBox.Show("Record Inserted Successfully !");
            }
            else
            {

                MessageBox.Show("Please enter all values !", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            try
            {
                int t = Convert.ToInt32(army_NoTextBox.Text);
            }
            catch (Exception h)
            {
                MessageBox.Show("Please provide number only at Army No.");
            }
        }

        private void Soldier_Master_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'dataSet1.Platoon_Master' table. You can move, or remove it, as needed.
            this.platoon_MasterTableAdapter.Fill(this.dataSet1.Platoon_Master);
            // TODO: This line of code loads data into the 'dataSet1.Company_Master' table. You can move, or remove it, as needed.
            this.company_MasterTableAdapter.Fill(this.dataSet1.Company_Master);
            // TODO: This line of code loads data into the 'dataSet1.Battalion_Master' table. You can move, or remove it, as needed.
            this.battalion_MasterTableAdapter.Fill(this.dataSet1.Battalion_Master);
            // TODO: This line of code loads data into the 'dataSet1.Section_Master' table. You can move, or remove it, as needed.
            //this.section_MasterTableAdapter.Fill(this.dataSet1.Section_Master);
            // TODO: This line of code loads data into the 'dataSet1.Soldier_Master' table. You can move, or remove it, as needed.
            this.soldier_MasterTableAdapter.Fill(this.dataSet1.Soldier_Master);
            //clearfn();
        }

        
        private void button3_Click(object sender, EventArgs e)
        {
            //DataTable dt = new DataTable();
            //dt = a1.search_soldier(army_NoTextBox.Text);
            //if (dt.Rows.Count > 0)
            //{
            //    battalion_IdTextBox.Text = dt.Rows[i][0].ToString();
            //    company_IdTextBox.Text = dt.Rows[i][1].ToString();
            //    platoon_IdTextBox.Text = dt.Rows[i][2].ToString();
            //    section_NameTextBox.Text = dt.Rows[i][3].ToString();
            //    army_NoTextBox.Text = dt.Rows[i][4].ToString();
            //    enameTextBox.Text = dt.Rows[i][5].ToString();
            //    dOBDateTimePicker.Value = Convert.ToDateTime(dt.Rows[i][6].ToString());
            //    addressTextBox.Text = dt.Rows[i][7].ToString();
            //    qualificationTextBox.Text = dt.Rows[i][8].ToString();
            //    civil_OccupationTextBox.Text = dt.Rows[i][9].ToString();
            //    level_Of_IncomeTextBox.Text = dt.Rows[i][10].ToString();
            //    army_QualiTextBox.Text = dt.Rows[i][11].ToString();
            //    course_2TextBox.Text = dt.Rows[i][12].ToString();
            //    course_3TextBox.Text = dt.Rows[i][13].ToString();
            //    grade_1TextBox.Text = dt.Rows[i][14].ToString();
            //    grade_2TextBox.Text = dt.Rows[i][15].ToString();
            //    grade_3TextBox.Text = dt.Rows[i][16].ToString();
            //    dOJDateTimePicker.Value = Convert.ToDateTime(dt.Rows[i][17].ToString());
            //    rankTextBox.Text = dt.Rows[i][18].ToString();
            //    nOKTextBox.Text = dt.Rows[i][19].ToString();
            //    sport_1TextBox.Text = dt.Rows[i][20].ToString();
            //    sport_2TextBox.Text = dt.Rows[i][21].ToString();
            //    sport_3TextBox.Text = dt.Rows[i][22].ToString();
            //    level_1TextBox.Text = dt.Rows[i][23].ToString();
            //    level_2TextBox.Text = dt.Rows[i][24].ToString();
            //    level_3TextBox.Text = dt.Rows[i][25].ToString();
            //    medal_1TextBox.Text = dt.Rows[i][26].ToString();
            //    medal_2TextBox.Text = dt.Rows[i][27].ToString();
            //    medal_3TextBox.Text = dt.Rows[i][28].ToString();
            //    childrenTextBox.Text = dt.Rows[i][29].ToString();
            //    sonTextBox.Text = dt.Rows[i][30].ToString();
            //    daughterTextBox.Text = dt.Rows[i][31].ToString();
            //    bank_Acc_NoTextBox.Text = dt.Rows[i][32].ToString();
            //    pANTextBox.Text = dt.Rows[i][33].ToString();
            //    billTextBox.Text = dt.Rows[i][34].ToString();
            //    joint_Acc_NoTextBox.Text = dt.Rows[i][35].ToString();
            //    red_InkTextBox.Text = dt.Rows[i][36].ToString();
            //    black_InkTextBox.Text = dt.Rows[i][37].ToString();
            //    descriptionTextBox.Text = dt.Rows[i][38].ToString();
               
            
            Asearch AS1 = new Asearch();
            AS1.ShowDialog();
           
            }

        private void button5_Click(object sender, EventArgs e)
        {
        DataTable dt = new DataTable();
            dt = a1.search_soldier(army_NoTextBox.Text);
            if (dt.Rows.Count > 0)
            {
                MessageBox.Show("Already Exists !");
                army_NoTextBox.Text = "";
                army_NoTextBox.Focus();
            }
            else
            {
                MessageBox.Show("Army_no Available !");
            }
        }

        private void bindingNavigatorAddNewItem_Click(object sender, EventArgs e)
        {
        
        }

        private void toolStripButton1_Click(object sender, EventArgs e)
        {
              a1.delete_soldier(army_NoTextBox.Text);
            MessageBox.Show("Record Deleted Successfully !");
        }

        private void toolStripButton2_Click(object sender, EventArgs e)
        {
            a1.update_soldier(battalion_IdComboBox.Text, company_IdComboBox.Text, platoon_IdComboBox.Text, section_NameComboBox.Text, army_NoTextBox.Text, enameTextBox.Text, dOBDateTimePicker.Value, addressTextBox.Text, qualificationTextBox.Text, civil_OccupationTextBox.Text, level_Of_IncomeComboBox.Text, army_QualiTextBox.Text, course_2TextBox.Text, course_3TextBox.Text, grade_1TextBox.Text, grade_2TextBox.Text, grade_3TextBox.Text, dOJDateTimePicker.Value, rankTextBox.Text, nOKTextBox.Text, sport_1ComboBox.Text, sport_2ComboBox.Text, sport_3ComboBox.Text, level_1ComboBox.Text, level_2ComboBox.Text, level_3ComboBox.Text, medal_1ComboBox.Text, medal_2ComboBox.Text, medal_3ComboBox.Text, childrenTextBox.Text, sonTextBox.Text, daughterTextBox.Text, bank_Acc_NoTextBox.Text, pANTextBox.Text, billTextBox.Text, joint_Acc_NoTextBox.Text, red_InkTextBox.Text, black_InkTextBox.Text, descriptionTextBox.Text);
            MessageBox.Show("Record Updated Successfully !");
        }
        }

        //private void button5_Click(object sender, EventArgs e)
        //{
            
        //}
    }