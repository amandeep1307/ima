using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Military_Project
{
    public partial class Sports_Master : Form
    {
        public Sports_Master()
        {
            InitializeComponent();
        }
        DataSet1TableAdapters.Sports_MasterTableAdapter a1 = new Military_Project.DataSet1TableAdapters.Sports_MasterTableAdapter();
        private void clearfn()
        {
            sports_IdTextBox.Clear();
            sports_NameTextBox.Clear();
            descriptionTextBox.Clear();
        }
        private void sports_MasterBindingNavigatorSaveItem_Click(object sender, EventArgs e)
        {
            if (sports_IdTextBox.Text.Length != 0 && sports_NameTextBox.Text.Length != 0 && descriptionTextBox.Text.Length != 0)
            {
                a1.insert_sports(sports_IdTextBox.Text, sports_NameTextBox.Text, descriptionTextBox.Text);
                MessageBox.Show("Record Inserted Successfully !");
            }
            else
            {
                MessageBox.Show("Please enter all values", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }
        private void Sports_Master_Load(object sender, EventArgs e)
        {

            // TODO: This line of code loads data into the 'dataSet1.Sports_Master' table. You can move, or remove it, as needed.
            this.sports_MasterTableAdapter.Fill(this.dataSet1.Sports_Master);
            //clearfn();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            DataTable dt = new DataTable();
            dt = a1.search_sports(sports_IdTextBox.Text);
            if (dt.Rows.Count > 0)
            {
                MessageBox.Show("Already Exists !");
                sports_IdTextBox.Text = "";
                sports_IdTextBox.Focus();
            }
            else
            {
                MessageBox.Show("Sports_Id Available !");
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            sports_search S1 = new sports_search();
            S1.ShowDialog();
            
        }

        private void toolStripButton1_Click(object sender, EventArgs e)
        {
            a1.delete_sports(sports_IdTextBox.Text);
            MessageBox.Show("Record Deleted Successfully !");
        }

        private void toolStripButton2_Click(object sender, EventArgs e)
        {
            a1.update_sports(sports_IdTextBox.Text, sports_NameTextBox.Text, descriptionTextBox.Text);
            MessageBox.Show("Record Updated Successfully !");
        }
    }
}