using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Military_Project
{
    public partial class TSearch : Form
    {
        public TSearch()
        {
            InitializeComponent();
        }
        DataSet1TableAdapters.Training_MasterTableAdapter TS1 = new Military_Project.DataSet1TableAdapters.Training_MasterTableAdapter();
        private void training_MasterBindingNavigatorSaveItem_Click(object sender, EventArgs e)
        {
            this.Validate();
            this.training_MasterBindingSource.EndEdit();
            this.training_MasterTableAdapter.Update(this.dataSet1.Training_Master);

        }

        private void TSearch_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'dataSet1.Training_Master' table. You can move, or remove it, as needed.
            this.training_MasterTableAdapter.Fill(this.dataSet1.Training_Master);
            nameComboBox.Text = "";
        }

        private void nameComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void dOBDateTimePicker_ValueChanged(object sender, EventArgs e)
        {
            
        }

        private void dOBComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            DataTable dt = new DataTable();
            dt = TS1.search_training(nameComboBox.Text, dOBDateTimePicker.Value);
            if (dt.Rows.Count > 0)
            {
                training_MasterDataGridView.DataSource = dt;
            }
        }

        private void dOBLabel1_Click(object sender, EventArgs e)
        {

        }
    }
}