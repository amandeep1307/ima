using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Military_Project
{
    public partial class Training : Form
    {
        public Training()
        {
            InitializeComponent();
        }

        private void Training_Load(object sender, EventArgs e)
        {
            CrystalReport3 cr3 = new CrystalReport3();
            //cr3.Load("D:/c # .net complete/Military Project/Military Project/Military Project/CrystalReport3.rpt");
            cr3.Load("D:/new d drive/pendrive project/Military Project/Military Project/CrystalReport3.rpt");
            DataSet1TableAdapters.Training_MasterTableAdapter c3 = new Military_Project.DataSet1TableAdapters.Training_MasterTableAdapter();
            DataTable dt = new DataTable();
            UES_Application u1=new UES_Application();
            dt = c3.search_training(UES_Application.ss, Convert.ToDateTime(UES_Application.dst)); 
            cr3.SetDataSource(dt);
            cr3.Refresh();
            crystalReportViewer1.ReportSource = cr3;
       
        }

        private void crystalReportViewer1_Load(object sender, EventArgs e)
        {

        }
    }
}