namespace Military_Project
{
    partial class Training_Master
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.Label nameLabel;
            System.Windows.Forms.Label surnameLabel;
            System.Windows.Forms.Label expanded_Full_NameLabel;
            System.Windows.Forms.Label dOBLabel;
            System.Windows.Forms.Label fNameLabel;
            System.Windows.Forms.Label fOccupLabel;
            System.Windows.Forms.Label addressLabel;
            System.Windows.Forms.Label pin1Label;
            System.Windows.Forms.Label tele_FaxLabel;
            System.Windows.Forms.Label emailLabel;
            System.Windows.Forms.Label permanent_AddLabel;
            System.Windows.Forms.Label pin2Label;
            System.Windows.Forms.Label tele_Fax2Label;
            System.Windows.Forms.Label marital_StatusLabel;
            System.Windows.Forms.Label stateLabel;
            System.Windows.Forms.Label nationalityLabel;
            System.Windows.Forms.Label visual_id_marksLabel;
            System.Windows.Forms.Label pP_Size_ImageLabel;
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Training_Master));
            this.training_MasterBindingNavigator = new System.Windows.Forms.BindingNavigator(this.components);
            this.bindingNavigatorAddNewItem = new System.Windows.Forms.ToolStripButton();
            this.training_MasterBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataSet1 = new Military_Project.DataSet1();
            this.bindingNavigatorCountItem = new System.Windows.Forms.ToolStripLabel();
            this.bindingNavigatorMoveFirstItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorMovePreviousItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorSeparator = new System.Windows.Forms.ToolStripSeparator();
            this.bindingNavigatorPositionItem = new System.Windows.Forms.ToolStripTextBox();
            this.bindingNavigatorSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.bindingNavigatorMoveNextItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorMoveLastItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripButton1 = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton2 = new System.Windows.Forms.ToolStripButton();
            this.training_MasterBindingNavigatorSaveItem = new System.Windows.Forms.ToolStripButton();
            this.training_MasterTableAdapter = new Military_Project.DataSet1TableAdapters.Training_MasterTableAdapter();
            this.button3 = new System.Windows.Forms.Button();
            this.nameTextBox = new System.Windows.Forms.TextBox();
            this.surnameTextBox = new System.Windows.Forms.TextBox();
            this.expanded_Full_NameTextBox = new System.Windows.Forms.TextBox();
            this.dOBDateTimePicker = new System.Windows.Forms.DateTimePicker();
            this.fNameTextBox = new System.Windows.Forms.TextBox();
            this.fOccupTextBox = new System.Windows.Forms.TextBox();
            this.addressTextBox = new System.Windows.Forms.TextBox();
            this.pin1TextBox = new System.Windows.Forms.TextBox();
            this.tele_FaxTextBox = new System.Windows.Forms.TextBox();
            this.emailTextBox = new System.Windows.Forms.TextBox();
            this.permanent_AddTextBox = new System.Windows.Forms.TextBox();
            this.pin2TextBox = new System.Windows.Forms.TextBox();
            this.tele_Fax2TextBox = new System.Windows.Forms.TextBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.marital_StatusTextBox = new System.Windows.Forms.TextBox();
            this.stateTextBox = new System.Windows.Forms.TextBox();
            this.nationalityTextBox = new System.Windows.Forms.TextBox();
            this.visual_id_marksTextBox = new System.Windows.Forms.TextBox();
            this.pP_Size_ImageTextBox = new System.Windows.Forms.TextBox();
            this.button1 = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            nameLabel = new System.Windows.Forms.Label();
            surnameLabel = new System.Windows.Forms.Label();
            expanded_Full_NameLabel = new System.Windows.Forms.Label();
            dOBLabel = new System.Windows.Forms.Label();
            fNameLabel = new System.Windows.Forms.Label();
            fOccupLabel = new System.Windows.Forms.Label();
            addressLabel = new System.Windows.Forms.Label();
            pin1Label = new System.Windows.Forms.Label();
            tele_FaxLabel = new System.Windows.Forms.Label();
            emailLabel = new System.Windows.Forms.Label();
            permanent_AddLabel = new System.Windows.Forms.Label();
            pin2Label = new System.Windows.Forms.Label();
            tele_Fax2Label = new System.Windows.Forms.Label();
            marital_StatusLabel = new System.Windows.Forms.Label();
            stateLabel = new System.Windows.Forms.Label();
            nationalityLabel = new System.Windows.Forms.Label();
            visual_id_marksLabel = new System.Windows.Forms.Label();
            pP_Size_ImageLabel = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.training_MasterBindingNavigator)).BeginInit();
            this.training_MasterBindingNavigator.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.training_MasterBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // nameLabel
            // 
            nameLabel.AutoSize = true;
            nameLabel.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            nameLabel.Location = new System.Drawing.Point(107, 74);
            nameLabel.Name = "nameLabel";
            nameLabel.Size = new System.Drawing.Size(47, 17);
            nameLabel.TabIndex = 37;
            nameLabel.Text = "Name:";
            // 
            // surnameLabel
            // 
            surnameLabel.AutoSize = true;
            surnameLabel.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            surnameLabel.Location = new System.Drawing.Point(91, 103);
            surnameLabel.Name = "surnameLabel";
            surnameLabel.Size = new System.Drawing.Size(63, 17);
            surnameLabel.TabIndex = 39;
            surnameLabel.Text = "Surname:";
            // 
            // expanded_Full_NameLabel
            // 
            expanded_Full_NameLabel.AutoSize = true;
            expanded_Full_NameLabel.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            expanded_Full_NameLabel.Location = new System.Drawing.Point(21, 134);
            expanded_Full_NameLabel.Name = "expanded_Full_NameLabel";
            expanded_Full_NameLabel.Size = new System.Drawing.Size(134, 17);
            expanded_Full_NameLabel.TabIndex = 41;
            expanded_Full_NameLabel.Text = "Expanded Full Name:";
            // 
            // dOBLabel
            // 
            dOBLabel.AutoSize = true;
            dOBLabel.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dOBLabel.Location = new System.Drawing.Point(111, 161);
            dOBLabel.Name = "dOBLabel";
            dOBLabel.Size = new System.Drawing.Size(43, 17);
            dOBLabel.TabIndex = 43;
            dOBLabel.Text = "DOB:";
            // 
            // fNameLabel
            // 
            fNameLabel.AutoSize = true;
            fNameLabel.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            fNameLabel.Location = new System.Drawing.Point(57, 192);
            fNameLabel.Name = "fNameLabel";
            fNameLabel.Size = new System.Drawing.Size(98, 17);
            fNameLabel.TabIndex = 45;
            fNameLabel.Text = "Father\'s Name:";
            // 
            // fOccupLabel
            // 
            fOccupLabel.AutoSize = true;
            fOccupLabel.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            fOccupLabel.Location = new System.Drawing.Point(25, 224);
            fOccupLabel.Name = "fOccupLabel";
            fOccupLabel.Size = new System.Drawing.Size(129, 17);
            fOccupLabel.TabIndex = 47;
            fOccupLabel.Text = "Father\'s Occupation:";
            // 
            // addressLabel
            // 
            addressLabel.AutoSize = true;
            addressLabel.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            addressLabel.Location = new System.Drawing.Point(94, 361);
            addressLabel.Name = "addressLabel";
            addressLabel.Size = new System.Drawing.Size(60, 17);
            addressLabel.TabIndex = 49;
            addressLabel.Text = "Address:";
            // 
            // pin1Label
            // 
            pin1Label.AutoSize = true;
            pin1Label.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            pin1Label.Location = new System.Drawing.Point(415, 361);
            pin1Label.Name = "pin1Label";
            pin1Label.Size = new System.Drawing.Size(37, 17);
            pin1Label.TabIndex = 51;
            pin1Label.Text = "Pin1:";
            // 
            // tele_FaxLabel
            // 
            tele_FaxLabel.AutoSize = true;
            tele_FaxLabel.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            tele_FaxLabel.Location = new System.Drawing.Point(390, 390);
            tele_FaxLabel.Name = "tele_FaxLabel";
            tele_FaxLabel.Size = new System.Drawing.Size(62, 17);
            tele_FaxLabel.TabIndex = 53;
            tele_FaxLabel.Text = "Tele Fax:";
            // 
            // emailLabel
            // 
            emailLabel.AutoSize = true;
            emailLabel.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            emailLabel.Location = new System.Drawing.Point(111, 278);
            emailLabel.Name = "emailLabel";
            emailLabel.Size = new System.Drawing.Size(44, 17);
            emailLabel.TabIndex = 55;
            emailLabel.Text = "Email:";
            // 
            // permanent_AddLabel
            // 
            permanent_AddLabel.AutoSize = true;
            permanent_AddLabel.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            permanent_AddLabel.Location = new System.Drawing.Point(28, 420);
            permanent_AddLabel.Name = "permanent_AddLabel";
            permanent_AddLabel.Size = new System.Drawing.Size(127, 17);
            permanent_AddLabel.TabIndex = 57;
            permanent_AddLabel.Text = "Permanent Address:";
            // 
            // pin2Label
            // 
            pin2Label.AutoSize = true;
            pin2Label.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            pin2Label.Location = new System.Drawing.Point(415, 418);
            pin2Label.Name = "pin2Label";
            pin2Label.Size = new System.Drawing.Size(37, 17);
            pin2Label.TabIndex = 59;
            pin2Label.Text = "Pin2:";
            // 
            // tele_Fax2Label
            // 
            tele_Fax2Label.AutoSize = true;
            tele_Fax2Label.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            tele_Fax2Label.Location = new System.Drawing.Point(383, 447);
            tele_Fax2Label.Name = "tele_Fax2Label";
            tele_Fax2Label.Size = new System.Drawing.Size(69, 17);
            tele_Fax2Label.TabIndex = 61;
            tele_Fax2Label.Text = "Tele Fax2:";
            // 
            // marital_StatusLabel
            // 
            marital_StatusLabel.AutoSize = true;
            marital_StatusLabel.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            marital_StatusLabel.Location = new System.Drawing.Point(61, 253);
            marital_StatusLabel.Name = "marital_StatusLabel";
            marital_StatusLabel.Size = new System.Drawing.Size(93, 17);
            marital_StatusLabel.TabIndex = 75;
            marital_StatusLabel.Text = "Marital Status:";
            // 
            // stateLabel
            // 
            stateLabel.AutoSize = true;
            stateLabel.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            stateLabel.Location = new System.Drawing.Point(114, 308);
            stateLabel.Name = "stateLabel";
            stateLabel.Size = new System.Drawing.Size(41, 17);
            stateLabel.TabIndex = 77;
            stateLabel.Text = "State:";
            // 
            // nationalityLabel
            // 
            nationalityLabel.AutoSize = true;
            nationalityLabel.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            nationalityLabel.Location = new System.Drawing.Point(81, 337);
            nationalityLabel.Name = "nationalityLabel";
            nationalityLabel.Size = new System.Drawing.Size(74, 17);
            nationalityLabel.TabIndex = 79;
            nationalityLabel.Text = "Nationality:";
            // 
            // visual_id_marksLabel
            // 
            visual_id_marksLabel.AutoSize = true;
            visual_id_marksLabel.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            visual_id_marksLabel.Location = new System.Drawing.Point(53, 477);
            visual_id_marksLabel.Name = "visual_id_marksLabel";
            visual_id_marksLabel.Size = new System.Drawing.Size(101, 17);
            visual_id_marksLabel.TabIndex = 81;
            visual_id_marksLabel.Text = "Visual id marks:";
            // 
            // pP_Size_ImageLabel
            // 
            pP_Size_ImageLabel.AutoSize = true;
            pP_Size_ImageLabel.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            pP_Size_ImageLabel.Location = new System.Drawing.Point(391, 249);
            pP_Size_ImageLabel.Name = "pP_Size_ImageLabel";
            pP_Size_ImageLabel.Size = new System.Drawing.Size(53, 34);
            pP_Size_ImageLabel.TabIndex = 83;
            pP_Size_ImageLabel.Text = "PP Size\r\nImage:";
            // 
            // training_MasterBindingNavigator
            // 
            this.training_MasterBindingNavigator.AddNewItem = this.bindingNavigatorAddNewItem;
            this.training_MasterBindingNavigator.BindingSource = this.training_MasterBindingSource;
            this.training_MasterBindingNavigator.CountItem = this.bindingNavigatorCountItem;
            this.training_MasterBindingNavigator.DeleteItem = null;
            this.training_MasterBindingNavigator.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.bindingNavigatorMoveFirstItem,
            this.bindingNavigatorMovePreviousItem,
            this.bindingNavigatorSeparator,
            this.bindingNavigatorPositionItem,
            this.bindingNavigatorCountItem,
            this.bindingNavigatorSeparator1,
            this.bindingNavigatorMoveNextItem,
            this.bindingNavigatorMoveLastItem,
            this.bindingNavigatorSeparator2,
            this.bindingNavigatorAddNewItem,
            this.toolStripButton1,
            this.toolStripButton2,
            this.training_MasterBindingNavigatorSaveItem});
            this.training_MasterBindingNavigator.Location = new System.Drawing.Point(0, 0);
            this.training_MasterBindingNavigator.MoveFirstItem = this.bindingNavigatorMoveFirstItem;
            this.training_MasterBindingNavigator.MoveLastItem = this.bindingNavigatorMoveLastItem;
            this.training_MasterBindingNavigator.MoveNextItem = this.bindingNavigatorMoveNextItem;
            this.training_MasterBindingNavigator.MovePreviousItem = this.bindingNavigatorMovePreviousItem;
            this.training_MasterBindingNavigator.Name = "training_MasterBindingNavigator";
            this.training_MasterBindingNavigator.PositionItem = this.bindingNavigatorPositionItem;
            this.training_MasterBindingNavigator.Size = new System.Drawing.Size(684, 25);
            this.training_MasterBindingNavigator.TabIndex = 0;
            this.training_MasterBindingNavigator.Text = "bindingNavigator1";
            // 
            // bindingNavigatorAddNewItem
            // 
            this.bindingNavigatorAddNewItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorAddNewItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorAddNewItem.Image")));
            this.bindingNavigatorAddNewItem.Name = "bindingNavigatorAddNewItem";
            this.bindingNavigatorAddNewItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorAddNewItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorAddNewItem.Text = "Add new data";
            // 
            // training_MasterBindingSource
            // 
            this.training_MasterBindingSource.DataMember = "Training_Master";
            this.training_MasterBindingSource.DataSource = this.dataSet1;
            this.training_MasterBindingSource.PositionChanged += new System.EventHandler(this.training_MasterBindingSource_PositionChanged);
            // 
            // dataSet1
            // 
            this.dataSet1.DataSetName = "DataSet1";
            this.dataSet1.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // bindingNavigatorCountItem
            // 
            this.bindingNavigatorCountItem.Name = "bindingNavigatorCountItem";
            this.bindingNavigatorCountItem.Size = new System.Drawing.Size(35, 22);
            this.bindingNavigatorCountItem.Text = "of {0}";
            this.bindingNavigatorCountItem.ToolTipText = "Total number of items";
            // 
            // bindingNavigatorMoveFirstItem
            // 
            this.bindingNavigatorMoveFirstItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMoveFirstItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveFirstItem.Image")));
            this.bindingNavigatorMoveFirstItem.Name = "bindingNavigatorMoveFirstItem";
            this.bindingNavigatorMoveFirstItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMoveFirstItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorMoveFirstItem.Text = "Move first";
            // 
            // bindingNavigatorMovePreviousItem
            // 
            this.bindingNavigatorMovePreviousItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMovePreviousItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMovePreviousItem.Image")));
            this.bindingNavigatorMovePreviousItem.Name = "bindingNavigatorMovePreviousItem";
            this.bindingNavigatorMovePreviousItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMovePreviousItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorMovePreviousItem.Text = "Move previous";
            // 
            // bindingNavigatorSeparator
            // 
            this.bindingNavigatorSeparator.Name = "bindingNavigatorSeparator";
            this.bindingNavigatorSeparator.Size = new System.Drawing.Size(6, 25);
            // 
            // bindingNavigatorPositionItem
            // 
            this.bindingNavigatorPositionItem.AccessibleName = "Position";
            this.bindingNavigatorPositionItem.AutoSize = false;
            this.bindingNavigatorPositionItem.Name = "bindingNavigatorPositionItem";
            this.bindingNavigatorPositionItem.Size = new System.Drawing.Size(50, 23);
            this.bindingNavigatorPositionItem.Text = "0";
            this.bindingNavigatorPositionItem.ToolTipText = "Current position";
            // 
            // bindingNavigatorSeparator1
            // 
            this.bindingNavigatorSeparator1.Name = "bindingNavigatorSeparator1";
            this.bindingNavigatorSeparator1.Size = new System.Drawing.Size(6, 25);
            // 
            // bindingNavigatorMoveNextItem
            // 
            this.bindingNavigatorMoveNextItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMoveNextItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveNextItem.Image")));
            this.bindingNavigatorMoveNextItem.Name = "bindingNavigatorMoveNextItem";
            this.bindingNavigatorMoveNextItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMoveNextItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorMoveNextItem.Text = "Move next";
            // 
            // bindingNavigatorMoveLastItem
            // 
            this.bindingNavigatorMoveLastItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMoveLastItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveLastItem.Image")));
            this.bindingNavigatorMoveLastItem.Name = "bindingNavigatorMoveLastItem";
            this.bindingNavigatorMoveLastItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMoveLastItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorMoveLastItem.Text = "Move last";
            // 
            // bindingNavigatorSeparator2
            // 
            this.bindingNavigatorSeparator2.Name = "bindingNavigatorSeparator2";
            this.bindingNavigatorSeparator2.Size = new System.Drawing.Size(6, 25);
            // 
            // toolStripButton1
            // 
            this.toolStripButton1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton1.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton1.Image")));
            this.toolStripButton1.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton1.Name = "toolStripButton1";
            this.toolStripButton1.Size = new System.Drawing.Size(23, 22);
            this.toolStripButton1.Text = "Delete";
            this.toolStripButton1.Click += new System.EventHandler(this.toolStripButton1_Click);
            // 
            // toolStripButton2
            // 
            this.toolStripButton2.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton2.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton2.Image")));
            this.toolStripButton2.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton2.Name = "toolStripButton2";
            this.toolStripButton2.Size = new System.Drawing.Size(23, 22);
            this.toolStripButton2.Text = "Update";
            this.toolStripButton2.Click += new System.EventHandler(this.toolStripButton2_Click);
            // 
            // training_MasterBindingNavigatorSaveItem
            // 
            this.training_MasterBindingNavigatorSaveItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.training_MasterBindingNavigatorSaveItem.Image = ((System.Drawing.Image)(resources.GetObject("training_MasterBindingNavigatorSaveItem.Image")));
            this.training_MasterBindingNavigatorSaveItem.Name = "training_MasterBindingNavigatorSaveItem";
            this.training_MasterBindingNavigatorSaveItem.Size = new System.Drawing.Size(23, 22);
            this.training_MasterBindingNavigatorSaveItem.Text = "Save";
            this.training_MasterBindingNavigatorSaveItem.Click += new System.EventHandler(this.training_MasterBindingNavigatorSaveItem_Click);
            // 
            // training_MasterTableAdapter
            // 
            this.training_MasterTableAdapter.ClearBeforeFill = true;
            // 
            // button3
            // 
            this.button3.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button3.Location = new System.Drawing.Point(531, 42);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(75, 23);
            this.button3.TabIndex = 35;
            this.button3.Text = "Search";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // nameTextBox
            // 
            this.nameTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.training_MasterBindingSource, "Name", true));
            this.nameTextBox.Location = new System.Drawing.Point(173, 73);
            this.nameTextBox.Name = "nameTextBox";
            this.nameTextBox.Size = new System.Drawing.Size(212, 20);
            this.nameTextBox.TabIndex = 38;
            // 
            // surnameTextBox
            // 
            this.surnameTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.training_MasterBindingSource, "Surname", true));
            this.surnameTextBox.Location = new System.Drawing.Point(173, 102);
            this.surnameTextBox.Name = "surnameTextBox";
            this.surnameTextBox.Size = new System.Drawing.Size(212, 20);
            this.surnameTextBox.TabIndex = 40;
            // 
            // expanded_Full_NameTextBox
            // 
            this.expanded_Full_NameTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.training_MasterBindingSource, "Expanded_Full_Name", true));
            this.expanded_Full_NameTextBox.Location = new System.Drawing.Point(173, 131);
            this.expanded_Full_NameTextBox.Name = "expanded_Full_NameTextBox";
            this.expanded_Full_NameTextBox.Size = new System.Drawing.Size(212, 20);
            this.expanded_Full_NameTextBox.TabIndex = 42;
            // 
            // dOBDateTimePicker
            // 
            this.dOBDateTimePicker.DataBindings.Add(new System.Windows.Forms.Binding("Value", this.training_MasterBindingSource, "DOB", true));
            this.dOBDateTimePicker.Location = new System.Drawing.Point(173, 161);
            this.dOBDateTimePicker.Name = "dOBDateTimePicker";
            this.dOBDateTimePicker.Size = new System.Drawing.Size(212, 20);
            this.dOBDateTimePicker.TabIndex = 44;
            // 
            // fNameTextBox
            // 
            this.fNameTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.training_MasterBindingSource, "FName", true));
            this.fNameTextBox.Location = new System.Drawing.Point(173, 191);
            this.fNameTextBox.Name = "fNameTextBox";
            this.fNameTextBox.Size = new System.Drawing.Size(212, 20);
            this.fNameTextBox.TabIndex = 46;
            // 
            // fOccupTextBox
            // 
            this.fOccupTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.training_MasterBindingSource, "FOccup", true));
            this.fOccupTextBox.Location = new System.Drawing.Point(173, 221);
            this.fOccupTextBox.Name = "fOccupTextBox";
            this.fOccupTextBox.Size = new System.Drawing.Size(212, 20);
            this.fOccupTextBox.TabIndex = 48;
            // 
            // addressTextBox
            // 
            this.addressTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.training_MasterBindingSource, "Address", true));
            this.addressTextBox.Location = new System.Drawing.Point(172, 360);
            this.addressTextBox.Multiline = true;
            this.addressTextBox.Name = "addressTextBox";
            this.addressTextBox.Size = new System.Drawing.Size(212, 50);
            this.addressTextBox.TabIndex = 50;
            // 
            // pin1TextBox
            // 
            this.pin1TextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.training_MasterBindingSource, "Pin1", true));
            this.pin1TextBox.Location = new System.Drawing.Point(458, 360);
            this.pin1TextBox.Name = "pin1TextBox";
            this.pin1TextBox.Size = new System.Drawing.Size(191, 20);
            this.pin1TextBox.TabIndex = 52;
            // 
            // tele_FaxTextBox
            // 
            this.tele_FaxTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.training_MasterBindingSource, "Tele_Fax", true));
            this.tele_FaxTextBox.Location = new System.Drawing.Point(458, 389);
            this.tele_FaxTextBox.Name = "tele_FaxTextBox";
            this.tele_FaxTextBox.Size = new System.Drawing.Size(191, 20);
            this.tele_FaxTextBox.TabIndex = 54;
            // 
            // emailTextBox
            // 
            this.emailTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.training_MasterBindingSource, "Email", true));
            this.emailTextBox.Location = new System.Drawing.Point(172, 278);
            this.emailTextBox.Name = "emailTextBox";
            this.emailTextBox.Size = new System.Drawing.Size(212, 20);
            this.emailTextBox.TabIndex = 56;
            // 
            // permanent_AddTextBox
            // 
            this.permanent_AddTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.training_MasterBindingSource, "Permanent_Add", true));
            this.permanent_AddTextBox.Location = new System.Drawing.Point(172, 419);
            this.permanent_AddTextBox.Multiline = true;
            this.permanent_AddTextBox.Name = "permanent_AddTextBox";
            this.permanent_AddTextBox.Size = new System.Drawing.Size(212, 46);
            this.permanent_AddTextBox.TabIndex = 58;
            // 
            // pin2TextBox
            // 
            this.pin2TextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.training_MasterBindingSource, "Pin2", true));
            this.pin2TextBox.Location = new System.Drawing.Point(458, 417);
            this.pin2TextBox.Name = "pin2TextBox";
            this.pin2TextBox.Size = new System.Drawing.Size(191, 20);
            this.pin2TextBox.TabIndex = 60;
            // 
            // tele_Fax2TextBox
            // 
            this.tele_Fax2TextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.training_MasterBindingSource, "Tele_Fax2", true));
            this.tele_Fax2TextBox.Location = new System.Drawing.Point(458, 446);
            this.tele_Fax2TextBox.Name = "tele_Fax2TextBox";
            this.tele_Fax2TextBox.Size = new System.Drawing.Size(191, 20);
            this.tele_Fax2TextBox.TabIndex = 62;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Location = new System.Drawing.Point(478, 80);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(150, 132);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 73;
            this.pictureBox1.TabStop = false;
            // 
            // marital_StatusTextBox
            // 
            this.marital_StatusTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.training_MasterBindingSource, "Marital_Status", true));
            this.marital_StatusTextBox.Location = new System.Drawing.Point(173, 250);
            this.marital_StatusTextBox.Name = "marital_StatusTextBox";
            this.marital_StatusTextBox.Size = new System.Drawing.Size(212, 20);
            this.marital_StatusTextBox.TabIndex = 76;
            // 
            // stateTextBox
            // 
            this.stateTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.training_MasterBindingSource, "State", true));
            this.stateTextBox.Location = new System.Drawing.Point(172, 308);
            this.stateTextBox.Name = "stateTextBox";
            this.stateTextBox.Size = new System.Drawing.Size(213, 20);
            this.stateTextBox.TabIndex = 78;
            // 
            // nationalityTextBox
            // 
            this.nationalityTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.training_MasterBindingSource, "Nationality", true));
            this.nationalityTextBox.Location = new System.Drawing.Point(172, 334);
            this.nationalityTextBox.Name = "nationalityTextBox";
            this.nationalityTextBox.Size = new System.Drawing.Size(213, 20);
            this.nationalityTextBox.TabIndex = 80;
            // 
            // visual_id_marksTextBox
            // 
            this.visual_id_marksTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.training_MasterBindingSource, "Visual_id_marks", true));
            this.visual_id_marksTextBox.Location = new System.Drawing.Point(173, 474);
            this.visual_id_marksTextBox.Name = "visual_id_marksTextBox";
            this.visual_id_marksTextBox.Size = new System.Drawing.Size(211, 20);
            this.visual_id_marksTextBox.TabIndex = 82;
            // 
            // pP_Size_ImageTextBox
            // 
            this.pP_Size_ImageTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.training_MasterBindingSource, "PP_Size_Image", true));
            this.pP_Size_ImageTextBox.Location = new System.Drawing.Point(458, 248);
            this.pP_Size_ImageTextBox.Name = "pP_Size_ImageTextBox";
            this.pP_Size_ImageTextBox.Size = new System.Drawing.Size(191, 20);
            this.pP_Size_ImageTextBox.TabIndex = 84;
            // 
            // button1
            // 
            this.button1.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.Location = new System.Drawing.Point(574, 276);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 74;
            this.button1.Text = "Browse";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click_1);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Times New Roman", 15.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(236, 25);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(238, 24);
            this.label1.TabIndex = 85;
            this.label1.Text = "UES Applicants Database";
            // 
            // Training_Master
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(684, 512);
            this.Controls.Add(this.label1);
            this.Controls.Add(marital_StatusLabel);
            this.Controls.Add(this.marital_StatusTextBox);
            this.Controls.Add(stateLabel);
            this.Controls.Add(this.stateTextBox);
            this.Controls.Add(nationalityLabel);
            this.Controls.Add(this.nationalityTextBox);
            this.Controls.Add(visual_id_marksLabel);
            this.Controls.Add(this.visual_id_marksTextBox);
            this.Controls.Add(pP_Size_ImageLabel);
            this.Controls.Add(this.pP_Size_ImageTextBox);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(nameLabel);
            this.Controls.Add(this.nameTextBox);
            this.Controls.Add(surnameLabel);
            this.Controls.Add(this.surnameTextBox);
            this.Controls.Add(expanded_Full_NameLabel);
            this.Controls.Add(this.expanded_Full_NameTextBox);
            this.Controls.Add(dOBLabel);
            this.Controls.Add(this.dOBDateTimePicker);
            this.Controls.Add(fNameLabel);
            this.Controls.Add(this.fNameTextBox);
            this.Controls.Add(fOccupLabel);
            this.Controls.Add(this.fOccupTextBox);
            this.Controls.Add(addressLabel);
            this.Controls.Add(this.addressTextBox);
            this.Controls.Add(pin1Label);
            this.Controls.Add(this.pin1TextBox);
            this.Controls.Add(tele_FaxLabel);
            this.Controls.Add(this.tele_FaxTextBox);
            this.Controls.Add(emailLabel);
            this.Controls.Add(this.emailTextBox);
            this.Controls.Add(permanent_AddLabel);
            this.Controls.Add(this.permanent_AddTextBox);
            this.Controls.Add(pin2Label);
            this.Controls.Add(this.pin2TextBox);
            this.Controls.Add(tele_Fax2Label);
            this.Controls.Add(this.tele_Fax2TextBox);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.training_MasterBindingNavigator);
            this.Name = "Training_Master";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "UES_Applicants_Database";
            this.Load += new System.EventHandler(this.Training_Master_Load);
            ((System.ComponentModel.ISupportInitialize)(this.training_MasterBindingNavigator)).EndInit();
            this.training_MasterBindingNavigator.ResumeLayout(false);
            this.training_MasterBindingNavigator.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.training_MasterBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DataSet1 dataSet1;
        private System.Windows.Forms.BindingSource training_MasterBindingSource;
        private Military_Project.DataSet1TableAdapters.Training_MasterTableAdapter training_MasterTableAdapter;
        private System.Windows.Forms.BindingNavigator training_MasterBindingNavigator;
        private System.Windows.Forms.ToolStripButton bindingNavigatorAddNewItem;
        private System.Windows.Forms.ToolStripLabel bindingNavigatorCountItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMoveFirstItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMovePreviousItem;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator;
        private System.Windows.Forms.ToolStripTextBox bindingNavigatorPositionItem;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator1;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMoveNextItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMoveLastItem;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator2;
        private System.Windows.Forms.ToolStripButton training_MasterBindingNavigatorSaveItem;
        private System.Windows.Forms.ToolStripButton toolStripButton1;
        private System.Windows.Forms.ToolStripButton toolStripButton2;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.TextBox nameTextBox;
        private System.Windows.Forms.TextBox surnameTextBox;
        private System.Windows.Forms.TextBox expanded_Full_NameTextBox;
        private System.Windows.Forms.DateTimePicker dOBDateTimePicker;
        private System.Windows.Forms.TextBox fNameTextBox;
        private System.Windows.Forms.TextBox fOccupTextBox;
        private System.Windows.Forms.TextBox addressTextBox;
        private System.Windows.Forms.TextBox pin1TextBox;
        private System.Windows.Forms.TextBox tele_FaxTextBox;
        private System.Windows.Forms.TextBox emailTextBox;
        private System.Windows.Forms.TextBox permanent_AddTextBox;
        private System.Windows.Forms.TextBox pin2TextBox;
        private System.Windows.Forms.TextBox tele_Fax2TextBox;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.TextBox marital_StatusTextBox;
        private System.Windows.Forms.TextBox stateTextBox;
        private System.Windows.Forms.TextBox nationalityTextBox;
        private System.Windows.Forms.TextBox visual_id_marksTextBox;
        private System.Windows.Forms.TextBox pP_Size_ImageTextBox;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label label1;
    }
}