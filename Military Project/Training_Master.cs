using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Military_Project
{
    public partial class Training_Master : Form
    {
        public Training_Master()
        {
            InitializeComponent();
        }
        DataSet1TableAdapters.Training_MasterTableAdapter a1 = new Military_Project.DataSet1TableAdapters.Training_MasterTableAdapter();
        private void clearfn()
        {
            nameTextBox.Clear();
            expanded_Full_NameTextBox.Clear();
            surnameTextBox.Clear();
            fNameTextBox.Clear();
            fOccupTextBox.Clear();
            marital_StatusTextBox.Clear();
            emailTextBox.Clear();
            nationalityTextBox.Clear();
            addressTextBox.Clear();
            permanent_AddTextBox.Clear();
            pin1TextBox.Clear();
            pin2TextBox.Clear();
            tele_Fax2TextBox.Clear();
            tele_FaxTextBox.Clear();
            visual_id_marksTextBox.Clear();
        }
        private void training_MasterBindingNavigatorSaveItem_Click(object sender, EventArgs e)
        {
            if (nameTextBox.Text.Length != 0 && surnameTextBox.Text.Length != 0 && expanded_Full_NameTextBox.Text.Length != 0 && fNameTextBox.Text.Length != 0 && fOccupTextBox.Text.Length != 0 && addressTextBox.Text.Length != 0 && pin1TextBox.Text.Length != 0 && tele_FaxTextBox.Text.Length != 0 && emailTextBox.Text.Length != 0 && permanent_AddTextBox.Text.Length != 0 && pin2TextBox.Text.Length != 0 && tele_Fax2TextBox.Text.Length != 0 && marital_StatusTextBox.Text.Length != 0 && stateTextBox.Text.Length != 0 && nationalityTextBox.Text.Length != 0 && visual_id_marksTextBox.Text.Length != 0 && pP_Size_ImageTextBox.Text.Length != 0)
            {
                a1.insert_training(nameTextBox.Text, surnameTextBox.Text, expanded_Full_NameTextBox.Text, dOBDateTimePicker.Value, fNameTextBox.Text, fOccupTextBox.Text, addressTextBox.Text, pin1TextBox.Text, tele_FaxTextBox.Text, emailTextBox.Text, permanent_AddTextBox.Text, pin2TextBox.Text, tele_Fax2TextBox.Text, marital_StatusTextBox.Text, stateTextBox.Text, nationalityTextBox.Text, visual_id_marksTextBox.Text, pP_Size_ImageTextBox.Text);
                MessageBox.Show("Record Inserted Successfully !");

            }
            else
            {
                MessageBox.Show("Please enter all values", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        private void Training_Master_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'dataSet1.Training_Master' table. You can move, or remove it, as needed.
            this.training_MasterTableAdapter.Fill(this.dataSet1.Training_Master);
            //clearfn();
        }

        private void toolStripButton1_Click(object sender, EventArgs e)
        {
            a1.delete_training(nameTextBox.Text, dOBDateTimePicker.Value);
            MessageBox.Show("Record Deleted Successfully !");
        }

        private void toolStripButton2_Click(object sender, EventArgs e)
        {
            a1.update_training(nameTextBox.Text, surnameTextBox.Text, expanded_Full_NameTextBox.Text, dOBDateTimePicker.Value, fNameTextBox.Text, fOccupTextBox.Text, addressTextBox.Text, pin1TextBox.Text, tele_FaxTextBox.Text, emailTextBox.Text, permanent_AddTextBox.Text, pin2TextBox.Text, tele_Fax2TextBox.Text, marital_StatusTextBox.Text, stateTextBox.Text, nationalityTextBox.Text, visual_id_marksTextBox.Text, pP_Size_ImageTextBox.Text);
            MessageBox.Show("Record Updated Successfully !");
        }

        private void button3_Click(object sender, EventArgs e)
        {
            TSearch T1 = new TSearch();
            T1.ShowDialog();

        }

        private void button1_Click(object sender, EventArgs e)
        {
            using (OpenFileDialog dlg = new OpenFileDialog())
            {
                dlg.Title = "open.image";
                dlg.Filter = "All files|*.*";
                if (dlg.ShowDialog() == DialogResult.OK)
                {
                    PictureBox pic1 = new PictureBox();
                    pP_Size_ImageTextBox.Text = (dlg.FileName).ToString();

                }
            }

        }

        private void training_MasterBindingSource_PositionChanged(object sender, EventArgs e)
        {
            if (pP_Size_ImageTextBox.Text == "")
            {


            }
            else
            {
                pictureBox1.Image = Image.FromFile(pP_Size_ImageTextBox.Text);
            }
        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            using (OpenFileDialog dlg = new OpenFileDialog())
            {
                dlg.Title = "open.image";
                dlg.Filter = "All files|*.*";
                if (dlg.ShowDialog() == DialogResult.OK)
                {
                    PictureBox pic1 = new PictureBox();
                    pP_Size_ImageTextBox.Text = (dlg.FileName).ToString();

                }
            }
        }
    }
}