using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Military_Project
{
    public partial class UES_Application : Form
    {
        public UES_Application()
        {
            InitializeComponent();
        }
        DataSet1TableAdapters.Training_MasterTableAdapter AS = new Military_Project.DataSet1TableAdapters.Training_MasterTableAdapter();
        private void training_MasterBindingNavigatorSaveItem_Click(object sender, EventArgs e)
        {
            this.Validate();
            this.training_MasterBindingSource.EndEdit();
            this.training_MasterTableAdapter.Update(this.dataSet1.Training_Master);

        }

        private void UES_Application_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'dataSet1.Training_Master' table. You can move, or remove it, as needed.
            this.training_MasterTableAdapter.Fill(this.dataSet1.Training_Master);
            nameComboBox.Text = "";
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void nameComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            DataTable dt = new DataTable();
            dt = AS.search_training(nameComboBox.Text, dOBDateTimePicker.Value);
            if (dt.Rows.Count > 0)
            {
                training_MasterDataGridView.DataSource = dt;
            }
        }

        private void dOBDateTimePicker_ValueChanged(object sender, EventArgs e)
        {
            DataTable dt = new DataTable();
            dt = AS.search_training(nameComboBox.Text, dOBDateTimePicker.Value);
            if (dt.Rows.Count > 0)
            {
               training_MasterDataGridView.DataSource = dt;
            }
        }
        public static string ss;
        public static DateTime dst;
        private void button1_Click(object sender, EventArgs e)
        {

            ss = this.nameComboBox.Text;
            dst = this.dOBDateTimePicker.Value;  
            Training tr = new Training();
            tr.Show();
        }
    }
}