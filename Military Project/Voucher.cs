using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Military_Project
{
    public partial class Voucher : Form
    {
        public Voucher()
        {
            InitializeComponent();
        }

        private void Voucher_Load(object sender, EventArgs e)
        {
            CrystalReport2 cr2 = new CrystalReport2();
            //cr2.Load("D:/c # .net complete/Military Project/Military Project/Military Project/CrystalReport2.rpt");
            cr2.Load("D:/new d drive/pendrive project/Military Project/Military Project/CrystalReport2.rpt");
            DataSet1TableAdapters.Voucher_MasterTableAdapter c2 = new Military_Project.DataSet1TableAdapters.Voucher_MasterTableAdapter();
            DataTable dt = new DataTable();
            Payment_Receipt pr = new Payment_Receipt();
            dt = c2.search_voucher(Payment_Receipt.vn);
            cr2.SetDataSource(dt);
            cr2.Refresh();
            crystalReportViewer1.ReportSource = cr2;
        }

        private void crystalReportViewer1_Load(object sender, EventArgs e)
        {

        }
    }
}