namespace Military_Project
{
    partial class Voucher_Master
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.Label voucher_NoLabel;
            System.Windows.Forms.Label vDateLabel;
            System.Windows.Forms.Label cash_Cheque_NoLabel;
            System.Windows.Forms.Label date2Label;
            System.Windows.Forms.Label recieved_FromLabel;
            System.Windows.Forms.Label sum1Label;
            System.Windows.Forms.Label witnessLabel;
            System.Windows.Forms.Label account_HeadLabel1;
            System.Windows.Forms.Label emp_NameLabel;
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Voucher_Master));
            this.dataSet1 = new Military_Project.DataSet1();
            this.voucher_MasterBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.voucher_MasterTableAdapter = new Military_Project.DataSet1TableAdapters.Voucher_MasterTableAdapter();
            this.voucher_MasterBindingNavigator = new System.Windows.Forms.BindingNavigator(this.components);
            this.bindingNavigatorAddNewItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorCountItem = new System.Windows.Forms.ToolStripLabel();
            this.bindingNavigatorMoveFirstItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorMovePreviousItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorSeparator = new System.Windows.Forms.ToolStripSeparator();
            this.bindingNavigatorPositionItem = new System.Windows.Forms.ToolStripTextBox();
            this.bindingNavigatorSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.bindingNavigatorMoveNextItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorMoveLastItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripButton1 = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton2 = new System.Windows.Forms.ToolStripButton();
            this.voucher_MasterBindingNavigatorSaveItem = new System.Windows.Forms.ToolStripButton();
            this.voucher_NoTextBox = new System.Windows.Forms.TextBox();
            this.vDateDateTimePicker = new System.Windows.Forms.DateTimePicker();
            this.cash_Cheque_NoTextBox = new System.Windows.Forms.TextBox();
            this.date2DateTimePicker = new System.Windows.Forms.DateTimePicker();
            this.recieved_FromTextBox = new System.Windows.Forms.TextBox();
            this.sum1TextBox = new System.Windows.Forms.TextBox();
            this.witnessTextBox = new System.Windows.Forms.TextBox();
            this.button1 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.account_HeadComboBox = new System.Windows.Forms.ComboBox();
            this.accountMasterBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.emp_NameComboBox = new System.Windows.Forms.ComboBox();
            this.employeeMasterBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.account_MasterTableAdapter = new Military_Project.DataSet1TableAdapters.Account_MasterTableAdapter();
            this.employee_MasterTableAdapter = new Military_Project.DataSet1TableAdapters.Employee_MasterTableAdapter();
            this.label1 = new System.Windows.Forms.Label();
            voucher_NoLabel = new System.Windows.Forms.Label();
            vDateLabel = new System.Windows.Forms.Label();
            cash_Cheque_NoLabel = new System.Windows.Forms.Label();
            date2Label = new System.Windows.Forms.Label();
            recieved_FromLabel = new System.Windows.Forms.Label();
            sum1Label = new System.Windows.Forms.Label();
            witnessLabel = new System.Windows.Forms.Label();
            account_HeadLabel1 = new System.Windows.Forms.Label();
            emp_NameLabel = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.voucher_MasterBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.voucher_MasterBindingNavigator)).BeginInit();
            this.voucher_MasterBindingNavigator.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.accountMasterBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.employeeMasterBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // voucher_NoLabel
            // 
            voucher_NoLabel.AutoSize = true;
            voucher_NoLabel.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            voucher_NoLabel.Location = new System.Drawing.Point(148, 121);
            voucher_NoLabel.Name = "voucher_NoLabel";
            voucher_NoLabel.Size = new System.Drawing.Size(82, 17);
            voucher_NoLabel.TabIndex = 1;
            voucher_NoLabel.Text = "Voucher No:";
            // 
            // vDateLabel
            // 
            vDateLabel.AutoSize = true;
            vDateLabel.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            vDateLabel.Location = new System.Drawing.Point(137, 157);
            vDateLabel.Name = "vDateLabel";
            vDateLabel.Size = new System.Drawing.Size(93, 17);
            vDateLabel.TabIndex = 3;
            vDateLabel.Text = "Voucher Date:";
            // 
            // cash_Cheque_NoLabel
            // 
            cash_Cheque_NoLabel.AutoSize = true;
            cash_Cheque_NoLabel.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            cash_Cheque_NoLabel.Location = new System.Drawing.Point(118, 228);
            cash_Cheque_NoLabel.Name = "cash_Cheque_NoLabel";
            cash_Cheque_NoLabel.Size = new System.Drawing.Size(112, 17);
            cash_Cheque_NoLabel.TabIndex = 7;
            cash_Cheque_NoLabel.Text = "Cash/Cheque No:";
            // 
            // date2Label
            // 
            date2Label.AutoSize = true;
            date2Label.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            date2Label.Location = new System.Drawing.Point(183, 266);
            date2Label.Name = "date2Label";
            date2Label.Size = new System.Drawing.Size(47, 17);
            date2Label.TabIndex = 9;
            date2Label.Text = "Date2:";
            // 
            // recieved_FromLabel
            // 
            recieved_FromLabel.AutoSize = true;
            recieved_FromLabel.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            recieved_FromLabel.Location = new System.Drawing.Point(129, 338);
            recieved_FromLabel.Name = "recieved_FromLabel";
            recieved_FromLabel.Size = new System.Drawing.Size(101, 17);
            recieved_FromLabel.TabIndex = 13;
            recieved_FromLabel.Text = "Recieved From:";
            // 
            // sum1Label
            // 
            sum1Label.AutoSize = true;
            sum1Label.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            sum1Label.Location = new System.Drawing.Point(186, 375);
            sum1Label.Name = "sum1Label";
            sum1Label.Size = new System.Drawing.Size(44, 17);
            sum1Label.TabIndex = 15;
            sum1Label.Text = "Sum1:";
            // 
            // witnessLabel
            // 
            witnessLabel.AutoSize = true;
            witnessLabel.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            witnessLabel.Location = new System.Drawing.Point(173, 413);
            witnessLabel.Name = "witnessLabel";
            witnessLabel.Size = new System.Drawing.Size(57, 17);
            witnessLabel.TabIndex = 17;
            witnessLabel.Text = "Witness:";
            // 
            // account_HeadLabel1
            // 
            account_HeadLabel1.AutoSize = true;
            account_HeadLabel1.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            account_HeadLabel1.Location = new System.Drawing.Point(133, 194);
            account_HeadLabel1.Name = "account_HeadLabel1";
            account_HeadLabel1.Size = new System.Drawing.Size(97, 17);
            account_HeadLabel1.TabIndex = 20;
            account_HeadLabel1.Text = "Account Head:";
            // 
            // emp_NameLabel
            // 
            emp_NameLabel.AutoSize = true;
            emp_NameLabel.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            emp_NameLabel.Location = new System.Drawing.Point(152, 303);
            emp_NameLabel.Name = "emp_NameLabel";
            emp_NameLabel.Size = new System.Drawing.Size(78, 17);
            emp_NameLabel.TabIndex = 21;
            emp_NameLabel.Text = "Emp Name:";
            emp_NameLabel.Click += new System.EventHandler(this.emp_NameLabel_Click);
            // 
            // dataSet1
            // 
            this.dataSet1.DataSetName = "DataSet1";
            this.dataSet1.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // voucher_MasterBindingSource
            // 
            this.voucher_MasterBindingSource.DataMember = "Voucher_Master";
            this.voucher_MasterBindingSource.DataSource = this.dataSet1;
            // 
            // voucher_MasterTableAdapter
            // 
            this.voucher_MasterTableAdapter.ClearBeforeFill = true;
            // 
            // voucher_MasterBindingNavigator
            // 
            this.voucher_MasterBindingNavigator.AddNewItem = this.bindingNavigatorAddNewItem;
            this.voucher_MasterBindingNavigator.BindingSource = this.voucher_MasterBindingSource;
            this.voucher_MasterBindingNavigator.CountItem = this.bindingNavigatorCountItem;
            this.voucher_MasterBindingNavigator.DeleteItem = null;
            this.voucher_MasterBindingNavigator.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.bindingNavigatorMoveFirstItem,
            this.bindingNavigatorMovePreviousItem,
            this.bindingNavigatorSeparator,
            this.bindingNavigatorPositionItem,
            this.bindingNavigatorCountItem,
            this.bindingNavigatorSeparator1,
            this.bindingNavigatorMoveNextItem,
            this.bindingNavigatorMoveLastItem,
            this.bindingNavigatorSeparator2,
            this.bindingNavigatorAddNewItem,
            this.toolStripButton1,
            this.toolStripButton2,
            this.voucher_MasterBindingNavigatorSaveItem});
            this.voucher_MasterBindingNavigator.Location = new System.Drawing.Point(0, 0);
            this.voucher_MasterBindingNavigator.MoveFirstItem = this.bindingNavigatorMoveFirstItem;
            this.voucher_MasterBindingNavigator.MoveLastItem = this.bindingNavigatorMoveLastItem;
            this.voucher_MasterBindingNavigator.MoveNextItem = this.bindingNavigatorMoveNextItem;
            this.voucher_MasterBindingNavigator.MovePreviousItem = this.bindingNavigatorMovePreviousItem;
            this.voucher_MasterBindingNavigator.Name = "voucher_MasterBindingNavigator";
            this.voucher_MasterBindingNavigator.PositionItem = this.bindingNavigatorPositionItem;
            this.voucher_MasterBindingNavigator.Size = new System.Drawing.Size(684, 25);
            this.voucher_MasterBindingNavigator.TabIndex = 0;
            this.voucher_MasterBindingNavigator.Text = "bindingNavigator1";
            // 
            // bindingNavigatorAddNewItem
            // 
            this.bindingNavigatorAddNewItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorAddNewItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorAddNewItem.Image")));
            this.bindingNavigatorAddNewItem.Name = "bindingNavigatorAddNewItem";
            this.bindingNavigatorAddNewItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorAddNewItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorAddNewItem.Text = "Add new data";
            // 
            // bindingNavigatorCountItem
            // 
            this.bindingNavigatorCountItem.Name = "bindingNavigatorCountItem";
            this.bindingNavigatorCountItem.Size = new System.Drawing.Size(35, 22);
            this.bindingNavigatorCountItem.Text = "of {0}";
            this.bindingNavigatorCountItem.ToolTipText = "Total number of items";
            // 
            // bindingNavigatorMoveFirstItem
            // 
            this.bindingNavigatorMoveFirstItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMoveFirstItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveFirstItem.Image")));
            this.bindingNavigatorMoveFirstItem.Name = "bindingNavigatorMoveFirstItem";
            this.bindingNavigatorMoveFirstItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMoveFirstItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorMoveFirstItem.Text = "Move first";
            // 
            // bindingNavigatorMovePreviousItem
            // 
            this.bindingNavigatorMovePreviousItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMovePreviousItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMovePreviousItem.Image")));
            this.bindingNavigatorMovePreviousItem.Name = "bindingNavigatorMovePreviousItem";
            this.bindingNavigatorMovePreviousItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMovePreviousItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorMovePreviousItem.Text = "Move previous";
            // 
            // bindingNavigatorSeparator
            // 
            this.bindingNavigatorSeparator.Name = "bindingNavigatorSeparator";
            this.bindingNavigatorSeparator.Size = new System.Drawing.Size(6, 25);
            // 
            // bindingNavigatorPositionItem
            // 
            this.bindingNavigatorPositionItem.AccessibleName = "Position";
            this.bindingNavigatorPositionItem.AutoSize = false;
            this.bindingNavigatorPositionItem.Name = "bindingNavigatorPositionItem";
            this.bindingNavigatorPositionItem.Size = new System.Drawing.Size(50, 23);
            this.bindingNavigatorPositionItem.Text = "0";
            this.bindingNavigatorPositionItem.ToolTipText = "Current position";
            // 
            // bindingNavigatorSeparator1
            // 
            this.bindingNavigatorSeparator1.Name = "bindingNavigatorSeparator1";
            this.bindingNavigatorSeparator1.Size = new System.Drawing.Size(6, 25);
            // 
            // bindingNavigatorMoveNextItem
            // 
            this.bindingNavigatorMoveNextItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMoveNextItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveNextItem.Image")));
            this.bindingNavigatorMoveNextItem.Name = "bindingNavigatorMoveNextItem";
            this.bindingNavigatorMoveNextItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMoveNextItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorMoveNextItem.Text = "Move next";
            // 
            // bindingNavigatorMoveLastItem
            // 
            this.bindingNavigatorMoveLastItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMoveLastItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveLastItem.Image")));
            this.bindingNavigatorMoveLastItem.Name = "bindingNavigatorMoveLastItem";
            this.bindingNavigatorMoveLastItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMoveLastItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorMoveLastItem.Text = "Move last";
            // 
            // bindingNavigatorSeparator2
            // 
            this.bindingNavigatorSeparator2.Name = "bindingNavigatorSeparator2";
            this.bindingNavigatorSeparator2.Size = new System.Drawing.Size(6, 25);
            // 
            // toolStripButton1
            // 
            this.toolStripButton1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton1.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton1.Image")));
            this.toolStripButton1.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton1.Name = "toolStripButton1";
            this.toolStripButton1.Size = new System.Drawing.Size(23, 22);
            this.toolStripButton1.Text = "Delete";
            this.toolStripButton1.Click += new System.EventHandler(this.toolStripButton1_Click);
            // 
            // toolStripButton2
            // 
            this.toolStripButton2.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton2.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton2.Image")));
            this.toolStripButton2.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton2.Name = "toolStripButton2";
            this.toolStripButton2.Size = new System.Drawing.Size(23, 22);
            this.toolStripButton2.Text = "Update";
            this.toolStripButton2.Click += new System.EventHandler(this.toolStripButton2_Click);
            // 
            // voucher_MasterBindingNavigatorSaveItem
            // 
            this.voucher_MasterBindingNavigatorSaveItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.voucher_MasterBindingNavigatorSaveItem.Image = ((System.Drawing.Image)(resources.GetObject("voucher_MasterBindingNavigatorSaveItem.Image")));
            this.voucher_MasterBindingNavigatorSaveItem.Name = "voucher_MasterBindingNavigatorSaveItem";
            this.voucher_MasterBindingNavigatorSaveItem.Size = new System.Drawing.Size(23, 22);
            this.voucher_MasterBindingNavigatorSaveItem.Text = "Save";
            this.voucher_MasterBindingNavigatorSaveItem.Click += new System.EventHandler(this.voucher_MasterBindingNavigatorSaveItem_Click);
            // 
            // voucher_NoTextBox
            // 
            this.voucher_NoTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.voucher_MasterBindingSource, "Voucher_No", true));
            this.voucher_NoTextBox.Location = new System.Drawing.Point(245, 118);
            this.voucher_NoTextBox.Name = "voucher_NoTextBox";
            this.voucher_NoTextBox.Size = new System.Drawing.Size(215, 20);
            this.voucher_NoTextBox.TabIndex = 2;
            // 
            // vDateDateTimePicker
            // 
            this.vDateDateTimePicker.DataBindings.Add(new System.Windows.Forms.Binding("Value", this.voucher_MasterBindingSource, "VDate", true));
            this.vDateDateTimePicker.Location = new System.Drawing.Point(245, 154);
            this.vDateDateTimePicker.Name = "vDateDateTimePicker";
            this.vDateDateTimePicker.Size = new System.Drawing.Size(215, 20);
            this.vDateDateTimePicker.TabIndex = 4;
            // 
            // cash_Cheque_NoTextBox
            // 
            this.cash_Cheque_NoTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.voucher_MasterBindingSource, "Cash_Cheque_No", true));
            this.cash_Cheque_NoTextBox.Location = new System.Drawing.Point(245, 227);
            this.cash_Cheque_NoTextBox.Name = "cash_Cheque_NoTextBox";
            this.cash_Cheque_NoTextBox.Size = new System.Drawing.Size(215, 20);
            this.cash_Cheque_NoTextBox.TabIndex = 8;
            // 
            // date2DateTimePicker
            // 
            this.date2DateTimePicker.DataBindings.Add(new System.Windows.Forms.Binding("Value", this.voucher_MasterBindingSource, "Date2", true));
            this.date2DateTimePicker.Location = new System.Drawing.Point(245, 263);
            this.date2DateTimePicker.Name = "date2DateTimePicker";
            this.date2DateTimePicker.Size = new System.Drawing.Size(215, 20);
            this.date2DateTimePicker.TabIndex = 10;
            // 
            // recieved_FromTextBox
            // 
            this.recieved_FromTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.voucher_MasterBindingSource, "Recieved_From", true));
            this.recieved_FromTextBox.Location = new System.Drawing.Point(245, 337);
            this.recieved_FromTextBox.Name = "recieved_FromTextBox";
            this.recieved_FromTextBox.Size = new System.Drawing.Size(215, 20);
            this.recieved_FromTextBox.TabIndex = 14;
            // 
            // sum1TextBox
            // 
            this.sum1TextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.voucher_MasterBindingSource, "Sum1", true));
            this.sum1TextBox.Location = new System.Drawing.Point(245, 374);
            this.sum1TextBox.Name = "sum1TextBox";
            this.sum1TextBox.Size = new System.Drawing.Size(215, 20);
            this.sum1TextBox.TabIndex = 16;
            // 
            // witnessTextBox
            // 
            this.witnessTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.voucher_MasterBindingSource, "Witness", true));
            this.witnessTextBox.Location = new System.Drawing.Point(245, 412);
            this.witnessTextBox.Multiline = true;
            this.witnessTextBox.Name = "witnessTextBox";
            this.witnessTextBox.Size = new System.Drawing.Size(215, 57);
            this.witnessTextBox.TabIndex = 18;
            // 
            // button1
            // 
            this.button1.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.Location = new System.Drawing.Point(466, 118);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(130, 23);
            this.button1.TabIndex = 19;
            this.button1.Text = "Check Availability?";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button3
            // 
            this.button3.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button3.Location = new System.Drawing.Point(552, 57);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(75, 23);
            this.button3.TabIndex = 20;
            this.button3.Text = "Search";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // account_HeadComboBox
            // 
            this.account_HeadComboBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.voucher_MasterBindingSource, "Account_Head", true));
            this.account_HeadComboBox.DataSource = this.accountMasterBindingSource;
            this.account_HeadComboBox.DisplayMember = "Acc_Head";
            this.account_HeadComboBox.FormattingEnabled = true;
            this.account_HeadComboBox.Location = new System.Drawing.Point(245, 190);
            this.account_HeadComboBox.Name = "account_HeadComboBox";
            this.account_HeadComboBox.Size = new System.Drawing.Size(215, 21);
            this.account_HeadComboBox.TabIndex = 21;
            // 
            // accountMasterBindingSource
            // 
            this.accountMasterBindingSource.DataMember = "Account_Master";
            this.accountMasterBindingSource.DataSource = this.dataSet1;
            // 
            // emp_NameComboBox
            // 
            this.emp_NameComboBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.voucher_MasterBindingSource, "Emp_Name", true));
            this.emp_NameComboBox.DataSource = this.employeeMasterBindingSource;
            this.emp_NameComboBox.DisplayMember = "Emp_Name";
            this.emp_NameComboBox.FormattingEnabled = true;
            this.emp_NameComboBox.Location = new System.Drawing.Point(245, 299);
            this.emp_NameComboBox.Name = "emp_NameComboBox";
            this.emp_NameComboBox.Size = new System.Drawing.Size(215, 21);
            this.emp_NameComboBox.TabIndex = 22;
            // 
            // employeeMasterBindingSource
            // 
            this.employeeMasterBindingSource.DataMember = "Employee_Master";
            this.employeeMasterBindingSource.DataSource = this.dataSet1;
            // 
            // account_MasterTableAdapter
            // 
            this.account_MasterTableAdapter.ClearBeforeFill = true;
            // 
            // employee_MasterTableAdapter
            // 
            this.employee_MasterTableAdapter.ClearBeforeFill = true;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Times New Roman", 15.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(259, 56);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(187, 24);
            this.label1.TabIndex = 23;
            this.label1.Text = "Payments Database";
            // 
            // Voucher_Master
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(684, 487);
            this.Controls.Add(this.label1);
            this.Controls.Add(emp_NameLabel);
            this.Controls.Add(this.emp_NameComboBox);
            this.Controls.Add(account_HeadLabel1);
            this.Controls.Add(this.account_HeadComboBox);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.button1);
            this.Controls.Add(voucher_NoLabel);
            this.Controls.Add(this.voucher_NoTextBox);
            this.Controls.Add(vDateLabel);
            this.Controls.Add(this.vDateDateTimePicker);
            this.Controls.Add(cash_Cheque_NoLabel);
            this.Controls.Add(this.cash_Cheque_NoTextBox);
            this.Controls.Add(date2Label);
            this.Controls.Add(this.date2DateTimePicker);
            this.Controls.Add(recieved_FromLabel);
            this.Controls.Add(this.recieved_FromTextBox);
            this.Controls.Add(sum1Label);
            this.Controls.Add(this.sum1TextBox);
            this.Controls.Add(witnessLabel);
            this.Controls.Add(this.witnessTextBox);
            this.Controls.Add(this.voucher_MasterBindingNavigator);
            this.Name = "Voucher_Master";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Payments_Database";
            this.Load += new System.EventHandler(this.Voucher_Master_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataSet1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.voucher_MasterBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.voucher_MasterBindingNavigator)).EndInit();
            this.voucher_MasterBindingNavigator.ResumeLayout(false);
            this.voucher_MasterBindingNavigator.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.accountMasterBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.employeeMasterBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DataSet1 dataSet1;
        private System.Windows.Forms.BindingSource voucher_MasterBindingSource;
        private Military_Project.DataSet1TableAdapters.Voucher_MasterTableAdapter voucher_MasterTableAdapter;
        private System.Windows.Forms.BindingNavigator voucher_MasterBindingNavigator;
        private System.Windows.Forms.ToolStripButton bindingNavigatorAddNewItem;
        private System.Windows.Forms.ToolStripLabel bindingNavigatorCountItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMoveFirstItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMovePreviousItem;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator;
        private System.Windows.Forms.ToolStripTextBox bindingNavigatorPositionItem;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator1;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMoveNextItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMoveLastItem;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator2;
        private System.Windows.Forms.ToolStripButton voucher_MasterBindingNavigatorSaveItem;
        private System.Windows.Forms.TextBox voucher_NoTextBox;
        private System.Windows.Forms.DateTimePicker vDateDateTimePicker;
        private System.Windows.Forms.TextBox cash_Cheque_NoTextBox;
        private System.Windows.Forms.DateTimePicker date2DateTimePicker;
        private System.Windows.Forms.TextBox recieved_FromTextBox;
        private System.Windows.Forms.TextBox sum1TextBox;
        private System.Windows.Forms.TextBox witnessTextBox;
        private System.Windows.Forms.ToolStripButton toolStripButton1;
        private System.Windows.Forms.ToolStripButton toolStripButton2;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.ComboBox account_HeadComboBox;
        private System.Windows.Forms.ComboBox emp_NameComboBox;
        private System.Windows.Forms.BindingSource accountMasterBindingSource;
        private Military_Project.DataSet1TableAdapters.Account_MasterTableAdapter account_MasterTableAdapter;
        private System.Windows.Forms.BindingSource employeeMasterBindingSource;
        private Military_Project.DataSet1TableAdapters.Employee_MasterTableAdapter employee_MasterTableAdapter;
        private System.Windows.Forms.Label label1;
    }
}