using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Military_Project
{
    public partial class Voucher_Master : Form
    {
        public Voucher_Master()
        {
            InitializeComponent();
        }
        DataSet1TableAdapters.Voucher_MasterTableAdapter a1 = new Military_Project.DataSet1TableAdapters.Voucher_MasterTableAdapter();
        private void clearfn()
        {
            voucher_NoTextBox.Clear();
            account_HeadComboBox.Text = "";
            cash_Cheque_NoTextBox.Clear();
            emp_NameComboBox.Text = "";
            recieved_FromTextBox.Clear();
            sum1TextBox.Clear();
            witnessTextBox.Clear();
        }
        private void voucher_MasterBindingNavigatorSaveItem_Click(object sender, EventArgs e)
        {
            if (voucher_NoTextBox.Text.Length != 0 && account_HeadComboBox.Text.Length != 0 && cash_Cheque_NoTextBox.Text.Length != 0 && emp_NameComboBox.Text.Length != 0 && recieved_FromTextBox.Text.Length != 0 && sum1TextBox.Text.Length != 0 && witnessTextBox.Text.Length != 0)
            {
                a1.insert_voucher(voucher_NoTextBox.Text, vDateDateTimePicker.Value, account_HeadComboBox.Text, cash_Cheque_NoTextBox.Text, date2DateTimePicker.Value, emp_NameComboBox.Text, recieved_FromTextBox.Text, sum1TextBox.Text, witnessTextBox.Text);
                MessageBox.Show("Record Inserted Successfully !");
            }
            else
            {
                MessageBox.Show("Please enter all values", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }

        }

        private void Voucher_Master_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'dataSet1.Employee_Master' table. You can move, or remove it, as needed.
            this.employee_MasterTableAdapter.Fill(this.dataSet1.Employee_Master);
            // TODO: This line of code loads data into the 'dataSet1.Account_Master' table. You can move, or remove it, as needed.
            this.account_MasterTableAdapter.Fill(this.dataSet1.Account_Master);
            // TODO: This line of code loads data into the 'dataSet1.Voucher_Master' table. You can move, or remove it, as needed.
            this.voucher_MasterTableAdapter.Fill(this.dataSet1.Voucher_Master);
            //clearfn();
        }

        private void toolStripButton1_Click(object sender, EventArgs e)
        {
            a1.delete_voucher(voucher_NoTextBox.Text);
            MessageBox.Show("Record Deleted Successfully !");
        }

        private void toolStripButton2_Click(object sender, EventArgs e)
        {
            a1.update_voucher(voucher_NoTextBox.Text, vDateDateTimePicker.Value, account_HeadComboBox.Text, cash_Cheque_NoTextBox.Text, date2DateTimePicker.Value, emp_NameComboBox.Text, recieved_FromTextBox.Text, sum1TextBox.Text, witnessTextBox.Text);
            MessageBox.Show("Record Updated Successfully !");
        }

        private void button3_Click(object sender, EventArgs e)
        {
            VSearch V1 = new VSearch();
            V1.ShowDialog();
            
        }

        private void button1_Click(object sender, EventArgs e)
        {
            DataTable dt = new DataTable();
            dt = a1.search_voucher(voucher_NoTextBox.Text);
            if (dt.Rows.Count > 0)
            {
                MessageBox.Show("Already Exists !");
                voucher_NoTextBox.Text = "";
                voucher_NoTextBox.Focus();
            }
            else
            {
                MessageBox.Show("Voucher_No Available !");
            }
        }

        private void emp_NameLabel_Click(object sender, EventArgs e)
        {

        }
    }
}