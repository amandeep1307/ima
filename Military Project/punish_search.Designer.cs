namespace Military_Project
{
    partial class punish_search
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.Label soldier_NameLabel;
            this.dataSet1 = new Military_Project.DataSet1();
            this.punishment_MasterBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.punishment_MasterTableAdapter = new Military_Project.DataSet1TableAdapters.Punishment_MasterTableAdapter();
            this.punishment_MasterDataGridView = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.soldier_NameComboBox = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            soldier_NameLabel = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.punishment_MasterBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.punishment_MasterDataGridView)).BeginInit();
            this.SuspendLayout();
            // 
            // soldier_NameLabel
            // 
            soldier_NameLabel.AutoSize = true;
            soldier_NameLabel.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            soldier_NameLabel.Location = new System.Drawing.Point(217, 169);
            soldier_NameLabel.Name = "soldier_NameLabel";
            soldier_NameLabel.Size = new System.Drawing.Size(91, 17);
            soldier_NameLabel.TabIndex = 3;
            soldier_NameLabel.Text = "Soldier Name:";
            // 
            // dataSet1
            // 
            this.dataSet1.DataSetName = "DataSet1";
            this.dataSet1.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // punishment_MasterBindingSource
            // 
            this.punishment_MasterBindingSource.DataMember = "Punishment_Master";
            this.punishment_MasterBindingSource.DataSource = this.dataSet1;
            // 
            // punishment_MasterTableAdapter
            // 
            this.punishment_MasterTableAdapter.ClearBeforeFill = true;
            // 
            // punishment_MasterDataGridView
            // 
            this.punishment_MasterDataGridView.AutoGenerateColumns = false;
            this.punishment_MasterDataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn1,
            this.dataGridViewTextBoxColumn2,
            this.dataGridViewTextBoxColumn3,
            this.dataGridViewTextBoxColumn4});
            this.punishment_MasterDataGridView.DataSource = this.punishment_MasterBindingSource;
            this.punishment_MasterDataGridView.Location = new System.Drawing.Point(119, 228);
            this.punishment_MasterDataGridView.Name = "punishment_MasterDataGridView";
            this.punishment_MasterDataGridView.Size = new System.Drawing.Size(443, 220);
            this.punishment_MasterDataGridView.TabIndex = 3;
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.DataPropertyName = "Punishment_Id";
            this.dataGridViewTextBoxColumn1.HeaderText = "Punishment_Id";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.DataPropertyName = "Soldier_Name";
            this.dataGridViewTextBoxColumn2.HeaderText = "Soldier_Name";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.DataPropertyName = "Type_Of_Punishment";
            this.dataGridViewTextBoxColumn3.HeaderText = "Type_Of_Punishment";
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            // 
            // dataGridViewTextBoxColumn4
            // 
            this.dataGridViewTextBoxColumn4.DataPropertyName = "Comments";
            this.dataGridViewTextBoxColumn4.HeaderText = "Comments";
            this.dataGridViewTextBoxColumn4.Name = "dataGridViewTextBoxColumn4";
            // 
            // soldier_NameComboBox
            // 
            this.soldier_NameComboBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.punishment_MasterBindingSource, "Soldier_Name", true));
            this.soldier_NameComboBox.DataSource = this.punishment_MasterBindingSource;
            this.soldier_NameComboBox.DisplayMember = "Soldier_Name";
            this.soldier_NameComboBox.FormattingEnabled = true;
            this.soldier_NameComboBox.Location = new System.Drawing.Point(314, 166);
            this.soldier_NameComboBox.Name = "soldier_NameComboBox";
            this.soldier_NameComboBox.Size = new System.Drawing.Size(161, 21);
            this.soldier_NameComboBox.TabIndex = 4;
            this.soldier_NameComboBox.SelectedIndexChanged += new System.EventHandler(this.soldier_NameComboBox_SelectedIndexChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Times New Roman", 15.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(259, 79);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(182, 24);
            this.label1.TabIndex = 5;
            this.label1.Text = "Punishment Search";
            // 
            // punish_search
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(684, 487);
            this.Controls.Add(this.label1);
            this.Controls.Add(soldier_NameLabel);
            this.Controls.Add(this.soldier_NameComboBox);
            this.Controls.Add(this.punishment_MasterDataGridView);
            this.Name = "punish_search";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Punishment_Search";
            this.Load += new System.EventHandler(this.punish_search_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataSet1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.punishment_MasterBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.punishment_MasterDataGridView)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DataSet1 dataSet1;
        private System.Windows.Forms.BindingSource punishment_MasterBindingSource;
        private Military_Project.DataSet1TableAdapters.Punishment_MasterTableAdapter punishment_MasterTableAdapter;
        private System.Windows.Forms.DataGridView punishment_MasterDataGridView;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
        private System.Windows.Forms.ComboBox soldier_NameComboBox;
        private System.Windows.Forms.Label label1;
    }
}