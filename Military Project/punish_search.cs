using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Military_Project
{
    public partial class punish_search : Form
    {
        public punish_search()
        {
            InitializeComponent();
        }
        DataSet1TableAdapters.Punishment_MasterTableAdapter PS = new Military_Project.DataSet1TableAdapters.Punishment_MasterTableAdapter();


        private void punishment_MasterBindingNavigatorSaveItem_Click(object sender, EventArgs e)
        {
            this.Validate();
            this.punishment_MasterBindingSource.EndEdit();
            this.punishment_MasterTableAdapter.Update(this.dataSet1.Punishment_Master);

        }

        private void punish_search_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'dataSet1.Punishment_Master' table. You can move, or remove it, as needed.
            this.punishment_MasterTableAdapter.Fill(this.dataSet1.Punishment_Master);

        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
           
        }

        private void soldier_NameComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            DataTable dt = new DataTable();
            dt = PS.search_punishment(soldier_NameComboBox.Text);
            if (dt.Rows.Count > 0)
            {
                punishment_MasterDataGridView.DataSource = dt;
            }
        }
    }
}