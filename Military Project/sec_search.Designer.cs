namespace Military_Project
{
    partial class sec_search
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.Label section_NameLabel;
            this.dataSet1 = new Military_Project.DataSet1();
            this.section_MasterBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.section_MasterTableAdapter = new Military_Project.DataSet1TableAdapters.Section_MasterTableAdapter();
            this.section_MasterDataGridView = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            section_NameLabel = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.section_MasterBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.section_MasterDataGridView)).BeginInit();
            this.SuspendLayout();
            // 
            // section_NameLabel
            // 
            section_NameLabel.AutoSize = true;
            section_NameLabel.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            section_NameLabel.Location = new System.Drawing.Point(168, 143);
            section_NameLabel.Name = "section_NameLabel";
            section_NameLabel.Size = new System.Drawing.Size(94, 17);
            section_NameLabel.TabIndex = 2;
            section_NameLabel.Text = "Section Name:";
            // 
            // dataSet1
            // 
            this.dataSet1.DataSetName = "DataSet1";
            this.dataSet1.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // section_MasterBindingSource
            // 
            this.section_MasterBindingSource.DataMember = "Section_Master";
            this.section_MasterBindingSource.DataSource = this.dataSet1;
            // 
            // section_MasterTableAdapter
            // 
            this.section_MasterTableAdapter.ClearBeforeFill = true;
            // 
            // section_MasterDataGridView
            // 
            this.section_MasterDataGridView.AutoGenerateColumns = false;
            this.section_MasterDataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn1,
            this.dataGridViewTextBoxColumn2,
            this.dataGridViewTextBoxColumn3,
            this.dataGridViewTextBoxColumn4,
            this.dataGridViewTextBoxColumn5});
            this.section_MasterDataGridView.DataSource = this.section_MasterBindingSource;
            this.section_MasterDataGridView.Location = new System.Drawing.Point(66, 197);
            this.section_MasterDataGridView.Name = "section_MasterDataGridView";
            this.section_MasterDataGridView.Size = new System.Drawing.Size(544, 220);
            this.section_MasterDataGridView.TabIndex = 1;
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.DataPropertyName = "Battalion_Id";
            this.dataGridViewTextBoxColumn1.HeaderText = "Battalion_Id";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.DataPropertyName = "Company_Id";
            this.dataGridViewTextBoxColumn2.HeaderText = "Company_Id";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.DataPropertyName = "Platoon_Id";
            this.dataGridViewTextBoxColumn3.HeaderText = "Platoon_Id";
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            // 
            // dataGridViewTextBoxColumn4
            // 
            this.dataGridViewTextBoxColumn4.DataPropertyName = "Section_Name";
            this.dataGridViewTextBoxColumn4.HeaderText = "Section_Name";
            this.dataGridViewTextBoxColumn4.Name = "dataGridViewTextBoxColumn4";
            // 
            // dataGridViewTextBoxColumn5
            // 
            this.dataGridViewTextBoxColumn5.DataPropertyName = "Section_Desc";
            this.dataGridViewTextBoxColumn5.HeaderText = "Section_Desc";
            this.dataGridViewTextBoxColumn5.Name = "dataGridViewTextBoxColumn5";
            // 
            // comboBox1
            // 
            this.comboBox1.DataSource = this.section_MasterBindingSource;
            this.comboBox1.DisplayMember = "Section_Name";
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Location = new System.Drawing.Point(285, 142);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(181, 21);
            this.comboBox1.TabIndex = 3;
            this.comboBox1.SelectedIndexChanged += new System.EventHandler(this.comboBox1_SelectedIndexChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Times New Roman", 15.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(281, 55);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(143, 24);
            this.label1.TabIndex = 4;
            this.label1.Text = "Section Search";
            // 
            // sec_search
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(684, 487);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.comboBox1);
            this.Controls.Add(section_NameLabel);
            this.Controls.Add(this.section_MasterDataGridView);
            this.Name = "sec_search";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Section_Search";
            this.Load += new System.EventHandler(this.sec_search_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataSet1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.section_MasterBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.section_MasterDataGridView)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DataSet1 dataSet1;
        private System.Windows.Forms.BindingSource section_MasterBindingSource;
        private Military_Project.DataSet1TableAdapters.Section_MasterTableAdapter section_MasterTableAdapter;
        private System.Windows.Forms.DataGridView section_MasterDataGridView;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn5;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.Label label1;
    }
}