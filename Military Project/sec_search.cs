using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Military_Project
{
    public partial class sec_search : Form
    {
        public sec_search()
        {
            InitializeComponent();
        }
        DataSet1TableAdapters.Section_MasterTableAdapter SEC_S = new Military_Project.DataSet1TableAdapters.Section_MasterTableAdapter();

        private void section_MasterBindingNavigatorSaveItem_Click(object sender, EventArgs e)
        {
            this.Validate();
            this.section_MasterBindingSource.EndEdit();
            this.section_MasterTableAdapter.Update(this.dataSet1.Section_Master);

        }

        private void sec_search_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'dataSet1.Section_Master' table. You can move, or remove it, as needed.
            this.section_MasterTableAdapter.Fill(this.dataSet1.Section_Master);
            comboBox1.Text = "";
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            DataTable dt = new DataTable();
            dt = SEC_S.search_section(comboBox1.Text);
            if (dt.Rows.Count > 0)
            {
                section_MasterDataGridView.DataSource = dt;
            }
        }
    }
}