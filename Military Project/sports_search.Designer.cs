namespace Military_Project
{
    partial class sports_search
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.Label sports_IdLabel;
            this.dataSet1 = new Military_Project.DataSet1();
            this.sports_MasterBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.sports_MasterTableAdapter = new Military_Project.DataSet1TableAdapters.Sports_MasterTableAdapter();
            this.sports_MasterDataGridView = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            sports_IdLabel = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sports_MasterBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sports_MasterDataGridView)).BeginInit();
            this.SuspendLayout();
            // 
            // sports_IdLabel
            // 
            sports_IdLabel.AutoSize = true;
            sports_IdLabel.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            sports_IdLabel.Location = new System.Drawing.Point(230, 142);
            sports_IdLabel.Name = "sports_IdLabel";
            sports_IdLabel.Size = new System.Drawing.Size(64, 17);
            sports_IdLabel.TabIndex = 1;
            sports_IdLabel.Text = "Sports Id:";
            // 
            // dataSet1
            // 
            this.dataSet1.DataSetName = "DataSet1";
            this.dataSet1.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // sports_MasterBindingSource
            // 
            this.sports_MasterBindingSource.DataMember = "Sports_Master";
            this.sports_MasterBindingSource.DataSource = this.dataSet1;
            // 
            // sports_MasterTableAdapter
            // 
            this.sports_MasterTableAdapter.ClearBeforeFill = true;
            // 
            // sports_MasterDataGridView
            // 
            this.sports_MasterDataGridView.AutoGenerateColumns = false;
            this.sports_MasterDataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn1,
            this.dataGridViewTextBoxColumn2,
            this.dataGridViewTextBoxColumn3});
            this.sports_MasterDataGridView.DataSource = this.sports_MasterBindingSource;
            this.sports_MasterDataGridView.Location = new System.Drawing.Point(167, 194);
            this.sports_MasterDataGridView.Name = "sports_MasterDataGridView";
            this.sports_MasterDataGridView.Size = new System.Drawing.Size(342, 220);
            this.sports_MasterDataGridView.TabIndex = 3;
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.DataPropertyName = "Sports_Id";
            this.dataGridViewTextBoxColumn1.HeaderText = "Sports_Id";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.DataPropertyName = "Sports_Name";
            this.dataGridViewTextBoxColumn2.HeaderText = "Sports_Name";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.DataPropertyName = "Description";
            this.dataGridViewTextBoxColumn3.HeaderText = "Description";
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            // 
            // comboBox1
            // 
            this.comboBox1.DataSource = this.sports_MasterBindingSource;
            this.comboBox1.DisplayMember = "Sports_Id";
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Location = new System.Drawing.Point(311, 139);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(146, 21);
            this.comboBox1.TabIndex = 4;
            this.comboBox1.SelectedIndexChanged += new System.EventHandler(this.comboBox1_SelectedIndexChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Times New Roman", 15.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(281, 50);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(134, 24);
            this.label1.TabIndex = 5;
            this.label1.Text = "Sports Search";
            // 
            // sports_search
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(684, 487);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.comboBox1);
            this.Controls.Add(this.sports_MasterDataGridView);
            this.Controls.Add(sports_IdLabel);
            this.Name = "sports_search";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Sports_Search";
            this.Load += new System.EventHandler(this.sports_search_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataSet1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sports_MasterBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sports_MasterDataGridView)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DataSet1 dataSet1;
        private System.Windows.Forms.BindingSource sports_MasterBindingSource;
        private Military_Project.DataSet1TableAdapters.Sports_MasterTableAdapter sports_MasterTableAdapter;
        private System.Windows.Forms.DataGridView sports_MasterDataGridView;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.Label label1;
    }
}