using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Military_Project
{
    public partial class sports_search : Form
    {
        public sports_search()
        {
            InitializeComponent();
        }
        DataSet1TableAdapters.Sports_MasterTableAdapter SS = new Military_Project.DataSet1TableAdapters.Sports_MasterTableAdapter();
        private void sports_MasterBindingNavigatorSaveItem_Click(object sender, EventArgs e)
        {
            this.Validate();
            this.sports_MasterBindingSource.EndEdit();
            this.sports_MasterTableAdapter.Update(this.dataSet1.Sports_Master);

        }

        private void sports_search_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'dataSet1.Sports_Master' table. You can move, or remove it, as needed.
            this.sports_MasterTableAdapter.Fill(this.dataSet1.Sports_Master);
            comboBox1.Text = "";
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            DataTable dt = new DataTable();
            dt = SS.search_sports(comboBox1.Text);
            if (dt.Rows.Count > 0)
            {
                sports_MasterDataGridView.DataSource = dt;
            }
        }
    }
}